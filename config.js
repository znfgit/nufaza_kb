var config = {

	site_title: 'Nufaza Knowledge Base',

	base_url: '',

	support_email: 'donny.fauzan@gmail.com',

	copyright: 'Copyright &copy; '+ new Date().getFullYear() +' Nufaza, Inc',

	excerpt_length: 400,

	page_sort_meta: 'sort',

	category_sort: true,

	image_url: '/images',

	content_dir: './content/',

	analytics: ""

};

module.exports = config;