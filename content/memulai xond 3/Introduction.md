/*
Title: Pengenalan
Sort: 1
*/ 

### Sejarah Xond

Xond dimulai saat mendapatkan Proyek dari Pramindo/Telkom untuk membuat Manajemen Hotspot. Xond membantu
perintisan key personnel cikal bakal PT.Nufaza sehingga eksis sampai sekarang.


Berikut versi-versi Xond dan aplikasi yang didukungnya:

#### Xond 1:

Libraries:
- Frontend: Extjs 3.0 - Extjs 3.2
- Backend: PHP, Propel, PEAR
- Inspiration: CakePHP & Zend Library

Apps:
- Hotspot Management (unfinished) Telkom/Pramindo
- SIM Keuangan  (Websai Dikdasmen)
- SPIN Keuangan  Dikdasmen

Pros:
- Automatic modules & menu
- CRUD Modules
- Cool components support (Tree, Calendar, TreeGrid, DnD)

Cons:
- Folder layout
- No front end generators
- Stiff front end layout
- Stiff automated backend 
- Incomplete security model
- Limited parameters
- Incomplete REST service

#### Xond 1.5:

Libraries:
- Frontend: Extjs 3.0 - Extjs 3.2
- Backend: PHP, Propel, PEAR
- Inspiration: CakePHP & Zend Library

Apps:
- SIM Tunjangan Profesi  P2TK Dikdas
- SIM Aneka Tunjangan P2TK Dikdas
- SIM Rasio Gen.1 P2TK Dikdas
- SIMPAK Gen.1 P2TK Dikdas
- SIM Jabfung Gen 1 P2TK Dikdas

Pros:
- Flexible front end layout
- CRUD Modules
- Cool components support (Tree, Calendar, TreeGrid, DnD)

Cons:
- No automatic modules & menu
- Bad folder layout
- No front end generators
- Stiff automated backend 
- Incomplete security model
- Limited parameters
- Incomplete REST service


#### Xond 2:

Libraries:
- Frontend: Extjs 4
- Backend: PHP, Silex, Propel, Composer
- Inspiration: Symfony

Apps:
- Dapodikdas
- Dapodikmen
- SIM Inpassing

Pros:
- Sophisticated generators
- Bridge generators between DB to Frontend
- Flexible front end layout
- Costumizable CRUD Front End generation
- Cool components support (Tree, Calendar, TreeGrid, DnD)

Cons:
- No automatic modules & menu
- Too flexible backend layout (no real enforcement of modules and Classes hierarchy)
- Library is still tied with production code
- Incomplete REST service
- NO documentation

#### Xond 3:

Libraries:
- Frontend: Extjs 5
- Backend: PHP, Silex, Propel, Composer
- Inspiration: Symfony

Apps:
- SIMPAK Gen.2 P2TK Dikdas
- SIM Rasio Gen.2 (Experiment) P2TK Dikdas
- SIM Jabfung Gen.2 P2TK Dikdas
- SIM PKG P2TK Dikdas
- EIS P2TK Dikdas
- SPM Dikdas 
- Orda Jabar

Pros:
- Much sophisticated generators
- Bridge generators between DB to Frontend
- Flexible front end layout
- Costumizable CRUD Front End generation
- Cool components support (Tree, Calendar, TreeGrid, DnD)
- Complete separation between libraries and production code
- Complete REST service

Cons:
- No automatic modules & menu
- Too flexible backend layout (no real enforcement of modules and Classes hierarchy)
- Incomplete documentation (especially for magic features)
- No testing yet

### Requirements 

Requirement Xond adalah sbb:

- Composer
- Silex 1.x
- Propel 1.6.x
- Twig (built in with Silex)
- PHP Excel 1.7
- Git
- Ruby 1.8.x (for Sencha Cmd) 
- Sencha Cmd (exactly 5.0.0.160)
- Extjs 5 (exactly 5.0.0)
