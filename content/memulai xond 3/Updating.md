/*
Title: Kontribusi ke Xond 3 Installer
Sort: 3
*/

Untuk development backend Xond, misal REST, printing dsb, anda dapat menggunakan langsung folder di bawah aplikasi
yang sedang anda kembangkan, di direktori vendor/xond/xond/ . Di situ sudah source controlled. Namun hati2, jangan
melakukan coding xond di beberapa folder aplikasi, karena akan membingungkan repo. Satu-satu sajalah.

Xond secara default sudah teregister dan menginstal beberapa route sendiri, jelasnya baca vendor/xond/xond/src/Silex.

Untuk mengupdate / push perubahan dapat dilakukan di masing-masing folder ybs. 

-   Untuk Xond 3 Installer posisinya di root folder, misal /d/Projects/localhost/xond_installer, 
-   Untuk Xond 3 Runtime, masuklah dulu ke /d/Projects/localhost/your_app/vendor/xond/xond/ 
