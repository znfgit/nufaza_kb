/*
Title: Structure
Sort: 2
*/ 

Secara struktur, Xond terdiri dari dua bagian besar, yaitu:

1.  Xond 3 Installer
    
    Xond 3 Installer adalah web application yang bersifat private yang digunakan untuk memulai "kick-off" pengembangan aplikasi.
    App ini membuat struktur dasar kerangka web app, termasuk menjalankan script-script instalasi awal. Aplikasi ini dikembangkan 
    secara private, hostingnya di bitbucket.

2.  Xond 3 Runtime
    
    Xond 3 Runtime adalah framework runtime Xond yang selalu ikut di setiap aplikasi yang dibuat oleh Xond 3 Installer. 
    Aplikasi Runtime ini dapat selalu diupdate melalui composer, dan dihosting di github dan didaftarkan di packagist.

### Struktur Xond 3 Installer
Berikut struktur direktori Xond 3 Installer

Folder              | Deskripsi                              
:-------------------|:--------------------------------------------------------------------------------------------
app                 | Backend Silex untuk installer
doc                 | Berisi dokumen ini 
src                 | Class-class Installer 
⊢ MyApp             | Dummy aplikasi untuk mengetes development Xond Runtime
⊢ XondInstaller     | Class-class Utama Installer
templates           | Berisi skeleton/template struktur aplikasi web yang akan diinstal
vendor              | Berisi vendor files yang akan dicopy-kan ke aplikasi yang akan diinstal
⊢ xond              | Berisi copy library runtime Xond 3 yang git-enabled. Digunakan utk mengembangkan runtime.
web                 | Frontend installer


### Struktur Xond 3 Runtime
Berikut struktur direktori Xond 3 Runtime

Folder              | Deskripsi                              
:-------------------|:--------------------------------------------------------------------------------------------
lib                 | Berisi library fungsi-fungsi php reusable. File diload oleh bootstrap.php
src                 | Berisi Class-class Utama Xond 3 Runtime
⊢ auth              | Security classes
⊢ gen               | Generator classes (Infos, Charts)
  ⊢ templates       | Templates
⊢ info              | Base Info classes (Table, Fields, etc)
⊢ output            | Output classes (Print, PDF, etc)
⊢ rest              | Backend Data Classes (REST)

