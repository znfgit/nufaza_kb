/*
Title: Menginstall Xond 3 Installer
Sort: 2
*/

### Perkenalan

Xond Installer adalah aplikasi installer framework Xond 3 yang dibuat untuk 
memudahkan startup / kick-off pengembangan aplikasi menggunakan framework Xond 3. 

Pada dasarnya, instalasi Xond 3 Installer masih dengan cara copy manual antar disk. Baru kemudian user dapat melakukan update  ataupun kontribusi code melalui git. Pada satu folder tsb, ada tiga repository yang saling independent satu sama lain.  Struktur Xond 3 Installer yang dimaksud dapat dibaca di halaman [Pengenalan](./Introduction.md). 

Berikut alamat repository git yang dimaksud:

-   Xond 3 Installer: https://bitbucket.org/znfgit/xond_installer
-   Xond 3 Installer Wiki: https://bitbucket.org/znfgit/xond_installer/wiki
-   Xond 3 Runtime: https://github.com/zaunaf/xond

Penggunaan Xond Installer dilakukan dalam beberapa tahap.

-   [Persiapan](#persiapan)
-   [Pemilihan Setting Aplikasi](#pemilihan-setting-aplikasi)
-   [Identitas Aplikasi](#identitas-aplikasi)
-   [Setting Aplikasi](#setting-aplikasi)
-   [Setting Database](#setting-database)
-   [Setting Security](#setting-security)
-   [Proses Instalasi](#proses-instalasi)


### Persiapan

Tahap persiapan ada beberapa langkah. 

1.  Instalasi Dependency
2.  Konfigurasi Host & Apache Virtualhost
3.  Konfigurasi PHP
4.  Konfigurasi PATH
5.  Periksa Hasil Instalasi dan Konfigurasi
6.  Install Database


#### Instalasi Dependency

Pertama anda harus unduh dulu software-software berikut:

-   Lampp stack ([xampp](https://www.apachefriends.org/index.html) atau [wamp](http://www.wampserver.com/en/)).
-   Git (http://git-scm.com/) 
-   Composer (https://getcomposer.org/) 
-   Ruby (https://www.ruby-lang.org) versi 1.8 - 1.9
-   Sencha Command (http://www.sencha.com/products/sencha-cmd/download) versi 5.0.0.160
-   Sencha Extjs 5 GPL Library (http://www.sencha.com/products/extjs/details) versi 5.0.1
-   Xond 3 Installer (by disk)

Tambahan jika anda menggunakan Database SQLServer:

-   SQLServer Native Client. Gunakan yang sesuai versi server anda.
-   PDO_Sqlsrv (PDO Driver buatan Microsoft). Gunakan versi 3.0.

Setelah itu installah semua software yang dimaksud.

-   Git pakai source installer biasa, jalankan seperti biasa di OS masing-masing
-   Composer diinstall dengan command line. Bisa menggunakan command phar yang ada instalasi setiap OS.
    Pastikan saja path ke php nya sudah tersedia di environment variable. Ada juga installer windowsnya, bisa dijalankan
    seperti biasa.
-   Sencha command diinstal dengan installer, jadi seharusnya mudah.
-   Ekstraklah Library Extjs 5 di project folder anda. Standardnya adalah `d:\Projects\localhost` , atau
    untuk Posix OS (n*x) di `/Projects/localhost`. Rename menjadi extjs5, sehingga pathnya menjadi 
    `d:\Projects\localhost\extjs5`. Jangan lupa setting [http://localhost](http://localhost) ke `d:\Projects\localhost` 
    sehingga extjs5 bisa diakses melalui [http://localhost/extjs5](http://localhost/extjs5)
-   Ekstraklah Xond 3 Installer di project folder yang sama, sehingga pathnya menjadi
    `d:\Projects\localhost\xond3`. Sediakan local domain "xond3" dan arahkan virtualhostnya, sehingga 
    dapat diakses melalui [http://xond3](http://xond3)


#### Konfigurasi Hosts dan Apache (Konfigurasi utama & VirtualHost)

-   Sesuaikan konfigurasi host anda, di windows posisinya di c:\Windows\System32\drivers\etc\hosts. 
    Sedangkan di sistem POSIX biasanya di /etc/hosts. Tambahkan setidaknya dua entri, contohnya:
    ```
    127.0.0.1    xond3
    127.0.0.1    my_app
    ```

    Silakan ganti my_app dengan virtual domain aplikasi yang ingin anda pakai.

-   Setelah itu sesuaikan konfigurasi Apache anda. Di windows posisinya misalnya di
    c:\wamp\bin\apache\apache2.2.22\conf\httpd.conf, di POSIX misalnya di /opt/local/apache2/conf/httpd.conf.
    Perubahan yang perlu dilakukan di file ini di antaranya:
    
    *    Uncomment baris ini: 
         `LoadModule rewrite_module modules/mod_rewrite.so`

    *    Uncomment baris ini:
         `Include conf/extra/httpd-vhosts.conf`

-   Setelah itu sesuaikan VirtualHost anda. Contoh posisinya jika anda menggunakan wamp posisinya di sini: 
    `c:\wamp\bin\apache\apache2.2.22\conf\extra\httpd-vhosts.conf`
    sedangkan di POSIX misalnya: 
    `/opt/local/apache2/conf/extra/httpd-vhosts.conf`

    Uncomment dan sisipkan baris berikut:
    
    ```xml
    NameVirtualHost *:80
    <VirtualHost *:80>
      ServerName xond3
      DocumentRoot "/Projects/localhost/xond3/web"
      DirectoryIndex index.php
      <Directory "/Projects/localhost/xond3/web">
        AllowOverride All
        Allow from All
      </Directory>
    </VirtualHost>
    <VirtualHost *:80>
      ServerName my_app
      DocumentRoot "/Projects/localhost/my_app/web"
      DirectoryIndex index.php
      <Directory "/Projects/localhost/my_app/web">
        AllowOverride All
        Allow from All
      </Directory>
    </VirtualHost>
    ```

-   Sesudah itu pastikan semua software terinstal ini semua pathnya telah masuk dalam Environment Variables

    *   Di windows, klik kanan pada My Computer atau pada Start > Computer, pilih Advanced, pilih Advanced
        system settings, pilih Advanced, pilih Environment Variables, di System Variables cari PATH, kemudian
        cek apakah Git, Composer, Sencha Command sudah ada di path. Jika sudah, utk memastikan restartlah
        komputer anda.
    *   Di POSIX based OS, Unix/BSD/Linux/Mac, cari .profile di user home directory. Ubah variabel PATH sesuai
        yang dikehendaki.

-   Setelah itu, siapkan database aplikasi yang akan dibuat. Database setidaknya memenuhi syarat2 berikut:

    *   Dapat diakses dari komputer ybs
    *   Sudah berisikan tabel2, termasuk tabel pengguna, peran (nama disesuaikan seting aplikasi)
    *   Semua tabel harus sudah di-set primary key nya.


#### Konfigurasi PHP

Konfigurasi yang perlu disesuaikan di antaranya

*   Sesuaikan baris ini:
    `max_execution_time = 3600`

*   Untuk windows, uncomment (hilangkan tanda ";") baris ini agar sistem dapat konek dengan mysql & pgsql:
    ```
    extension=php_pdo_mysql.dll
    extension=php_pdo_pgsql.dll
    ```

*   Untuk windows, unduh paket pdo_sqlsrv, masukkan isinya (berupa file2 dll) ke dalam extension directory.
    Misalnya `c:\wamp\bin\php\php5.4.3\ext\`

*   Untuk windows, tambahkan baris ini agar sistem dapat konek dengan mssql server:
    Misalnya untuk php 5.4 dan thread save folder, sisipkan baris berikut:
    ```
    extension=php_pdo_sqlsrv_54_ts.dll
    extension=php_sqlsrv_54_ts.dll
    ```

#### Konfigurasi PATH
    
Tahap berikutnya adalah memasukkan semua aplikasi di atas ke PATH

*   php executable directory

*   git binary directory

*   composer script path

*   sencha command path --> jika ada dua sencha command, tidak masalah. diatasi belakangan.

*   ruby binary directory


#### Periksa Hasil Instalasi dan Konfigurasi

##### Cek Konfigurasi Virtual Domain/Hosts
    
Untuk memeriksa virtual domain, gunakan command ping sbb:
```
$ ping xond3
PING xond3 (127.0.0.1): 56 data bytes
64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=0.038 ms
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.123 ms
64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.104 ms
64 bytes from 127.0.0.1: icmp_seq=3 ttl=64 time=0.064 ms
^C
--- xond3 ping statistics ---
4 packets transmitted, 4 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.038/0.082/0.123/0.033 ms
```

##### Cek Konfigurasi PATH

Agar installer dan kelak runtime Xond3 mampu mengotomatisasi proses reverse engineering dan build, maka
path harus berhasil terinstal dengan baik. Di bawah ini contohnya ('$'' menunjukkan prompt, untuk POSIX. 
Untuk Windows, diganti ">"):
```
$ php -v
PHP 5.3.28 (cli) (built: Dec 14 2013 02:01:06)
Copyright (c) 1997-2013 The PHP Group
Zend Engine v2.3.0, Copyright (c) 1998-2013 Zend Technologies
    with Xdebug v2.2.3, Copyright (c) 2002-2013, by Derick Rethans

$ composer --version
Composer version 0ce0cf42e80d68f5cf5c766457536e2894e32ffc 2014-07-23 17:19:39

$ git --version
git version 1.7.12.4 (Apple Git-37)

$ ruby -v
ruby 1.8.7 (2012-02-08 patchlevel 358) [universal-darwin12.0]

$ sencha which
Sencha Cmd v5.0.0.160
/Users/abah/bin/Sencha/Cmd/5.0.0.160/
```

##### Cek Konfigurasi PDO

Untuk memeriksa konfigurasi PDO, jalankan http://xond3/phpinfo.php. Di situ harusnya muncul bagian pdo driver
yang anda cari.


####  Install Database
Buatlah database yang anda inginkan di lokasi yang sesuai. Boleh lokal, remote. Nanti setting di installer sesuai kebutuhan.


### Setting Instalasi


#### Pemilihan Setting Aplikasi

Proses ini memilih setting yang sudah pernah dilakukan. Jadi jika developer pernah melakukan setting, 
jika prosesnya gagal kemudian diulang lagi, tidak usah kuatir harus melakukan seting dari awal. Cukup
pilih seting aplikasi yang sudah pernah dilakukan. Data disimpan pada cookie, jadi browser harus diset
allow jika bertanya tentang cookie.

#### Identitas Aplikasi

Tahap berikutnya adalah mengisikan identitas aplikasi. Ingat bahwa penamaan identitas harus konsisten.
Jika judulnya diubah, semua ke bawahnya juga harus diubah. Jangan sampai ada yang kelewat masih pakai 
standard "my_app".

Keterangan mengenai field setting rasanya sudah cukup self-explanatory, namun perlu di-highlight yang 
berikut ini:

-   Nama Aplikasi: Silakan dipanjangkan agar nama aplikasi cukup deskriptif. Ditampilkan sebagai judul app
    di berbagai tempat misal header bar, login, dashboard. Jangan terlalu panjang sih.
-   Local Domain: Ini adalah domain untuk tes di browser lokal. Lakukan setting sebelum instalasi. Untuk
    windows posisinya di c:\windows\system32\drivers\etc\host . Di OS Posix (n*x) di /etc/hosts.
-   Local Domain Deployment: Ini adalah domain untuk test deployment. Setelah aplikasi di build di frontend,
    hasil build harus bisa dideploy dengan lancar di server. Untuk mencobanya harus diset di local. Ini adalah
    domain penyimpanannya. Silakan diset dulu kemudian dites.

#### Setting Database

Setting database cukup self-explanatory. Yang sedikit perlu dijelaskan yaitu PDO Driver untuk SQLServer. Mohon perhatian, banyak instalasi gagal karena kurang teliti mengisi parameter-parameter di setting database ini. 

##### PDO SQLServ

Di Windows pastikan anda sudah menginstal pdo_sqlserv yang dibuat microsoft. Dengan catatan lampp stack yang 64bit tidak bisa digunakan. PHP yang digunakan harus versi 32 bit. Mau tidak mau harus pakai xampp yg masih 32 bit. Bisa juga install PHP 32bit yang terpisah, menggunakan FastCGI nya IIS.

##### PDO di Unix: FreeTDS & DBLIB

Di OS berbasis POSIX (n*x), sangat disarankan anda gunakan FreeTDS, karena sudah cukup baik supportnya. Untuk setupnya saya sarankan baca posting [blog subcoder tentang freetds](http://subcoder77.wordpress.com/2013/04/24/koneksi-ke-sqlserver-dari-lingkungan-posix-unixlinuxosx/). Tulisan tsb masih mengarahkan ke php_odbc, namun jika anda install pdo_dblib seharusnya lebih straightforward.

#### Setting Aplikasi

Tahap berikutnya adalah mengisikan setting aplikasi. Setting ini menentukan apa saja yang akan di-generate nantinya. Cukup self-explanatory.

#### Setting Security

Tahap berikutnya adalah mengisikan setting security. Setting ini menentukan mekanisme autentikasi dan autorisasi. 
Perlu dijelaskan sedikit contoh struktur database security yang dimaksud:

-   User Table --> misal "pengguna"
    *  pengguna_id: Primary key 
    *  nama: Nama lengkap pengguna 
    *  **username**: Nama account yang digunakan nantinya. Bisa email address
    *  **password**: Password. Encrypted. Password dapat digenerate melalui xond3/passgen/

-   Role Table --> misal "peran"
    *  peran_id: Primary key 
    *  nama: Nama peran 
    *  kode: Kode peran. Misal '`ROLE_ADMIN`', '`ROLE_USER`'

-   Role Mapping --> misal "peran_pengguna"
    *  peran_id: Foreign key to tabel peran
    *  pengguna_id: Foreign key to tabel pengguna


#### Proses Instalasi

Proses instalasi menampilkan step by step instalasi berikut progress dan hasilnya tiap langkah apakah sukses atau tidak.  Jika suatu langkah sukses, maka akan muncul tanda _check_ berwarna hijau. Dan jika langkah gagal, maka instalasi akan berhenti dan menampilkan tanda _cross_ berwarna merah. Progress bar di atas menampilkan persentasi rampungnya instalasi.

#### Tahap-tahap Progress Instalasi

Berikut ini tahap-tahap progress instalasi dan penjelasannya:

-   Checking folder permissions
    
    Pada tahap ini, installer memeriksa apakah semua folder yang akan diisi bisa ditulisi (permissionnya w utk user web daemon).

-   Checking sencha command
    
    Memeriksa apakah sencha command sudah diinstal.

-   Checking composer
    
    Memeriksa apakah composer sudah diinstall

-   Checking git
    
    Memeriksa apakah git sudah diinstall

-   Creating project folder
    
    Membuat folder project utama

-   Checking project folder via http - local domain
    
    Memeriksa apakah local domain utk project ybs sudah disetup (static host, virtual host setting, reset httpd dsb)

-   Generating front end application
    
    Menjalankan sencha generate app, setelah memeriksa folder sdk.

-   Copying base files
    
    Menyalin file-file dasar untuk backend dari folder installer secara rekursif. Berikut file-file yang dimaksud:

    -   `.gitignore`
    -   `app/config`
    -   `log/runtime.log`
    -   `vendor`
    -   `web/.htaccess`
    -   `web/index.php`
    -   `web/resources/app.css`
    -   `web/resources/css`
    -   `web/resources/fonts`
    -   `web/resources/icons`
    -   `web/resources/images`

-   Translating config files
    
    Di tahap ini, file config yang berupa template twig diterjemahkan dengan konfigurasi yang dikirimkan. Berikut file2nya:

    -   `composer.json`
    -   `app/app.php`
    -   `app/bootstrap.php`
    -   `app/config.php`
    -   `app/config/build.compile.properties`
    -   `app/config/build.reverse.properties`
    -   `app/config/runtime-conf.xml`
    -   `app/config/set_path.bat`
    -   `app/config/set_path.sh`
    -   `web/app.json`
    -   `web/app.js`
    -   `web/app/Application.js`
    -   `web/app/view/main/Main.js`
    -   `web/app/view/_components/SampleChat.js`
    -   `web/app/view/_components/SampleCharts.js`
    -   `web/app/view/SampleModule1.js`
    -   `web/app/view/SampleModule2.js`
    -   `web/login.html `

-   Initializing git repository
    
    Menjalankan `git init`

-   Updating composer autoloading
    
    Menjalankan `composer update nothing` agar autoloading khususnya untuk class-class backend aplikasi yg digenerate dapat berjalan baik.

-   Reversing database information
    
    Menjalankan `propel gen . -reverse` setelah menyesuaikan setting-setting koneksi databasenya. Tahap ini akan membuat file berikut:
    
    -   `app/config/schema.xml`

-   Building backend application
    
    Menjalankan `propel gen` untuk membangun file-file ORM. Tahap ini akan mengisi folder-folder berikut:
    
    -   `app/config/conf`
    -   `app/config/sql`
    -   `src/MyApp/Model` ("MyApp" akan sesuai dengan nama_singkat yang diisi)

-   Building front end application
    
    Menjalankan `InfoGen`, `FrontEndGen` dan `sencha app build`. Tahap ini akan mengisi folder-folder berikut:
    
    -   `src/MyApp/Info` 
    -   `web/app/view/_components` 



### Troubleshooting

#### Persiapan

-   Jika Kalo composer gak bisa diinstall, berarti setting sqlserv nya masih salah, pilih yang ts jangan yg nts

##### Instalasi

-   Web xond3 terbuka, tapi blank cuma background:

    Jika anda baru mengopi source xond3 dari mac, ke windows, jangan lupa untuk mengkoreksi symlink di web/app/ux 
    dan web/app/xond. Cek di sini caranya: https://subcoder77.wordpress.com/2014/12/16/creating-symbolic-link-in-windows/

-   Jika web bengong lama tanpa reaksi:

    Kemungkinan Ajax timeout. Pastikan di server side (php.ini) perpanjang timeout execution time:
    max_execution_time = 3600
    max_input_time = 6000

-   Berhenti pada checking git atau composer: 
    
    Jika git atau composer tidak terdeteksi, masukkan path kedua program tersebut ke PATH di environment variable form. 
    Setelah itu stop dan start kembali apache atau webserver yg anda pakai.

-   Jika installer berhenti pada tahap "updating composer autoloading": 
    
    Kemungkinan internet mati. Updating composer membutuhkan koneksi internet.

-   Jika keluar error di firebug/dev tools "isProvider not defined" saat membuka dan menjalankan installer:

    Biasanya karena module rewritenya belum dinyalakan.
    Baca petunjuknya di atas.

-   Trouble shooting gagal tahap 8:

    1) Buka install.log, di situ pasti ada keterangan kenapa error.

    2) Jika kurang yakin, bisa coba masuk ke my_app/app/config/ kemudian cmd dari situ. Jalankan set_path.bat dan reverse_structure.bat

    3) Jika errornya menyatakan database unreachable atau semacamnya, cek koneksi ke database yang diinput di form setting. Misal pakai navicat, bisa nggak.

    4) Jika koneksi ternyata gagal ke database, ada kemungkinan SQL Server Browsernya mati, atau firewallnya masih memblokir koneksi. Betulkan.

    3) Jika error mencantumkan SQLServer client dan sebangsanya, berarti sql server client harus diupgrade. Baca baik2 errornya.

-   Berhenti pada building front end application:
    
    Jika anda punya dua sencha command di lokal, editlah path di halaman ke 3 instalasi, kemudian hilangkan path utk sencha
    command yg lama. 
