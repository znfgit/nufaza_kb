/*
Title: Mempercepat Load Web
Sort: 4
*/

#### Permasalahan

Source ExtJS ataupun library javascript lainnya bisa jadi sangat bulky untuk di web, dan bikin lola alias loading lama. Untuk mengatasinya, anda harus lakukan dulu build sebelum dideploy pada webserver atau instalasi aplikasi. Untuk lebih mantap lagi, mengurangi loadtime, kita bisa menggunakan strategi compression.

#### Memeriksa Apakah Kompresi Sudah Disetting

Sebelum melakukan perubahan apa-apa pada webserver, pastikan dulu apakah kompresi sudah disetting pada server ybs. Gunakan developer tools di browser anda misal firebug di mozilla, bagian net / resources. Baca di bagian “request”, biasanya deskripsi headersnya seperti ini:

```
GET http://localhost:45002/blog/2013/6/7/enabling-gzip-in-iis-on-windows-8 HTTP/1.1 
Host: localhost:45002 
Connection: keep-alive Cache-Control: no-cache 
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8 Pragma: no-cache 
User-Agent: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36 
Accept-Encoding: gzip,deflate,sdch Accept-Language: en-US,en;q=0.8,fr-CA;q=0.6,fr;q=0.4
```

Baris paling bawah di atas menunjukkan bahwa browser siap menerima kompresi, memberi tahu server untuk mengirimkan data dalam format kompresi jika server mendukungnya. Nah jika server sudah support gzip, responsenya akan seperti berikut:

```
HTTP/1.1 200 OK Cache-Control: private Content-Type: text/html; charset=utf-8 
Content-Encoding: gzip Vary: Accept-Encoding 
Server: Microsoft-IIS/8.0 X-AspNetMvc-Version: 4.0 X-AspNet-Version: 4.0.30319 X-Powered-By: ASP.NET Date: Fri, 07 Jun 2013 13:50:04 GMT Content-Length: 3604
```



#### Menyalakan Kompresi Untuk Apache

Untuk apache, sudah ada mod_gzip yang biasanya default include pada port apache di setiap sistem operasi. Yang bisa kita lakukan adalah menyisipkan kode berikut di httpd.conf:

```
SetOutputFilter DEFLATE
SetEnvIfNoCase Request_URI \.(?:gif|jpe?g|ico|png)$ \ no-gzip dont-vary
SetEnvIfNoCase Request_URI \.(?:exe|t?gz|zip|bz2|sit|rar)$ \no-gzip dont-vary
SetEnvIfNoCase Request_URI \.pdf$ no-gzip dont-vary

BrowserMatch ^Mozilla/4 gzip-only-text/html
BrowserMatch ^Mozilla/4\.0[678] no-gzip
BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
```

Keterangan sedikit:

1. Baris pertama memberitahu apache untuk memanggil filter deflate untuk mengecilkan file sebelum dikirim.
2. Baris kedua menghindarkan file-file image seperti jpeg, png untuk dikompresi, karena file image yang dimaksud
   sudah dalam keadaan terkompresi dengan sangat baik
4. Baris ketiga menghindarkan archive yang sudah terkompresi agar tidak dikompresi lagi
5. Baris keempat menghindarkan pdf dari kompresi.
6. Memberi tahu yang dikompresi adalah file2 dengan mime text / html
7. Memberi tahu jika browser yg dipakai mozilla versi lama yang belum bisa terima encoding gzip, agar
   menghindari kompresi
8. Hal yang sama untuk Internet Exploder dari Mikrosop.



#### Menyalakan Kompresi Untuk IIS

Untuk IIS, pertama adalah pastikan setting kompresi sudah on atau belum:

[![Configuration Editor](/images/mempercepat_load_1_iis_compression.png)](/images/mempercepat_load_1_iis_compression.png)
[![Configuration Editor](/images/mempercepat_load_2_iis_compression.png)](/images/mempercepat_load_2_iis_compression.png)

Kalau dua-duanya sudah dicontreng berarti sudah aktif. Cek hasilnya. Biasanya format html & css akan 
terkompresi. Namun jika file js yg besar2 itu belum terkompresi biasanya karena ia terdetek sebagai “dynamic content”. Nah biasanya ada tampilan seperti ini di modul kompresi IIS.  (Lihat bagian “alerts” sebelah kanan):

[![Configuration Editor](/images/mempercepat_load_3_iis-no-gzip.jpg)](/images/mempercepat_load_3_iis-no-gzip.jpg)

Untuk mengatasinya di Windows 8 / Server, buka Control Panel / Programs / Turn Windows features on or off, lihat bagian ini:
[![Configuration Editor](/images/mempercepat_load_4_turn-on-dynamic-content-compression.jpg)](/images/mempercepat_load_4_turn-on-dynamic-content-compression.jpg)

Cari di gambar di atas “Dynamic Content Compression” masih belum dicheck. Nah itu check dulu. Paling simpel seperti itu. Maka semua output server semua akan dikompresi sebelum dikirim. Perhatikan bahwa jika load CPU di server ybs cukup berat, ini akan menambah beban pada CPU. Ada jalan alternatif untuk melakukan setting, yaitu edit configuration. Sedikit rumit. Bisa diintip di sini caranya:
http://www.hanselman.com/blog/EnablingDynamicCompressionGzipDeflateForWCFDataFeedsODataAndOtherCustomServicesInIIS7.aspx

Demikian. Silakan dicek hasilnya. Menurut pengalaman penulis, untuk file 1.7 MB, dengan sistem kompresi ini dapat turun sampai 500KB di linux, dan 700KB-an di Windows/IIS.

Silakan mencoba.
