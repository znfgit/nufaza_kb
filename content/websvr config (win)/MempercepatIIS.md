/*
Title: Mempercepat IIS
Sort: 3
*/

Kadang-kadang kita punya IIS server lemot dan tolol padahal CPU utilizationnya masih rendah banget adem ayem. Kok bloon gini ya? Gak mau kerja? Ohh ternyata seting IIS bawaan itu masih super malas. Mungkin agar spek server yg low masih bisa pake. Anyway to de poin aja :

1.  Buka IIS Manager, klik tuh di kiri bawah pojok “Configuration Editor”
    [![Configuration Editor](/images/mempercepat_iis_1_iis_manager.png)](/images/mempercepat_iis_1_iis_manager.png)

2.  Klik tombol dengan tiga titik yang ditandain merah di bawah ini
    [![Configuration Editor](/images/mempercepat_iis_2_configuration_editor.png)](/images/mempercepat_iis_2_configuration_editor.png)

3.  Ubah  maxInstances yang nilainya masih 4 menjadi 32 mengikuti  klik di panah2 pada gambar di bawah ini.
    [![Configuration Editor](/images/mempercepat_iis_3_edit_max_instances.png)](/images/mempercepat_iis_3_edit_max_instances.png)
        
4.  Tutup window di atas, kemudian tekan Apply pada windows sesudahnya. Test kembali website anda 
    (khususnya under load). Maka akan terasa kecepatan yang signifikan pada web server anda.