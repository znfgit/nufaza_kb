/*
Title: Gangguan Port 80 (Win)
Sort: 5
*/

Suatu waktu kita punya server windows yang sudah include fitur web IIS maupun sqlserver. Namun IIS itu kadang lambat untuk PHP, oleh karena itu kita kembali pada Apache. Agar apache jalan lancar tanpa gangguan di port 80:
1) Masuk ke IIS Manager, kemudian pindahkan semua site ke port lain. Ini harus. Soalnya IIS selalu hidup ketika server nyala.
2) Masuk ke services, periksa apakah ada SQLServer Reporting Services yg mengganggu. Matikan dulu baru start lagi apache.