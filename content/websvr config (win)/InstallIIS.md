/*
Title: Menginstall IIS
Sort: 2
*/

Install IIS di Windows gampang.

Win 7:
-  Buka Control Panel > Programs and Features > Turn Windows Feature on or off
-  Cari Internet Information Services, Pilih Web Management Tools & World Wide Web Services

Win Server 2012+: 
-  Buka Server Manager > Manage menu > select Add Roles and Features
-  Pilih Role-based or Feature-based Installation
-  Pilih local
-  Pilih Web Server (IIS)

