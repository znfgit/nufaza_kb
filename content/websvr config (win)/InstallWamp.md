/*
Title: Menginstall Apache
Sort: 1
*/

Menginstall Apache di windows gampang
-  Unduh wamp di sini http://www.wampserver.com/en pastikan install yang 32 bit saja.
-  Jalankan setup
-  Wamp akan tersimpan di C:\

File konfigurasi yang penting ada di path berikut (contoh untuk wamp 2.2):
-  c:\wamp\bin\apache\apache2.2.22\conf\ --> httpd.conf
-  c:\wamp\bin\apache\apache2.2.22\conf\extra\ --> httpd-vhosts.conf
-  c:\wamp\bin\apache\apache2.2.22\bin\ --> php.ini untuk web
-  c:\wamp\bin\mysql\mysql5.5.24\ --> untuk my.ini
-  c:\wamp\bin\php\php5.4.3 --> php.ini untuk command line

