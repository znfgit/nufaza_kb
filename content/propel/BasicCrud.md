/*
Title: Dasar CRUD dgn Propel
Sort: 2
*/

Di sini akan dibahas dasar-dasar C.R.U.D. (Create, Retrieve, Update, Delete) pada database menggunakan  Propel. Sebagai contoh digunakan contoh model bookstore seperti pada Intro.

Contoh2 berikut asumsinya anda sudah menginstall dan dapat menggunakan Propel.

#### CREATE

Penambahan data ke database caranya dengan membuat `instance` obyek yang digenerate Propel dan kemudian memanggil fungsi save() pada objek tersebut. Propel akan membuat INSERT SQL secara otomatis .

**Simple INSERT**

Sebagai contoh sederhana, membuat dan mengisi data adalah sbb:

```
<?php
/* initialize Propel, etc. */

$author = new Author();
$author->setFirstName("Jack");
$author->setLastName("London");
$author->save();
```

Di belakang script yg dihasilkan dan dieksekusi ke database adalah sbb:

```
INSERT INTO author (first_name, last_name) VALUES ('Jack', 'London');
Related Row Insert
```

Propel menyederhanakan pembuatan record data dengan secara otomatis _cascading_ atau insert berturutan ketika ada object lain yang berelasi kepada obyek tsb. Contohnya di sini kita buat obyek penulis (_Author_), penerbit (_Publisher_) yang ditambahkan pada obyek buku (_Book_), dan ketiganya akan tersimpan ketika Book->save() dipanggil.  

```
<?php
/* initialize Propel, etc. */

// 1) Create an Author (row of 'author' table)

include_once 'bookstore/Author.php';

$author = new Author();
$author->setFirstName("Leo");
$author->setLastName("Tolstoy");
// note: we don't save this yet

// 2) Create a Publisher (row of 'publisher' table)

include_once 'bookstore/Publisher.php';

$pub = new Publisher();
$pub->setName("Viking Press");
// note: we don't save this yet

// 3) Create a Book (row of 'book' table)

include_once 'bookstore/Book.php';

$book = new Book();
$book->setTitle("War & Peace");
$book->setIsbn("0140444173");
$book->setPublisher($pub);
$book->setAuthor($author);
$book->save(); // saves all 3 objects!
```

Propel dapat mendeteksi sebuah obyek tersimpan atau belum, sehingga dia melakukan save() sebelum menambahkannya pada objek Buku.

#### RETRIEVE

Membaca/memanggil object-object dari database, disebut juga _hydrating objects_, esensinya adalah mengeksekusi query SELECT pada database dan mengisinya pada _instance_ obyek terkait di tiap barisnya.

Dalam Propel, Anda menggunakan class **Peer** yg tergenerate untuk memilih baris yang ada dari database. Class Peer adalah kelas dengan metode eksklusif yang dipanggil secara statis untuk melakukan operasi pada tabel tertentu. Ada beberapa metode yang dihasilkan untuk membantu Anda dalam memilih satu objek atau query untuk beberapa objek dari database.

##### Retrieve menggunakan Primary Key

Cara paling sederhana untuk mengambil sebuah objek (baris) dari database, adalah dengan menggunakan metode yang dihasilkan retrieveByPK (). Metode ini harus melewati nilai Primary Key untuk objek yang akan diambil.

**Primary Key-nya Satu Kolom**

Biasanya tabel Anda akan memiliki satu kolom sebagai Primary Key, yang berarti bahwa metode retrieveByPK () akan menerima satu parameter.

```
<?php

$firstBook = BookPeer::retrieveByPK(1);
// now $firstBook is a Book object, or NULL if no match was found.

```

**Multi-Col PK**

Dalam beberapa kasus, Primary Key Anda dapat terdiri dari lebih dari satu kolom. Dalam hal metode retrieveByPK () akan dibuat untuk menerima beberapa parameter, satu untuk setiap kolom Primary Key.

Sebagai contoh, jika kita memiliki tabel dengan Primary Key multi-kolom, didefinisikan seperti ini:
```
   <table name="multicolpk_example" phpName="MultiColPKExample">
      <column name="id1" type="INTEGER" primaryKey="true"/>
      <column name="id2" type="INTEGER" primaryKey="true"/>
      <!-- other columns ... -->
   </table>
```
... maka retrieveByPK() dipanggilnya seperti ini:

```
<?php

$myObject = MultiColPKExamplePeer::retrieveByPK(1,2);
```

**Mengambil Beberapa Objects Menggunakan PK**

Anda juga dapat memilih beberapa objek berdasarkan Primary Key mereka, dengan memanggil fungsi retrieveByPKs() yg tergenerate pada setiap obyek _Peer_, yang mengambil sebagai parameter array kunci primer:

```
<?php

$selectedBooks = BookPeer::retrieveByPKs(array(1,2,3,4,5,6,7));
// $selectedBooks is an array of Book objects
```

Catatan bahwa ini hanya bekerja untuk tabel dengan kunci primer tunggal-kolom.


##### Query Database

Untuk memilih beberapa baris dengan criteria selain Primary Key, kami memiliki dua pilihan: 
1.  menggunakan class Propel `Criteria`, atau 
2.  menulis kustom SQL. 

Kelas Criteria menyediakan pendekatan yang relatif sederhana untuk membangun query. Kesederhanaan logis dan netralitas terhadap database apapun membuat Criteria menjadi pilihan yang baik untuk mengungkapkan banyak query umum; Namun, untuk query yang sangat kompleks, dapat digunakan query SQL biasa.

**Criteria sederhana**

Berikut adalah beberapa contoh dari Criteria sederhana yang digunakan untuk mengembalikan beberapa objek.

Contoh 1: Cari semua penulis dengan nama pertama Karl tapi nama terakhir adalah _not_ Marx.

```
<?php

$c = new Criteria();
$c->add(AuthorPeer::FIRST_NAME, "Karl");
$c->add(AuthorPeer::LAST_NAME, "Marx", Criteria::NOT_EQUAL);

$authors = AuthorPeer::doSelect($c);
// $authors contains array of Author objects
```
... hasilnya adalah query SQL:
```
SELECT ... FROM author WHERE author.FIRST_NAME = 'Karl' AND author.LAST_NAME <> 'Marx';
```
Contoh 2: Cari semua penulis dengan nama terakhir Tolstoy, Dostoevsky, atau Bakhtin
```
<?php

$c = new Criteria();
$c->add(AuthorPeer::LAST_NAME, array("Tolstoy", "Dostoevsky", "Bakhtin"), Criteria::IN);

$authors = AuthorPeer::doSelect($c);
// $authors contains array of Author objects
```
... hasilnya SQL query seperti:
```
SELECT ... FROM author WHERE author.LAST_NAME IN ('Tolstoy', 'Dostoevsky', 'Bakhtin');
```

**Logically Complex Criteria**

Bila Anda perlu untuk mengekspresikan hubungan logis (AND, OR, dll) antara klausa criteria yang berbeda, Anda perlu secara manual menggabungkan objek _Criterion_ satu per satu. Obyek-obyek Criterion  adalah komponen yang transparan dirakit menjadi objek Criteria bila Anda menggunakan fungsi Criteria->add().

Contoh 1: Cari semua penulis dengan nama pertama "Leo" OR nama terakhir "Tolstoy", "Dostoevsky", atau "Bakhtin"
```
<?php

$c = new Criteria();
$cton1 = $c->getNewCriterion(AuthorPeer::FIRST_NAME, "Leo");
$cton2 = $c->getNewCriterion(AuthorPeer::LAST_NAME,  array("Tolstoy", "Dostoevsky", "Bakhtin"), Criteria::IN);
 
// combine them
$cton1->addOr($cton2);
 
// add to Criteria
$c->add($cton1);
```
... hasilnya SQL query seperti:
```
SELECT ... FROM author WHERE (author.FIRST_NAME = 'Leo' OR author.LAST_NAME IN ('Tolstoy', 'Dostoevsky', 
'Bakhtin'));
```
Ada juga beberapa shortcut Criteria jika Anda ingin melakukan query dengan hubungan logis antara klausul yang referensi kolom yang sama.

Contoh 2: Cari semua penulis dengan nama pertama 'Leo' atau 'Karl'

Menggunakan pendekatan Criteria yg lengkap, ini tampak seperti:
```
<?php

$c = new Criteria();
$cton1 = $c->getNewCriterion(AuthorPeer::FIRST_NAME, "Leo");
$cton2 = $c->getNewCriterion(AuthorPeer::FIRST_NAME, "Karl");
 
// combine them
$cton1->addOr($cton2);
 
// add to Criteria
$c->add($cton1);
```

Menggunakan kependekan fungsi Criteria:
```
<?php

$c = new Criteria();
$c->add(AuthorPeer::FIRST_NAME, "Leo");
$c->addOr(AuthorPeer::FIRST_NAME, "Karl");
```
Hal ini penting untuk dicatat bahwa metode ini Criteria singkatan memiliki sejumlah keterbatasan - terutama bahwa mereka hanya bekerja untuk hubungan kolom tunggal. Dari sudut pandang penulis, tidak disarankan untuk menggunakan metode ini ketika Anda harus mengungkapkan hubungan logis antara klausa karena mereka mengaburkan relationsihps sebenarnya antara objek Criterion dan dapat dengan mudah menyebabkan masalah yang sulit-untuk-debug generasi query. (Metode ini mungkin akan diganti di Propel 2.)

**Menggunakan Custom SQL**

Propel dirancang untuk bekerja dengan Anda daripada melawan Anda. Dalam banyak kasus menulis query yang kompleks menggunakan Criteria akhirnya menjadi benar-benar berantakan dan bahkan kurang dimengerti atau dipertahankan dari query SQL standar. Propel diciptakan untuk bekerja keterbatasan yang melekat dari sistem Criteria database netral dengan membuatnya mudah bagi Anda untuk menggunakan query SQL Anda sendiri untuk menghidrasi hasil set.

Jadi, dengan hanya sedikit lebih banyak pekerjaan, Anda juga bisa mendapatkan objek dari database Anda menggunakan SQL. Menggunakan SQL untuk query database memperkenalkan kita kepada populateObjects dihasilkan () metode dalam kelas rekan kami - yang disebut di belakang layar dengan metode doSelect (). Metode ini mengharapkan akan melewati objek Creole ResultSet, numerik diindeks (yaitu yang dibuat menggunakan! ResultSet :: opsi FETCHMODE_NUM ke executeQuery ()).

Contoh 1: Gunakan sub-selects untuk mengisi database

```
<?php

$con = Propel::getConnection(DATABASE_NAME);

$sql = "SELECT books.* FROM books WHERE NOT EXISTS (SELECT id FROM review WHERE book_id = book.id)";  
$stmt = $con->prepare($sql);
$rs = $stmt->execute();

$books = BookPeer::populateObjects($rs);    
```
Penting diingat ketika menggunakan custom SQL untuk mengisi Propel:

-  Kolom ResultSet harus terindex secara numerik
-  ResultSet harus berisikan semua kolom di objek terkait 
-  ResultSet harus memiliki kolom dengan urutan yang sama dengan yang ditulis di file schema.xml


#### UPDATE

Memperbarui/mengupdate baris database pada dasarnya melibatkan mengambil objek, memodifikasi isi, dan kemudian menyimpannya. Dalam prakteknya, untuk Propel, ini adalah kombinasi dari apa yang telah kita lihat di RETRIEVE dan CREATE

```
<?php

// 1) Fetch an object by primary key

$myBook = BookPeer::retrieveByPK(1);

// 2) update the values & save() it.

$myBook ->setTitle("War & Peace");
$myBook->save();
```

Cukup begitu saja. Relationship juga dapat diupdate dengan cara itu.

```
<?php
/* initialize Propel, etc. */

// 1) retrieve an Author
$author = AuthorPeer::retrieveByPK(1);

// 2) retrieve a Book
$book = BookPeer::retrieveByPK(1);

// 3) now blindly set $author as the author for $book!

$book->setAuthor($author);
$book->save();

```


#### DELETE

Menghapus objek dapat dicapai dengan menggunakan kelas Peer atau kelas objek.

**Menggunakan Peer**

Anda dapat menggunakan dihasilkan doDelete () metode kelas rekan dihasilkan untuk menghapus baris dari tabel Anda. Anda dapat melewati metode ini kunci utama, sebuah instance dari objek yang tepat, atau bahkan benda Kriteria (namun hal ini tidak sangat berguna, karena Anda hanya dapat menghapus dengan primary key).

Contoh 1: Hapus menggunakan primary key

```
<?php

BookPeer::doDelete(1);
```

Example 2: Hapus menggunakan objek yang diambil terlebih dahulu

```
<?php

$book = BookPeer::retrieveByPK(1);
BookPeer::doDelete($book);
```

**Menggunakan Objek**

Untuk konsistensi dengan operasi C.R.U.D. lainnya, Anda juga bisa menghapus baris database menggunakan kelas objek. Sebagian orang memilih untuk melakukannya dengan cara ini, memanggil obyek dan kemudian memanggil delete() metode pada objek tersebut. Ini bisa membingungkan karena objek yg tersisa setelah pemanggilan delete tidak nyangkut pada baris apapun di database.
```
$book = BookPeer::retrieveByPK(1);
$book->delete();
```
// (ingat bahwa object $book tidak bisa lagi diapa-apakan)
