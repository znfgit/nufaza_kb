/*
Title: Retrieve Data Obyek
Sort: 3
*/


Berikut ini cara mengambil serangkaian data dari database melalui Propel


Buat dulu routenya:
```
// Mengambil data melalui propel dan menampilkannya via twig
$app->get('/cobapropelgetall', 'MyApp\Coba::propelGetAll');
```


Tambahkan di Coba.php
```

    public function propelGetAll(Request $request, Application $app) {

        // Load penggunas
        $c = new \Criteria();
        $penggunas = PenggunaPeer::doSelect($c);

        $arrayPenggunas = array();

        foreach ($penggunas as $p) {
            $arrayPenggunas[] = $p->toArray();
        }

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__).D."templates";
        $fileName = "penggunaspropel.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader);

        // Attach data and render
        $data = array(
            "penggunas" => $arrayPenggunas
        );

        $outStr = $twig->render($fileName, $data);

        return $outStr;
    }
```


Buat template twignya `C:\Projects\localhost\my_app\src\MyApp\templates\penggunaspropel.twig`:
```

Daftar pengguna di sistem ini adalah sbb:

<table border="1">
    <tr><th>ID</th><th>Nama</th><th>Username</th></tr>
{% for p in penggunas %}
    <tr><td>{{ p.pengguna_id }}</td><td>{{ p.nama }}</td><td>{{ p.username }}</td></tr>
{% endfor %}
</table>
```
