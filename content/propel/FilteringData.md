/*
Title: Memfilter Data dengan LIKE
Sort: 5
*/

Memfilter data menggunakan LIKE.

Buat dulu routenya:
```
// Mengambil satu data melalui filter LIKE
$app->get('/cobapropelgetlike', 'MyApp\Coba::propelGetLike');
```

Tambahkan di Coba.php:
```
    public function propelGetLike(Request $request, Application $app) {

        // Ambil penggunaId
        $queryNama = $request->get('query_nama');

        // If empty
        if (!$queryNama) {
            $queryNama = "";
        } else {
            $queryNama = "%" . $request->get('query_nama') . "%";   // perhatikan baris ini
        }

        // Load penggunas
        $c = new \Criteria();
        $c->add(PenggunaPeer::NAMA, $queryNama, \Criteria::LIKE);   // perhatikan baris ini
        $penggunas = PenggunaPeer::doSelect($c);

        $arrayPenggunas = array();

        foreach ($penggunas as $p) {
            $arrayPenggunas[] = $p->toArray();
        }

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__).D."templates";
        $fileName = "penggunassearch.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader);

        // Attach data and render
        $data = array(
            "penggunas" => $arrayPenggunas
        );

        $outStr = $twig->render($fileName, $data);

        return $outStr;

    }
```



Buat template twignya berikut form:
```
Cari pengguna:
<form action="/cobapropelgetlike" method="get">
    <input type="text" name="query_nama" value="{{ queryNama }}">
    <input type="submit">
</form>

<table border="1">
    <tr><th>ID</th><th>Nama</th><th>Username</th></tr>
    {% for p in penggunas %}
        <tr><td>{{ p.pengguna_id }}</td><td><a href="/cobapropelgetone?pengguna_id={{ p.pengguna_id }}">{{ p.nama }}</a></td><td>{{ p.username }}</td></tr>
    {% endfor %}
</table>
```
