/*
Title: Membuat Record Baru
Sort: 6
*/

Berikut ini cara create data record baru dengan propel.

Buat route:
```
// Mengambil satu data melalui filter LIKE
$app->get('/cobapropelcreatepengguna', 'MyApp\Coba::propelCreatePengguna');
```


Tambahkan:
```
    public function propelCreatePengguna(Request $request, Application $app) {

        try {

            $pengguna = new Pengguna();
            $pengguna->setPenggunaId("C90EC260-2320-11E4-9F93-0706A2D54CDD");
            $pengguna->setNama("Ahmad Banu");
            $pengguna->setUsername("banu");
            $pengguna->setPassword("354");
            $pengguna->save();

            // array("pengguna_id" => "C90EC260-2320-11E4-9F93-0706A2D54CDD", "nama" => "Ahmad Banu");
            // $pengguna->fromArray()

            return "Berhasil mengisi pengguna baru. Lihat di <a href='/cobapropelgetall'>daftar</a>";

        } catch (\Exception $e) {

            return "Gagal membuat pengguna. Error: ".$e->getMessage();

        }

    }
```