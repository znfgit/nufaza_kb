/*
Title: Menghapus Record
Sort: 7
*/

Berikut ini cara menghapus data melalui propel

Tambahkan routenya di app.php:
```
// Menghapus data
$app->get('/cobapropeldeletepengguna', 'MyApp\Coba::propelDeletePengguna');
```


Tambahkan controllernya di Coba.php:
```
    public function propelDeletePengguna(Request $request, Application $app) {

        try {

            $pengguna = PenggunaPeer::retrieveByPK("C90EC260-2320-11E4-9F93-0706A2D54CDD");
            $pengguna->delete();

            return "Berhasil menghapus pengguna. Lihat di <a href='/cobapropelgetall'>daftar</a>";

        } catch (\Exception $e) {

            return "Gagal menghapus pengguna. Error: ".$e->getMessage();

        }

    }
```
Tambahkan di penggunaspropel.twig
```
<a href="/cobapropelcreatepengguna">Buat pengguna baru</a>
<a href="/cobapropeldeletepengguna">Hapus pengguna baru</a>
```