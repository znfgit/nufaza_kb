/*
Title: Retrieve Satu Data
Sort: 4
*/


Berikut ini cara mengambil satu data pengguna melalui primary keynya.

Buat dulu routenya:
```
// Mengambil satu data melalui primary key
$app->get('/cobapropelgetone', 'MyApp\Coba::propelGetOne');
```


Tambahkan di Coba.php
```
    public function propelGetOne(Request $request, Application $app) {

        // Ambil penggunaId
        $penggunaId = $request->get('pengguna_id');

        // If empty
        if (!$penggunaId) {
            return "Mohon pilih penggunaId";
        }

        // Load penggunas
        $c = new \Criteria();
        $c->add(PenggunaPeer::PENGGUNA_ID, $penggunaId, \Criteria::EQUAL);
        $pengguna = PenggunaPeer::doSelectOne($c);

        $penggunaId = $pengguna->getPenggunaId();
        $nama = $pengguna->getNama();
        $username = $pengguna->getUsername();

        //print_r($pengguna); die;
        $outStr = "Anda memanggil<br>ID: {$penggunaId}<br>Nama: {$nama}<br>Username: {$username}";
        return $outStr;
    }
```