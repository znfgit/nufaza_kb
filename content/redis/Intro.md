/*
Title: Intro
Sort: 1
*/ 


### Pengenalan

Redis adalah sebuah suatu sistem penyimpanan data in-memory yang digunakan sebagai database, cache dll. 
Mendukung struktur data berupa string, hash, list, set, sorted set dengan query range, dll. 

Redis didesain untuk sangat simpel dan berorientasi performance sehingga super cepat dan sangat responsif. 
Oleh karena itu, sebagai NoSQL - Key Value storage, struktur data di Redis sangat sederhana. 
Mekanisme penyimpanan dan query data pada Redis dengan demikian jauh berbeda dengan RDBMS biasa. 
Bagi programmer RDBMS pada umumnya akan dirasakan cukup counter intuitive.

Tantangannya adalah bagi application dan data architect dan programmernya, bagaimana caranya memenuhi kebutuhan aplikasi 
dengan memetakan struktur datanya untuk diterapkan di redis.  

Serangkaian tutorial ini berusaha membimbing anda agar dapat berkontribusi menggunakan redis.

### Mencicipi

Jika anda terburu-buru dan hanya ingin mencicipi redis dengan segera, tidak usah dulu melanjutkan halaman-halaman berikutnya dari tutorial ini. Cukup buka http://try.redis.io/ . Di web ini ada interface redis yang mensimulasikan server redis sebenarnya, dan melalui consolenya anda dapat mencoba fitur2 redis dan mengenalkan terminologi berikut:

-   SET dan GET value 
-   INCR (increment value)
-   EXPIRE dan TTL (expiration value secara otomatis)
-   List dan pengelolaannya dengan RPUSH, LPUSH, dan LRANGE
-   List length dengan LLEN, mengeluarkan anggota list dengan LPOP dan RPOP
-   Set dan pengelolaannya denga SADD, SREM, SISMEMBER, SMEMBERS and SUNION
-   Sorted Set dan pengelolaannya dengan ZADD, ZRANGE
-   Hashes dan pengelolaannya dengan HSET, HGETALL, HMSET, HGET, HINCRBY, HDEL


