/*
Title: Bulk Loading
Sort: 4
*/

Cara paling cepat untuk bulk loading atau mengisi data awal untuk redis adalah menulis code insert data dalam bentuk command2 redis
dan memasukkannya melalui pipelining.

Contoh syntax insert data:
```
SET Key0 Value0
SET Key1 Value1
...
SET KeyN ValueN
```

Contoh pipelining:
```
cat data.txt | redis-cli --pipe
```