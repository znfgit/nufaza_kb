/*
Title: Struktur Data
Sort: 3
*/ 

### Pendahuluan

Struktur data di Redis adalah sbb:
-   Key value store
-   List
-   Set
-   Sorted Set
-   Hashes
-   Bit array
-   HyperLogLogs

### Key Value

Key value store ini menyimpan data string binary save tentang apa saja. Kalau di programming mungkin sama dengan mendefinisikan global variable. Cara menggunakannya:

Menyimpan:

```
SET tahun_operasional 2015
```

Membaca:
```
GET tahun_operasional
```


### Lists

List ini merupakan kumpulan elemen string yg berurutan sesuai dengan urutan pengisiannya. Bisa dianggap juga Linked List
Fakta yang perlu diingat:

-   Di programming identik dengan 1 dimensional array. 
-   Sebagaimana array, penambahan/pengurangan member bisa push, pop. 
-   Bisa menghitung elemen

-   Mengisi dengan RPUSH akan menambah elemen di belakang.
    ```
    RPUSH absensi "Abah"
    RPUSH absensi "Omand"
    ```
    Hasil: Abah, Omand

-   Mengisi dengan LPUSH akan menambah elemen di depan
    ```
    LPUSH absensi "Wawa"
    LPUSH absensi "Teteh"
    ```
    Hasil: Wawa, Teteh, Abah, Omand

-   Menghitung jumlah member dengan LLEN
    ```
    LLEN absensi
    ```
    Hasil: 4

-   Mengambil daftar member list, hanya bisa dengan LRANGE, yaitu get dengan limit (tidak ada command untuk get all)
    ```
    LRANGE absensi 0,1
    ```
    Hasil: Wawa, Teteh
    ```
    LRANGE absensi 2,3
    ```


### Sets

Set atau himpunan, adalah kumpulan elemen string yang unik (tidak duplikasi) dan tidak berurutan.

-  Di database identik dengan entitas unik atau index
-  Penambahan pengurangan member dengan SADD, SREM
-  Set dapat melakukan operasi himpunan seperti UNION, INTERSECT, DIFF, 
-  Set dapat dihitung jumlahnya dengan SCARD


### Sorted Sets

Mirip dengan sets namun setiap elemen dikaitkan dengan suatu angka float disebut score.
Dengan demikian bisa diambil secara berurutan dengan range tertentu, misal 10 teratas, 10 terbawah.


### Hash

Hash merupakan data yang terdiri dari field-field, mirip dengan hash di ruby atau python atau associative array di php.


### Bit Arrays

Nilai string dapat diperlakukan sebagai array terdiri dari bit-bit. Ada beberapa perintah yang memungkinkan pengaturan setiap bit, misal
set, clear, count yang nilainya 1, cari bit pertama yang set atau tidak dll.

