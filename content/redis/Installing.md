/*
Title: Instalasi
Sort: 2
*/ 

Agar tutorial kita langsung paham, install dulu versi windows redis dengan mengunduh di sini
https://github.com/MSOpenTech/redis/releases

Hanya tersedia versi binary yang 64 bit di situ, mungkin agar redis mampu memanfaatkan RAM > 2GB.

Jalankan instalasinya. Tidak usah mengubah setting apa-apa pada parameter default instalasi redis yang ada 
(IP dan port dll) untuk memudahkan kita nanti. Jawab yes yes saja untuk pertanyaan firewall dan service agar 
redis sudah langsung terinstal berupa service.

Karena 64 bit, instalasi akan menginstal redis di c:\Program Files\Redis\. Setelah instalasi selesai, 
dengan cmd masuklah ke direktori  tersebut, kemudian jalankan di command prompt redis-cli.

Maka akan muncul prompt 
```
127.0.0.1:6379> 
```

Untuk mencobanya ketiklah

```
> SET salam "Hello world" 

> GET salam
```

