/*
Title: Extention Google Chrome for Trello
Sort: 2
*/ 

### Extension Google Chrome yang wajib di-install

Gunakan _browser_ Google Chrome untuk membuka Trello. Extension chrome yang memberi fungsi-fungsi tambahan agar Trello lebih mudah digunakan sudah banyak. Di Nufaza ada beberapa extension chrome yang wajib diinstall untuk meningkatkan kinerja Trello.


#### Card Counter for Trello

Fungsinya untuk menampilkan jumlah _card_ di dalam setiap _list_. Dapat diunduh pada link di bawah ini:   
https://chrome.google.com/webstore/detail/cardcounter-for-trello/miejdnaildjcmahbhmfngfdoficmkdhi?utm_source=chrome-app-launcher-info-dialog

Tampilan sebelum diinstall
[![trello_card_counter_1](../images/trello_ext_1_card_counter_before.png)](../images/trello_ext_1_card_counter_before.png) 

Tampilan sesudah diinstall pada list tampil jumlah kartu
[![trello_card_counter_2](../images/trello_ext_2_card_counter_after.png)](../trello_ext_2_card_counter_after.png)


#### Cards View Filter for Trello

Kegunaan extension ini ialah untuk menampilkan filter list pada tampilan kartu. Untuk instalasi dapat klik pada tautan berikut ini:  
https://chrome.google.com/webstore/detail/cards-view-filter-for-tre/dlhlalojbpdheikhbpnbpmgidllcejeb?hl=en

Tampilan sebelum instalasi  
[![trello_card_view_filter_1](../images/trello_ext_3_card_view_filter_1.png)](../images/trello_ext_3_card_view_filter_1.png)  
Kartu di sorting berdasarkan board atau due date sehingga sulit mengetahui mana yang sedang dikerjakan.
 
Tampilan sesudah instalasi  
[![trello_card_view_filter_2](../images/trello_ext_4_card_view_filter_2.png)](../images/trello_ext_4_card_view_filter_2.png)  

Tampilan ketika difilter doing saja  
[![trello_card_view_filter_3](../images/trello_ext_5_card_view_filter_3.png)](../images/trello_ext_5_card_view_filter_3.png)  


#### Export for Trello

fungsinya untuk menambah fungsi ekspor xls ke trello. Dapat didapatkan di link berikut ini:  
https://chrome.google.com/webstore/detail/export-for-trello/nhdelomnagopgaealggpgojkhcafhnin?hl=en

Tampilan sebelum instalasi
[![trello_export_xls_1](../images/trello_ext_6_export_xls_1.png)](../images/trello_ext_6_export_xls_1.png)

Tampilan sesudah instalasi
[![trello_export_xls_2](../images/trello_ext_7_export_xls_2.png)](../images/trello_ext_7_export_xls_2.png)


#### List Layout for Trello

Penambahan _extention_ ini akan memberikan tampilan layout _board_ Trello menjadi dua baris. Untuk instalasi dapat 
https://chrome.google.com/webstore/detail/list-layouts-for-trello/aldklnbenbdgfgfbflalmlddkkndgnlc?hl=en

Tampilan sebelum instalasi
[![trello_list_layout_1](../images/trello_ext_8_list_layout_1.png)](../images/trello_ext_8_list_layout_1.png)
 
Tampilan sesudah diinstall ada simbol baru di sebelah kanan tanda lonceng. Apabila diklik maka list akan tampil menjadi dua row.
[![trello_list_layout_2](../images/trello_ext_9_list_layout_2.png)](../images/trello_ext_9_list_layout_2.png)

Ada sedikit bug jadi usahakan refresh dulu agar simbol ini muncul.


#### Trelabels for Trello

Extension ini berfungsi untuk menampilkan judul label di setiap kartu trello. Dapat didapatkan di link:  
https://chrome.google.com/webstore/detail/trelabels-for-trello/annjdmkbhchmobehkcfilecnlhibedbj?hl=en  

Sebelum install extension label hanya tampil sebagai warna  
[![trello_trelabels_1](../images/trello_ext_10_trelabels_1.png)](../images/trello_ext_10_trelabels_1.png)

Setelah menggunakan extension maka akan tampil simbol label di sebelah kanan kotak search  
[![trello_trelabels_1](../images/trello_ext_11_trelabels_2.png)](../images/trello_ext_11_trelabels_2.png)  
Klik pada tanda label

Maka akan tampil judul proyek tidak hanya warnanya saja  
[![trello_trelabels_1](../images/trello_ext_11_trelabels_3.png)](../images/trello_ext_11_trelabels_3.png)