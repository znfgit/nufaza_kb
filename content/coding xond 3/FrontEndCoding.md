/*
Title: Front End Coding
Sort: 2
*/

Daftar Isi:

-   [Memahami Komponen](#memahami-komponen)
    *   [Konsep](#konsep)

    *   [Komponen Bawaan Extjs ](#komponen-bawaan-extjs)

-   [Bermain dengan Sencha Fiddle](#bermain-dengan-sencha-fiddle)

-   [Membuat Modul](#membuat-modul)

-   [Membuat Komponen](#membuat-komponen)

-   [Memahami Layout](#memahami-layout)

-   [Memahami Akses Komponen dan Component Query](#memahami-akses-komponen-dan-component-query)

-   [Memahami Event Listeners](#memahami-event-listeners)

-   [Menangkap Global Parameters dari Backend](#menangkap-global-parameters-dari-backend)

-   [Membuat dan Menambah Item pada Menu](#membuat-dan-menambah-item-pada-menu)
    *   [Menambahkan Item pada Menu Berbasis ](#menambahkan-item-pada-menu-berbasis-toolbar)

    *   [Menambahkan Item pada Menu Berbasis Tree](#menambahkan-item-pada-menu-berbasis-tree)

    *   [Mengaktifkan Menu Berbasis Tree](#mengaktifkan-menu-berbasis-tree)    

-   [Menggunakan Form Grid Hasil Generate](#menggunakan-form-grid-hasil-generate-pada-tab)
    *   [Membuat Grid Menggunakan Komponen Generated](#membuat-grid-menggunakan-komponen-generated)

    *   [Membuat Form Menggunakan Komponen Generated](#membuat-form-menggunakan-komponen-generated)

-   [Memfilter STORE](#memfilter-store)


###  Memahami Komponen
     
#### Konsep

Setiap obyek di ExtJS 5 adalah komponen. Secara umum komponen dapat dibagi 2:

Ada 2 Jenis komponen:

1.  Reusable component  --> biasanya controller global
    *   sebuah komponen yang memiliki fungsi khusus yg dapat dipakai ulang di beberapa komponen kompleks ataupun modul
    *   turunan dari satu komponen eksisting, atau disusun dari sedikit komponen lain
    *   biasanya controllernya global

2.  Module: 
    *   sebuah modul yang merupakan bagian fungsional suatu aplikasi
    *   terdiri dari beberapa _reusable component_ yang sederhana maupun kompleks
    *   biasanya controllernya local / viewcontroller

Setiap komponen memiliki:

-   Config: 'setting yang dipakai hanya pada saat penciptaan komponen'
-   Properties: 'setting yang bisa diubah selama komponen hidup'
-   Method: 'kelakuan atau pekerjaan yg bisa kita perintahkan pada komponen'
-   Event: "kejadian / even yang terjadi terhadap suatu komponen yg dapat ditangkap untuk ditindaklanjuti"


Sebagai analogi, sebuah motor memiliki:

-   config:
    *   jenis: 'matic'
    *   type: 'Beat'
-   properties:
    *   nyala: true / false
    *   kecepatan: 0 - 100
-   method:
    *   stater()
    *   puterGas()
-   event:
    *   onNyala
    *   onGas

Contoh code analogi :
```
    motor.on('gas', function(motor,  persenGas) { 
        var kecepatan = (motor.rpm * persenGas * konstanta) + kecepatan;    
        motor.kecepatan = kecepatan;
    });
```

####   Komponen Bawaan Extjs 

ExtJS sangat kaya dengan komponen bawaan. Lihat semuanya di sini:
http://docs.sencha.com/extjs/5.0/5.0.0-apidocs/

Untuk cara menggunakannya dapat browsing sampel-sampel berikut:
http://dev.sencha.com/ext/5.0.0/examples/index.html

###   Bermain dengan Sencha Fiddle

Kita dapat membuat code dengan produk-produk sencha tanpa harus mempersiapkan environment terlebih dahulu. 
Caranya dengan menggunakan Sencha Fiddle http://fiddle.sencha.com. Pilih terlebih dahulu Framework yang akan dipakai,
kemudian tambahkan kode anda di blok `launch: function() { .... }`


###   Membuat Modul

Step by Step membuat modul baru:

1.  Buat tombolnya di `web/app/view/Main/Main.js` (copas dari tombol yg ada)
2.  Ubah judul dan logo tombol (glyph)
3.  Buat Modul nya di : `web/app/view/NamaModul.js`


Daftar script contoh:

-   Script tombol (di `Main.js`, bagian switch menu):   

    ```
    case 'modulbaru':
        me.searchAddActivateTab('modulbaru', 'Modul Baru');
        break;
    ```

-   Di `Main.js` bagian `me.tabPanel > toolbar` items:   

    ```
    }, {
        iconCls: null,
        glyph: 'xf17b@FontAwesome',
        xtype: 'button',
        text: 'Modul Baru',
        iconAlign: 'top',   // Alternatives: 'top', 'left'
        scale: 'medium',
        handler: function(){
            me.switchMenu('modulbaru');
        }
    ```

-   Contoh modul super sederhana (panel html) di `web/app/view/ModulBaru.js`

    ```
    Ext.define('MyApp.view.ModulBaru', {
        extend: 'Ext.panel.Panel',
        //alias: 'widget.modulbaru',
        //controller: 'modulbaru',
        xtype: 'modulbaru',        
        initComponent: function() {

            // Alternatif: langsung tempel ke this. Kurang bagus utk modul.        
            //this.title = 'Modul Baru';
            //this.html = 'Ini isi html modul baru';
            
            this.items = {
                xtype: 'panel',
                title: 'Modul Baru',
                html: 'Ini isi html modul baru'
            };
            
            this.callParent(arguments);
        }
    });
    ```

###  Membuat Komponen

Step by Step membuat komponen baru:

1.  Buat komponennya di `web/app/view/_components`
2.  Buat isinya, extend dari komponen pilihan anda
3.  `cmd > sencha app refresh`
4.  gunakan di modul anda sendiri

Daftar script contoh:

-   Contoh script komponen `(web/app/_components/TanggalanMini.js)`:
    ```
    Ext.define('MyApp.view._components.TanggalanMini', {
        extend: 'Ext.panel.Panel',
        alias: 'widget.tanggalanmini',
        
        initComponent: function() {
            
            var d = new Date();
            var tanggal = d.toString();

            this.items = {
                xtype: 'panel',
                title: 'Tanggal Hari Ini',
                html: tanggal
            };

            this.callParent(arguments);
        }
    });
    ```

-   Contoh layout (update `ModulBaru.js` di atas):
    ```
    Ext.define('MyApp.view.ModulBaru', {
        extend: 'Ext.panel.Panel',
        xtype: 'modulbaru',
        
        initComponent: function() {

            this.layout = {
                type: 'vbox',
                align: 'stretch'
            };

            this.items = [{
                xtype: 'tanggalanmini',
                flex: 1
            },{
                xtype: 'panel',
                flex: 4,
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [{
                    title: 'Komponen Bawaan',
                    html: 'Ini isi html modul baru',
                    flex: 1
                },{
                    xtype: 'tanggalanmini',
                    flex: 1
                }]
            }];
            
            this.callParent(arguments);
        }
    });
    ```


###   Memahami Layout

Layout adalah poin yang sangat penting di Extjs, karena berkaitan dengan penampilan aplikasi secara keseluruhan.
Ada beberapa jenis layout yang sangat sering digunakan di ExtJS:

1.  Border layout ('border') --> lazimnya untuk layout aplikasi di depan atau ViewPort, kadang dipakai di modul juga. Keunggulannya bisa diresize.
2.  Flexible horizontal box ('hbox') --> untuk dua komponen yang berderet horizontal.
3.  Flexible vertical box ('vbox') --> untuk dua komponen bertumpuk secara vertikal.

Contoh-contoh cara penggunaan layout dapat anda cek secara komperhensif di:
http://dev.sencha.com/ext/5.0.0/examples/kitchensink/#layouts

###  Memahami Akses Komponen dan Component Query

Dalam aplikasi terjadi komunikasi antar komponen baik manual melalui interaksi pengguna maupun otomatis secara kodifikasi. Jurus-jurus akses komponen
yang clean sangat diperlukan agar reusable component yang kita buat bersih dan dapat dipakai kapan saja oleh siapa saja. Akses komponen dapat dilakukan
dengan method sesimpel `up(), down()` dikombinasikan dengan `Component Query`.

Component query adalah fasilitas navigasi/pencarian komponen untuk mendapatkan suatu komponen sasaran dari komponen lain yang sedang menjadi player.
Dokumentasi jurus lengkap `ComponentQuery` dapat ditemukan dengan searching "ComponentQuery" di APIDocs atau langsung online di URL berikut:
[buka](http://docs.sencha.com/extjs/5.0/5.0.0-apidocs/#!/api/Ext.ComponentQuery)


###  Memahami Event Listeners

Event dapat dianalogikan suatu kejadian, dan `listener` bisa dianggap sebagai pemantau kejadian. Analoginya misal hakim garis. Anggaplah si hakim garis ditugaskan
untuk memantau garis belakang. Hakim garis diberi instruksi jika terjadi jatuh cock di belakang garis, dia harus berteriak "out".

Pseoudocode analoginya seperti ini:
```
    // Ciptakan hakim garis
    var hakimGarisUtara = new HakimGaris({
        nama: 'Baim'
    });

    // Ciptakan lapangan, ambil utara nya dulu
    var lapangan = new Lapangan();
    var garisBelakangUtara = lapangan.getGarisBelakangUtara();
    
    // Tugaskan hakim tadi
    garisBelakangUtara.hakim = hakimGarisUtara;

    garisBelakangUtara.on('jatuhcock', function(garisbelakang, posisiJatuh){
        
        // Cock jatuh di depan garis
        if (posisiJatuh == 'depan') {
            garisbelakang.hakim.tunjukBawah();
        } else {
            garisbelakang.hakim.angkatTangan();
        }
    });
```

Contoh kode (tambahkan di modul tadi):

-   Tombol  with component query
   
```
    this.tbar = [{
        text: 'Ganti isi',
        handler: function(btn) {
            // Jangan ditiru, ke depan simpan di ViewController
            // Ext.Msg.alert('Info', 'Tombol tertekan');
            // console.log('Tombol tertekan');
            var panelbawaan = btn.up('modulbaru').down('panel[itemId="panelbawaan"]');
            panelbawaan.update('Panel bawaan sudah diubah isinya');
        }
    },{
        text: 'Disable Komponen Bawaan',
        handler: function(btn) {
            // Jangan ditiru, ke depan simpan di ViewController
            // Ext.Msg.alert('Info', 'Tombol tertekan');
            // console.log('Tombol tertekan');
            var panelbawaan = btn.up('modulbaru').down('panel[itemId="panelbawaan"]');
            panelbawaan.disable();
        }
    
    }];
```

-   Tempelkan `event listener` ke obyek mana saja
   
```
    items: [{
        title: 'Komponen Bawaan',
        itemId: 'panelbawaan',
        html: 'Ini isi html modul baru',
        flex: 1,
        listeners: {
            disable: function(panel, eOpts) {
                Ext.Msg.alert('Info', 'Panel bawaan dilumpuhkan!');
            }
        }
    },{
```


###   Menangkap Global Parameters dari Backend

Hampir setiap aplikasi memerlukan ini, khususnya untuk mengirimkan berbagai parameter dan status aplikasi terkait loginnya.
Ada beberapa pendekatan dan trik untuk menyelesaikan masalah ini, di antaranya:

1.  Mengirimkan Ajax di FrontEnd dan route di backend yang mengirimkan data sesi.

    Contoh:

    -   Kode backend (`app/app.php`)

        ```
        $app->get('/cekLogin', 'MyApp\Auth::cekLogin');
        ```

    -   Kode backend (`src/MyApp/Auth.php`)

        ```
        namespace MyApp;

        use Silex\Application;
        use Symfony\Component\HttpFoundation\Request;
        use Symfony\Component\Security\Acl\Exception\Exception;

        use MyApp\Model;
        use MyApp\Model\PenggunaPeer;

        class Auth {

            public function cekLogin (Request $request, Application $app) {
        
                $user = $app['security']->getToken()->getUser();

                if ($app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {

                    $username = $user->getUsername(); 
                    
                    $c = new \Criteria();
                    $c->add(PenggunaPeer::USERNAME, $username);
                    $logins = PenggunaPeer::doSelectOne($c);
                        
                    $app['session']->set('user', $username);
                    $app['session']->set('nama', $logins->getNama());

                    $username = $app['session']->get('user');
                    $nama = $app['session']->get('nama');
                
                    if ($username) {
                        return "{ success:true, username: '$username', nama: '$nama', message: 'Session exists for user $username' }";
                    } else {
                        return "{ success:false, message: 'No session' }";
                    }
                }
            }

            // fungsi2 lain 
            // ...
            //
        }

        ```

    -   Kode Frontend (`web\app\Application.js`):

        ```
        launch: function () {
            
            // TODO - Launch the application
            Ext.setGlyphFontFamily("FontAwesome");
            Ext.setGlyphFontFamily("font-fileformats-icons");
            Ext.setGlyphFontFamily("font-icomoon");

            // Loader for ux & lib
            Ext.Loader.setPath('Ext.ux', 'app/ux');
            
            // Setup Ext Direct
            //Ext.direct.Manager.addProvider(Ext.app.REMOTING_API);

            // Initialize tooltip
            Ext.tip.QuickTipManager.init();
            
            Ext.Ajax.request({
                waitMsg: 'Menyimpan...',
                url: '/cekLogin',
                method: 'GET',
                failure:function(response,options){
                    Ext.Msg.alert('Warning','Response : ' + response );
                },
                success: function(response,options){
                    var json = Ext.util.JSON.decode(response.responseText);
                    if (json.success == true) {
                        MyApp.username = json.username;
                        MyApp.nama = json.nama;
                        var tentang_app = Ext.getCmp('tentang_app');
                        console.log(tentang_app);
                        tentang_app.update(
                            'Username: '+ MyApp.username + '<br>' +
                            'Nama: '+ MyApp.nama + '<br>' +
                        );
                    } else if (json.success == false){

                    }
                }
            });
        }

        ```

2.  Menyimpan cookies di sisi backend, dibaca menggunakan `CookieProvider` di frontend
    
    -   Kode backend (`app.php`)

        ```
        $app->before(function (Request $request) use ($app){
            
            $token = $app['security']->getToken();
            
            if (null !== $token) {
                
                $user = $token->getUser();
                
                if (is_object($user)) {
                
                    // You can, for example, find your user in your database with username given..
                    // then send it to the backend
                    
                    // Create an instance of ExtCookieGenerator
                    $cGen = new ExtCookieGenerator();
                    
                    // Write it to the cookie.
                    // It will automagically appear in the front end. Just call it for example:
                    // var cp = new Ext.state.CookieProvider();
                    // Ext.state.Manager.setProvider(cp);
                    // console.log(cp.get('username'));
                    $cGen->set("username", $user->getUsername());

                    // Write your other cookies here
                    $person = PenggunaQuery::create()->findOneByUsername($user->getUsername());
                    $wilayah = MstWilayahQuery::create()->findOneByKodeWilayah($person->getKodeWilayah());
                    
                    if ($wilayah->getIdLevelWilayah() == 1) {
                    
                        $namaPropinsi = $wilayah->getNama();
                        $namaKabKota = "-";
                    
                    } else if ($wilayah->getIdLevelWilayah() == 2) {
                    
                        $parentWilayah = MstWilayahQuery::create()->findOneByKodeWilayah($wilayah->getMstKodeWilayah());
                        $namaKabKota = $wilayah->getNama();
                        $namaPropinsi = $parentWilayah->getNama();
                    
                    } else {
                    
                        $namaKabKota = "-";
                        $namaPropinsi = "-";
                    
                    }
                    
                    $cGen->set("pengguna_id", $person->getPrimaryKey());
                    $cGen->set("peran_id", "{$person->getPeran()->getPeranId()}");
                    $cGen->set("kode_wilayah", $person->getKodeWilayah());
                    
                    $cGen->set("nama_operator", $person->getNama());
                    $cGen->set("nama_peran", $person->getPeran()->getNama());
                    $cGen->set("nama_propinsi", $namaPropinsi);
                    $cGen->set("nama_kabupaten_kota", $namaKabKota);
                    
                    // Penting untuk feedback ke xond3
                    $app["xond_user"] = $person;

                } else {

                    // Otherwise clean the cookies
                    $cGen = new ExtCookieGenerator();
                    $cGen->removeAll();

                }

            }

        });
        ```

    -   Kode front end

        ```
        Ext.define('Simpak.controller.Main', {
            extend: 'Ext.app.Controller',
            init: function() {
                this.control({
                    'app-main': {
                        afterrender: this.onMainStart
                    },
                    'mstwilayahsearch': {
                        select: this.onMstWilayahSearchSelect
                    }
                });

                this.callParent(arguments);
            },

            onMainStart: function(main) {
                
                var me = this;

                // ...
                me.registerUser();
                // ...
            },

            registerUser: function() {
                
                var me = this;

                // Prepare parameters from cookies
                var cp = new Ext.state.CookieProvider();
                Ext.state.Manager.setProvider(cp);
                
                console.log(cp);

                // User data
                var data = {
                    username: cp.get('username'),
                    pengguna_id: cp.get('pengguna_id'),
                    peran_id: parseInt(cp.get('peran_id')),                             // Integers doesn't transfer well with cookies
                    user_kode_wilayah: cp.get('kode_wilayah').replace('++', '  '),      // Some shit changes spaces to pluses.
                    kode_wilayah: cp.get('kode_wilayah').replace('++', '  ')            // Some shit changes spaces to pluses.
                };

                Ext.apply(Simpak, data);

                Ext.apply(data, {
                    nama_operator: cp.get('nama_operator'),
                    nama_peran: cp.get('nama_peran'),
                    nama_propinsi: cp.get('nama_propinsi'),
                    nama_kabupaten_kota: cp.get('nama_kabupaten_kota')
                });

                if (in_array(Simpak.peran_id, [1, 41, 42])) {
                    Simpak.is_pusat = true;
                }

                me.userfs.getViewModel().setData(data);

            },

        ```


###   Membuat dan Menambah Item pada Menu

Ada dua jenis Menu Modul Aplikasi yang dapat digunakan pada aplikasi di lingkungan Xond 3. 

1.  Menu berbasis toolbar, digunakan untuk
    -   aplikasi yang masih tipis
    -   aplikasi yang modulnya sedikit tapi dalam

2.  Menu berbasis tree.
    -   aplikasi yang sudah tebal dan besar
    -   aplikasi yang modulnya banyak

Biasanya awal pembuatan aplikasi, digunakan menu berbasis toolbar. Lama kelamaan diubah menjadi `tree`.
Kode contoh untuk kedua jenis toolbar tersedia di Xond 3, dan dapat dipilih dengan sedikit modifikasi.


####  Menambahkan Item pada Menu Berbasis Toolbar

Berikut contoh menu berbasis toolbar, perhatikan bagian kode berikut (search saja). Copy paste bagian array "items" untuk menambahkan:

```
// Create The Buttoned Tab Panel 
me.tabPanel = {
    region: 'center',
    xtype: 'tabpanel',
    id: 'main-tabpanel',
    tabPosition: 'bottom',
    dockedItems: [{
        dock: 'left', // Alternatives: 'left', 'top'
        xtype: 'toolbar',
        items: [{
            iconCls: null,
            glyph: '61461@FontAwesome',
            xtype: 'button',
            text: 'Dashboard',
            iconAlign: 'top',// Alternatives: 'top', 'left'
            scale: 'medium',
            handler: function(){
                me.switchMenu('dashboard');
            }
        }, {
            iconCls: null,
            glyph: 'xf0c0@FontAwesome',
            xtype: 'button',
            text: 'Kepegawaian',
            iconAlign: 'top',// Alternatives: 'top', 'left'
            scale: 'medium',
            handler: function(){
                me.switchMenu('gurukepegawaian');
            } 
//...dst...        
```

####  Menambahkan Item pada Menu Berbasis Tree

Perhatikan contoh Menu Berbasis Treee Berikut. Copy paste salah satu node children, dan untuk menu subitem, tambahkan saja config "children" di dalam node:

```
        // Create the TreePanel Menu
        me.menu = Ext.create('Ext.tree.Panel', {
            id: 'menu-tree-panel',
            flex: 1,
            rootVisible: false,
            useArrows: true,
            root: {
                text: 'Report',
                expanded: true,
                children: [{
                    text: 'Dashboard',
                    itemId: 'dashboard',
                    iconCls: 'fc-house',
                    leaf: true
                },{
                    iconCls: 'icon fc-user_suit',
                    text: 'Data Guru',
                    itemId: 'dataguru'
                }, {
                    iconCls: 'icon fc-share_workbook',
                    text: 'Penerimaan Berkas',
                    itemId: 'penerimaanberkas'
                }, {
                    iconCls: 'icon fc-point_gold',
                    text: 'Angka Kredit',
                    itemId: 'akusulan'
                    children: [{
                        iconCls: 'icon fc-point_gold',
                        text: 'Angka Kredit Usulan',
                        itemId: 'akusulan'
                    }, {
                        iconCls: 'icon fc-point_silver',
                        text: 'Angka Kredit Penilai I',
                        itemId: 'akpenilai1'
                    }, {
                        iconCls: 'icon fc-point_bronze',
                        text: 'Angka Kredit Penilai II',
                        itemId: 'akpenilai2'
                    }, {
                        iconCls: 'icon fc-points_small',
                        text: 'Angka Kredit Final',
                        itemId: 'akfinal'
                    }]
                }, {
                    iconCls: 'icon fc-accept_document',
                    text: 'Penetapan PAK Tahunan',
                    itemId: 'penetapanpaktahunan'
                }, {
                    iconCls: 'icon fc-printer',
                    text: 'Pencetakan PAK Tahunan',
                    itemId: 'pencetakanpaktahunan'
                }, {
                    iconCls: 'icon fc-setting_tools',
                    text: 'Pengaturan',
                    itemId: 'pengaturan'
                },{
                    text: 'Keluar Aplikasi',
                    itemId: 'keluar_aplikasi',
                    iconCls: 'fc-door_in',
                    leaf: true
                }]
            },

            listeners: {
                select: 'onMenuSelect'
            }
        }); 
```

####  Mengaktifkan Menu Berbasis Tree

Standard bawaan Installer Xond 3, tree yang didapat berbasis toolbar. Untuk menggantinya ke tree mudah saja.

1.  Disable config `DockedItems` di bagian TabPanel:

```
        // Create The Buttoned Tab Panel 
        me.tabPanel = {
            region: 'center',
            xtype: 'tabpanel',
            id: 'main-tabpanel',
            tabPosition: 'bottom',
            /* ---- perhatikan comment block ini
            dockedItems: [{
                dock: 'left',       // Alternatives: 'left', 'top'
                xtype: 'toolbar',
                items: [{
                    iconCls: null,
                    glyph: '61461@FontAwesome',
                    xtype: 'button',
                    text: 'Dashboard',
                    iconAlign: 'top',   // Alternatives: 'top', 'left'
                    scale: 'medium',                    
                    handler: function(){
                        me.switchMenu('dashboard');
                    }
                }, {
                    iconCls: null,
                    glyph: '61474@FontAwesome',
                    xtype: 'button',
                    text: 'Module 1',
                    iconAlign: 'top',   // Alternatives: 'top', 'left'
                    scale: 'medium',                    
                    handler: function(){
                        me.switchMenu('sample_module1');
                    }
                }, {
                    iconCls: null,
                    glyph: '61474@FontAwesome',
                    xtype: 'button',
                    text: 'Module 2',
                    iconAlign: 'top',   // Alternatives: 'top', 'left'
                    scale: 'medium',
                    handler: function(){
                        me.switchMenu('sample_module2');
                    }
                }, '->', {
                    iconCls: null,
                    glyph: '61596@FontAwesome',
                    xtype: 'button',
                    text: 'Logout',
                    iconAlign: 'top',   // Alternatives: 'top', 'left'
                    scale: 'medium',                    
                    handler: function(){
                        me.switchMenu('keluar_aplikasi');
                    }
                }]
            }],

            // perhatikan closing comment block ini ---- */

            items:[ me.dashboard ]
        };
```

2.  Setelah itu ubah bagian code terakhir jadi seperti berikut:

```
        // Menu Panel (tambahkan tree menu pada panel sebagai bagian dari border layout)
        me.menuPanel = {
            xtype: 'panel',
            title: 'Menu',
            region: 'west',            // ------ bagian border panel
            width: 220,
            split: true,
            collapsible: true,
            layout: {
                type: 'border'
            },
            items: [{
                flex: 1,
                region: 'center',
                layout: 'fit',
                items: me.menu
            }]
        };

        // Menuless TabPanel
        me.items = [{
            region: 'north',
            xtype: 'component',
            padding: 15,
            height: 60,
            style: 'background-image: url(/resources/images/blurbg-blue.png); background-size: '+ Ext.getBody().getViewSize().width +'px 100px; width: 800px; height: 160px;',
            html: '<div class="appheader">SPM Dikdas</div>'
        }, me.menuPanel, me.tabPanel ];
```


###   Menggunakan Form / Grid Hasil Generate pada Tab

Xond 3 mengakselerasi pengembangan aplikasi dengan cara meng-generate berbagai komponen dengan mengikuti struktur basis data yang digunakan aplikasi.
Posisi komponen hasil generate: 

- Backend:  src\Info
- Frontend: web\app\view\_components\

#### Membuat Grid Menggunakan Komponen Generated

Step by stepnya adalah

-  Proses Generation

    1.  Buka Menu

    2.  Jalankan Reverse

    3.  Jalankan Build Model

    4.  Jalankan InfoGen

    5.  Jalankan FrontendGen

-   Troubleshooting:

    -   Reverse gagal kemungkinannya & solusinya:

        *   File build.reverse.properties belum disesuaikan dengan environment coding & databasenya
        *   Perbaiki semua file configurasi, kemudian coba lagi di command prompt

    -   Build gagal kemungkinannya & solusinya:

        *   Perbaiki semua tabel, jangan sampai ada yg tidak ber-primary-key
        *   Cek koneksi database

    -   File tidak ditemukan di generated "_components" folder

        *   Cek src/Info/*.php sesuai tableInfo masing2. Beri setting pelengkap agar 
            generator tahu bahwa tabel ybs harus dibuat komponennya. Contoh: tabel Guru
            diberi tahu bahwa tabel ybs adalah data, dan harus dibuat gridnya.

            Contoh:

            ```
            <?php
            namespace MyApp\Info;
            use Xond\Info\TableInfo;
            use MyApp\Info\base;

            /**
             * The TableInfo for Guru Table
             * 
             * @author Donny Fauzan <donny.fauzan@gmail.com>
             * @version $version$
             */
            class GuruTableInfo extends base\BaseGuruTableInfo
            {
                const CLASS_NAME = 'MyApp.Info.GuruTableInfo';

                public function __construct(){        
                    parent::__construct();        
                }
                
                public function setVariables() {
                    parent::setVariables();
                    
                    // Override below here!
                    $this->setIsData(1);
                    $this->setIsStaticRef(0);
                    $this->setCreateGrid(1);
                    
                    $this->getColumnByName('nama')->setLabel('Nama Guru');
                    $this->getColumnByName('nama')->setHeader('Nama Guru');
                    $this->getColumnByName('nama')->setColumnWidth(200);
                    
                    $this->getColumnByName('alamat')->setColumnWidth(200);
                    $this->getColumnByName('alamat')->setHeader('Alamat Rumah');
                    
                    $this->getColumnByName('nip')->setColumnWidth(180);
                    $this->getColumnByName('nip')->setHeader('NIP');

                    $this->getColumnByName('pg')->setColumnWidth(60);
                }
                
            }
            ```


-   Proses Penempelan pada Aplikasi

    1.  Buat suatu modul sebagai 'casing'-nya

        Contoh (web\app\view\GuruModule.js):

        ```
        Ext.define('MyApp.view.GuruModule', {
            extend: 'Ext.panel.Panel',
            alias: 'widget.gurumodule',
            controller: 'gurumodule',
            initComponent: function() {
                
                this.items = {
                    xtype: 'panel',
                    itemId: 'panelutama',
                    listeners: {
                        afterrender: 'onStart'
                    },
                    layout: {
                        type: 'fit'
                    },
                    title: 'Sample Xond Module',
                    defaults: {
                        border: true,
                        bodyPadding: 15
                    },
                    items: [{
                        xtype: 'gurugrid',          // Di sini grid generated dipanggil
                        title: 'Daftar G'
                    }]
                };

                this.callParent(arguments);
            }
        });
        ```

    2.  Tambahkan controllernya
        Buka /Menu kemudian  jalankan ControllerGen. Pilih tabel yang akan dibuat, simpan ke web/app/controller. Hasilnya seperti ini:
        ```
        Ext.define('MyApp.controller.Guru', {
            extend: 'MyApp.controller.base.Guru',
            init: function() {
                this.superclass.init.call(this);
            }
        });
        ```

    3.  Daftarkan Controllernya di web/app/Application.js
        Jika controller yang baru kita buat belum didaftarkan di sini, sampai kiamat juga gak akan nendang.

    4.  Jalankan refresh sencha untuk mendaftarkan komponen ke app.json
        ```
        c:\Projects\localhost\my_app\web> sencha app refresh
        ```


#### Membuat Form Menggunakan Komponen Generated

Mengedit melalui form menuntut tersedianya  SATU buah "record" yang dapat di-"tempelkan" padanya. Sebuah form hanya dapat menangani satu record dalam suatu waktu. Tidak seperti grid yg bisa menampilkan beberapa record di saat yg sama.

Dengan demikian, kita perlu membuat sebuah store yang menyediakan akses terhadap records.

Step by step:

1.  Pastikan form tergenerate. Jika belum, edit InfoGen, tambahkan:

    ```
        $this->setCreateForm(1);
    ```
    Di tableInfo yang diinginkan.

2.  Generate kembali FrontEndGen

3.  Jangan lupa buat Controller nya, yg extend dari base sesuai /Menu > ControllerGen

4.  Setelah itu buatlah formulir sebagai "casing" objek yang dimaksud.

    ```
    Ext.define('MyApp.view.FormGuru', {
        extend: 'Ext.Panel',
        xtype: 'modulguru',
        layout: 'fit',
        initComponent: function() {
            
            var me = this;
            var tbar = "{}";

            // Creating the form
            me.items = {
                xtype: 'formguru',
                title: 'Formulir Guru'
            };

            // Realize the layout
            this.callParent();

        }
    });
    ```

5.  Sampai titik ini, form belum mengandung data. Buatlah sebuah store, dan tambahkan controller untuk memuat store tersebut kemudian menempelkan recordnya ke dalam form.
    ```
        'formguru' : {
            afterrender: this.prepareForm
        },
    ```

6.  Tambahkan pengendalinya
    ```     
        prepareForm: function(form) {
                me.store = new MyApp.store.Guru();
                me.store.on('load', function(store){
                     var rec = store.getAt(0);
                     form.loadRecord(rec)
                });
    }
    ```

7.  Sampel hasil akhir controller (`web/app/controller/Guru.js`):

    ```
    Ext.define('MyApp.controller.Guru', {
        extend: 'MyApp.controller.base.Guru',
        init: function() {
            this.control({
                'gurugrid': {
                    afterrender: this.afterGridRender,
                    rowclick: this.rowClick
                },
                'guruform': {
                    afterrender: this.afterFormRender
                }               
            });
        },

        afterGridRender: function(grid, options){
            
            //Xond.msg('Info', 'Ketendang');
            grid.getColumnByName('pg').setText('Pangkat Golongan');
            grid.getColumnByName('pg').setWidth(100);
            
            grid.down('toolbar').add({
                text: 'Info',
                iconCls: 'fc-information',
                
                handler: function(btn){
                    
                    var selections = grid.getSelection();
                    
                    if (selections.length < 1) {
                        Xond.msg('Error', 'Mohon pilih satu baris!');
                        return;
                    }

                    var rec = selections[0];

                    Xond.msg('Info', 'Isi baris terpilih adalah ' + rec.data.nama + ' alamatnya di ' + rec.data.alamat);
                }
            });

            // Muat data
            //grid.store.load();

            // Good way
            grid.store.on('load', function(store){
                
                // Memuat store teratas
                // var rec = store.getAt(0);                
                // Xond.msg('Info', 'Isi baris pertama adalah ' + rec.data.nama + ' alamatnya di ' + rec.data.alamat);

                // Jurus mencari record berdasarkan field tertentu
                var idx = store.find('nip', '123012831023');
                var rec = store.getAt(idx);
                Xond.msg('Info', 'Isi baris pertama adalah ' + rec.data.nama + ' alamatnya di ' + rec.data.alamat);

            });

            grid.store.load();

            //var selections = grid.getSelection();

            // Stupid way 
            // setTimeout(function(){
            //  var rec = grid.getStore().getAt(0);
            //  Xond.msg('Info', 'Isi baris pertama adalah ' + rec.data.nama + ' alamatnya di ' + rec.data.alamat);             
            // }, 4000);

        },

        afterFormRender: function(form, options){
            
            // Correct scope
            var me = this;

            // Buat store, dan muat record pertama ke form ybs
            var store = Ext.create('MyApp.store.Guru');
            
            store.on('load', function(store){
                var rec = store.getAt(0);
                form.loadRecord(rec);
            });
            
            store.load();
            form.store = store;
            form.counter = 0;

            // console.log('Form render cek store: ');
            // console.log(me.store);

            // Add toolbar
            form.addDocked({
                xtype: 'toolbar',
                dock: 'top',
                items: [{
                    dock: 'top',                
                    text: 'Prev',
                    itemId: 'prevRec',
                    iconCls: 'fc-control_rewind_blue',
                    listeners: {
                        click: me.clickPrev
                    }
                },{
                    dock: 'top',
                    text: 'Next',
                    itemId: 'nextRec',
                    iconCls: 'ic-control_play_blue',
                    listeners: {
                        click: me.clickNext
                    }
                },{
                    dock: 'top',
                    text: 'Save',
                    itemId: 'save',
                    iconCls: 'fc-diskette',
                    listeners: {
                        click: me.formSave
                    }
                }]
            });
            //form.addDocked();
        },

        clickPrev: function(btn){
            
            var form = btn.up('guruform');
            var store = form.store;

            if (form.counter > 0) {
                form.counter--;
            }
            
            var rec = form.store.getAt(form.counter);
            form.loadRecord(rec);     

        },

        clickNext: function(btn){
            
            var form = btn.up('guruform');
            var store = form.store;

            if (form.counter <= form.store.getCount()) {
                form.counter++;
            }
            
            var rec = form.store.getAt(form.counter);
            form.loadRecord(rec);

        },     

        formSave: function(btn){

            Xond.msg('Info', 'Najong');

            var form = btn.up('guruform');
            // var store = form.store;
            
            var record = form.getRecord();
            var values = form.getValues(false,false,false,true);
            record.set(values);

            var store = record.store;
            store.sync();
            
        },

        rowClick: function(grid, record, tr, rowIndex, e, eOpts ){
            Xond.msg('Info', 'Anda menekan ' + record.data.nama);
        }

    });

    ```

### Memfilter store

Ada dua cara memfilter store, harus berhati-hati dan teliti dalam memilih alternatif yang dipakai.
1.  Filter store di front end.
    Harus keep in mind bahwa ini hanya memfilter records yang sudah diload ke store. Jika store nya paging, maka yang difilter hanya store di halaman yang sedang aktif.
2.  Filter store yg menyeluruh sampai backend, dengan REST filter.

#### Filter di front end

Filter sederhana (menggunakan satu kolom):

Panggillah storenya, kemudian:
```
store.filter('nama_kolom', nilai);
```
    
Banyak jurus lain utk filter di frontend, sehingga bisa multiple filter. Baca di Apidoc, kemudian kontribute di sini. Contoh:
-   Filter by Function (store.filterBy)  --> filter programatikal, bisa menulis kondisi komplek melibatkan beberapa kolom sekaligus.
-   Multiple filter (store.addFilter) --> bisa menambah beberapa filter sekaligus baik sederhana maupun fungsi

#### Membersihkan filter di front end

Jika filter yang ada ingin direset, bersihkan filternya:
```
store.clearFilter();
```


#### Filter dari backend

Ada banyak jurus filter backend yang dapat ditendang melalui front end. Intinya kita harus menyisipkan "params" pada saat store loading. Contoh simpelnya filtering dikirimkan sebagai parameternya:
```
store.reload({
    params: {
        'nama_field': nilai
    }
});
```

Ada banyak jurus filter backend selain filter "EQUAL". 

1.  Filter Array
    Digunakan jika kita ingin menampilkan dua kemungkinan atau lebih isi suatu field:
    Contoh:

    ```
    store.reload({
        params: {
            'nama_field': "[1,4]"
        }
    });
    ```

2.  Filter Greater/Less Than/Equal.
    
    Ada 4 jenis filter ini:    
    -   #isgreaterthan
    -   #isgreaterequal
    -   #islessthen
    -   #islessequal    

    Cara pemakaiannya:

    ```
    store.reload({
        params: {
            'pg#islessequal': 2
        }
    });
    ```

3.  Filter tanpa nilai, atau cek keadaan parameter. Ada 3 jenis filter ini:
    -   #ISNULL
    -   #ISNOTNULL
    -   #ISEMPTY

    Cara pemakaiannya:

    ```
    store.reload({
        params: {
            'pg': #ISNOTNULL
        }
    });
    ```

#### Paging dan Persistent Filter

Jika data anda mulai banyak, store akan mulai memisahkan data per halaman. Ini jadi masalah jika anda melakukan filter sederhana apalagi di front end. Untuk mengatasinya, jika data yg ditampilkan besar, gunakanlah backend filtering & sorting, serta gunakan persistent filter (Filter yang "Nempel".

Contoh penggunaan:

Tambahkan saat inisialisasi grid:

```
grid.store.setPageSize(4);

grid.store.on('beforeload', function(store){
    
    store.proxy.extraParams = {};

    if (me.field) {
        var criteriaStr = me.field + "#" + me.filterCriteria;
        var params = {};
        params[criteriaStr] = me.filterValue;
        console.log(params);
        Ext.apply(store.proxy.extraParams, params);
    }

});
```

terus ganti filtering di tombol:
```
filterGridByPgGreaterThan: function(store){
    
    var me = this;
    me.field = 'pg';
    me.filterCriteria = 'isgreaterthan';
    me.filterValue = '1';

    store.reload();
},

filterGridByPgLessEqual: function(store){

    var me = this;
    me.field = 'pg';
    me.filterCriteria = 'islessequal';
    me.filterValue = '2';

    store.reload();

},
```

Tambahan untuk clear filter backend:
```
clearGridFrontendFilter: function(store){
    
    var me = this;
    store.clearFilter();
    me.field = "";
    store.reload();
},

clearGridBackendFilter: function(store){
    
    var me = this;
    store.clearFilter();
    me.field = "";
    store.reload();
},
```


### Sorting

####  Local Sorting

Sorting sederhana (menggunakan satu kolom), panggillah storenya, kemudian:
```
store.sort('myField', 'DESC');

```

Sorting dengan beberapa field:
```
//sorting by multiple fields
store.sort([
    {
        property : 'age',
        direction: 'ASC'
    },
    {
        property : 'name',
        direction: 'DESC'
    }
]);

```

####  Remote sorting

Menuntut store reload, sorting dikirimkan sebagai parameternya:
```
store.reload({
    params: {
        'ascending': 'status'
    }
});
```

Catat bahwa persistent sorting juga berlaku sama dengan persistent filtering, yaitu harus diset di scope variabel dan diaktifkan setiap beforeload.


### Jurus

-   Mengcustom komponen generated yang akan dipakai             DONE
-   Mengaktifkan Controller untuk Form / Grid yang Dimaksud     DONE
-   Menggenerate Controller Extending Base                      DONE
-   Melaporkan General Controller pada Application.js           DONE
-   Menambah Tombol pada Komponen                               DONE
-   Membuat Controller untuk Tombol                             DONE
-   Mengubah Icon Tombol                                        DONE
    -   Menggunakan Glyph                                       DONE
    -   Menggunakan IconCls                                     DONE
        Melihat Referensi FontIcon 

-   Menambah Toolbar pada Komponen                              DONE
    -   Menambah Toolbar on the fly                             DONE
    -   Membuat Teks, Separator, dan Cornerer                   DONE
    -   Menambah Toolbar dengan Menimpa Config                  DONE

-   Menggunakan dan Memuat Store & Grid
    -   Membuat Grid                                            DONE
    -   Mengisi Grid data dengan Store Sederhana (ArrayStore)   DONE
    -   Mengisi Grid dengan Data Ajax.                          DONE
    -   Memfilter Store frontend                                DONE
    -   Memfilter store backend                                 DONE
        = Exact match   DONE
        = wildcard      DONE
        = Not null,     -
        = Filter dengan array (IN)

-   Rest sederhana
    -   Filtering using normal variables (EQUAL)
    -   Filtering using array (IN)
    -   Filtering using wildcard (LIKE)
    -   Sorting
    -   Paging



####  Menimpa (Override) Generated Component Sebelum Dipakai

### Kembangan
####  Menambahkan Parameter Awal untuk Form / Grid yang Dimaksud

    
####  Menambahkan data pada store                                SKIP
##### Menambahkan data baru dengan generated ID                  SKIP
##### Menambahkan data baru dengan composite ID                  SKIP
##### Menambahkan data baru dengan existing ID                   SKIP
####  Mengoverride store --> BAD IDEA !!                         SKIP

####  Membuat instance store dengan config tertentu.
####  Menyetting kolom Pada grid


###   Menggunakan dan Membuat RestModules

####  Rest advanced
##### Meng-override column melalui restconfig
##### Menyajikan hiearchial & summarical data melalui restconfig
##### Menyetting Printing
##### Menyetting Excel Download
##### Menyajikan CrossTab dengan restconfig

###   Manajemen Layout & Modul/Tab Utama
####  Set Startup routines
##### Intro to ViewControllers
####  Menambahkan Komponen Hasil Generate pada Toolbar Utama
####  Menambahkan Komponen Hasil Generate pada Tree Menu
####  Membuat floating component


##### Overriding default "query" of a combo, enabling it to be searched by other column
##### Wiring event from a child and custom components (case FormSearchPtk)

##### Memanggil viewcontroller modul sebelah
##### Prinsip 1 fungsi  1 kerjaan. Sehingga bisa di-share pakai / dipanggil komponen lain


##### Overriding table info, creating virtual FK & the corresponding xtype combo 
##### Loading a tree

##### Creating a tree
##### changing tree icons

##### Enabling virtual FK (combo and stuff)

##### dynamically adding fields to model

##### wrap grid

##### Defining small ref, static ref, bigref


###### Change locale extjs : http://docs.sencha.com/extjs/5.0/core_concepts/localization.html

###### setloading

###### "and" "or" store filtering ? use backend and frontend filter

###### Membuat

###### mensimulasikan click menu dari interface
###### mensimulasikan click grid programmatically (example di simpak/web/app/controller/DataGuru.js)
