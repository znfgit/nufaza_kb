/*
Title: Coding Convention
Sort: 1
*/ 

Agar terjadi keseragaman dalam penyusunan code sehingga semua anggota tim merasa familiar
dengan code yang dibuat bersama, maka diperlukan kesepakatan code convention.


### Variable Naming
Naming constants
Naming global variable
Naming array

### Database Naming Convention
Naming tables
Naming columns
Naming relations

### User Interface
Naming search combo
Naming filter parameter form
Naming modules

