/*
Title: Array Looping
Sort: 3
*/

Twig dapat menampilkan array, baik array of associative arrays ataupun array of objects. Cara menampilkannya sama persis.

Buat dulu routenya:
```
// Mencoba twig array
$app->get('/cobatwigarray', 'MyApp\Coba::twigArray');
```

Tambahkan di Coba.php
```
    public function twigArray (Request $request, Application $app)
    {
        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__).D."templates";
        $fileName = "penggunas.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader);

        $penggunas = array("Abah", "Banu", "MGI", "Arfan");

        $data = array(
            "penggunas" => $penggunas
        );

        $outStr = $twig->render($fileName, $data);

        return $outStr;
    }
```

Buat twig templatenya `C:\Projects\localhost\my_app\src\MyApp\templates\penggunas.twig`:

```
Daftar pengguna di sistem ini adalah sbb:

{% for p in penggunas %}
<br>- {{ p }}

{% endfor %}
```

