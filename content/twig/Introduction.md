/*
Title: Introduction
Sort: 1
*/

## What

Twig adalah templating engine bawaan Silex.

## Why

Twig dipilih karena native dalam Silex. Twig juga cukup powerfull, small footprint dan cukup lengkap.

## Instalasi

Instalasi twig sama dengan silex, melalui composer. Utk Xond3, instalasi twig sudah otomatis melalui wizard.

