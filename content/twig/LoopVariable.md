/*
Title: Loop Variable
Sort: 6
*/


Untuk looping, ada variabel yang mungkin bermanfaat untuk template anda. 
Nama variabel yang muncul di tengah setiap loop for namanya adalah `loop`. Contoh:

```
    {% for p in penggunas %}
        {{ dump(loop) }}
    {% endfor %}
```

Sebagai contoh penggunaan di antaranya:

1.	Menambahkan kolom nomor secara otomatis berdasarkan index dengan start 1 (tanpa harus menambahkannya pada array secara manual).

	```
	... 
	<td>{{ loop.index }}</td>
	...
	```

2.	Perlakuan khusus untuk setiap row. Misal jika barisnya genap, maka warnanya digelapkan. Contoh:	
	```
	<tr {% if loop.index0 is even %}class='even'{% endif %}
	```

3.	Perlakuan khusus pada baris pertama, atau baris terakhir. Contoh:
	```
	{% if loop.last %}
	<tr><th colspan="3">Total</th><th>{{ jumlah }} orang</th></tr>
	{% endif %}
	```


