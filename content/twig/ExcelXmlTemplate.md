/*
Title: Membuat Excel berbasis XML
Sort: 7
*/


### Excel Template berbasis XML

Sudah umum bahwa di setiap proyek aplikasi selalu diminta fitur unduh XLS. 
Nah dengan twig, kita bisa juga membuat file excel, namun dalam hal ini excel yang dimaksud berbasis teks
dengan format XML. 

Keuntungan format ini adalah dia mampu menghasilkan excel dengan data cukup besar sampai ribuan baris
tanpa batasan performa server yang terlalu ketat. Lain halnya dengan PHPExcel di mana 1 cell membutuhkan RAM
1 KB, yang membuatnya kadang tidak efisien sebagai library export Excel.


### Tahapan

#### Membuat Template XML Spreadsheet

Pertama-tama Buat dulu templatenya. Bikin title, subtitle, header tabel, dan ​*satu*​ baris contoh datanya.
Sesudah itu ​*Save As XML 2003 Spreadsheet*​ ke direktori src/MyApp/templates, kemudian close Excelnya. (edited)


#### Menambahkan Code Twig

Edit file tadi dengan Sublime / Jetbrains PHPStorm. Tambahkan kode-kode twig.

Sebagai contoh:
```
{% for p in penggunas %}
   <Row>
    <Cell><Data ss:Type="Number">{{ loop.index }}</Data></Cell>
    <Cell><Data ss:Type="String">{{ p.pengguna_id }}</Data></Cell>
    <Cell><Data ss:Type="String">{{ p.nama }}</Data></Cell>
    <Cell><Data ss:Type="String">{{ p.username }}</Data></Cell>
   </Row>
{% endfor %}   
```

#### Menambahkan Code Controller 

Untuk backend controllernya untuk mensuply data dan mekanisme header, dilakukan seperti ini:

```
    public function propelTwigXmlXls(Request $request, Application $app) {

        // Load penggunas
        $c = new \Criteria();
        $penggunas = PenggunaPeer::doSelect($c);

        $arrayPenggunas = array();

        foreach ($penggunas as $p) {
            $arrayPenggunas[] = $p->toArray();
        }

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__).D."templates";
        $fileName = "penggunaspropelxml.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader, array(
            "debug" => true
        ));
        $twig->addExtension(new \Twig_Extension_Debug());

        // Attach data and render
        $data = array(
            "title" => "Daftar Pengguna",
            "subtitle" => "Aplikasi MyApp - PT. Nufaza",
            "penggunas" => $arrayPenggunas,
            "jumlah" => sizeof($penggunas)
        );

        // Render stringnya
        $outStr = $twig->render($fileName, $data);

        // Buat response
        $response = new Response();
        $response->setContent($outStr);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment; filename="pengguna.xls"');

        return $response;
    }
```

Buat sendiri route URL nya, kemudian buka di browser. Cek hasilnya sudah oke atau belum.