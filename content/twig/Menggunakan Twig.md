/*
Title: Menggunakan Twig
Sort: 2
*/


## Menggunakan Twig Built In ServiceProvider by Xond3

Tambahkan di app.php

```
// Mencoba twig
$app->get('/cobatwig', 'MyApp\Coba::twig');
```

Tambahkan di Coba.php
```
public function twig (Request $request, Application $app) {

    $nama = $request->get("nama");

    $pengguna = array ("username" => "abah", "password" => "sukasepjay");

    $data = array(
        "nama" => $nama,
        "alamat" => "Venus barat",
        "pengguna" => $pengguna
    );

    return $app['twig']->render('nama.html', $data);

}
```

Buat template twignya di `C:\Projects\localhost\my_app\web\nama.html'`, sejajar dengan index.html:

```
Hallo {{ nama }}. Apa kabar? Kantornya di {{ alamat }} ya?

User kamu {{ pengguna.username }} dengan password {{ pengguna.password }} ya ?
```

Catat: $app['twig'] templatenya hanya bisa disimpan di `web\`



## Twig Template dengan Custom Directory

Jika kita ingin membuat template2 khusus yang tidak terkait template web page, misal utk printing/reporting dsb, buatlah direktori template tersendiri kemudian simpan template2nya di situ.

Contoh:

1.  Buat `C:\Projects\localhost\my_app\src\MyApp\templates\`

2.  Salin name.html di atas, simpan ke directory templates tadi dengan nama `nama.twig`

3.  Buat code di app.php:
	```
	// Mencoba twig with custom dir
	$app->get('/cobatwigcustomdir', 'MyApp\Coba::twigCustomDir');
    ```
4.  Buat code di Coba.php:

    ```    
    public function twigCustomDir (Request $request, Application $app)
    {
        // Get nama from querystring
        $nama = $request->get("nama");

        // Data i.e from database
        $pengguna = array ("username" => "abah", "password" => "sukasepjay");

        // Apply custom placed template
        $sourceTplDir = dirname(__FILE__).D."templates";
        $fileName = "nama.twig";

        $loader = new \Twig_Loader_Filesystem($sourceTplDir);
        $twig = new \Twig_Environment($loader);

        // Data yang dikirimkan untuk digabungkan ke template
        $data = array(
            "nama" => $nama,
            "alamat" => "Venus barat",
            "pengguna" => $pengguna
        );

        $outStr = $twig->render($fileName, $data);

        return $outStr;
    }
    ```
    
## Commenting

Memberi komentar pada code dilakukan dengan sintak seperti berikut:
`{#  Komentar anda #}`

