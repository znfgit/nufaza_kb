/*
Title: Debugging
Sort: 5
*/

Ada kalanya kita kebingungan, kenapa variabel kita tidak tampak setelah template dirender. 
Untuk mengatasinya, kita bisa menggunakan debugging dengan melakukan dump variabel yang dimaksud.

Caranya, ubah code di backend / controller yang memanggil twig sbb:

```
 	$loader = new \Twig_Loader_Filesystem($sourceTplDir);
	$twig = new \Twig_Environment($loader,array(
        'debug'=> true
    ));
    $twig->addExtension(new \Twig_Extension_Debug());    
```

Kalau sudah, gunakan 
`<pre>{{ dump(varname) }}</pre>` untuk menampilkan keterangan lengkap mengenai variable yang dimaksud.

Contoh:
```
    {% for p in penggunas %}
    <pre>{{ dump(loop) }}</pre>
    ...
    {% endfor %}
```

Referensi: 
http://twig.sensiolabs.org/doc/functions/dump.html