/*
Title: Intro
Sort: 1
*/


Tutorial ini diextract dari turorial Angular 6 yang lumayan bagus di:
https://www.youtube.com/watch?v=0eWrpsCLMJQ&list=PLC3y8-rFHvwhBRAgFinJR8KHIrCdTkZcZ

Source selesainya bisa diambil di sini:
https://github.com/gopinav

Bab-babnya:
1   Introduction
2   Getting Started
3   Hello World
4   Components
5   Interpolation
6   Property Binding
7   Class Binding
8   Style Binding
9   Event Binding
10  Template Reference Variables
11  Two Way Binding
12  ngIf Directive
13  ngSwitch Directive
14  ngFor Directive
15  Component Interaction
16  Pipes
17  Services
18  Dependency Injection
19  Using a Service
20  HTTP and Observables
21  Fetch Data Using HTTP
22  HTTP Error Handling
23  Routing and Navigation
24  Wildcard Route and Redirecting Route
25  Routing Parameters
26  ParamMap Observable
27  Optional Route Parameters
28  Relative Navigation
29  Child Routes
30  Angular 6 New Features


### 1 - Intro

Intinya, angular itu all-in framework full UI scaffolding menggunakan bahasa typescript dan jurus2 luarbiasa. Learning curvenya lumayan, jadi beware. Angular dicoding dgn NPM, karena harus live build menjadi js setiap kali save code. Deploynya nanti bisa di server apa saja karena sudah jadi js.

### 2 - Getting Started

Instal Nodejs.
Instal Visual Studio Code. Pencet Ctrl + BackTick untuk buka shell. Pilih bash aja.

Jalanin di shell
```
npm install -g @angular/cli 
ng new namaproject
cd namaproject
ng serve
```

Buka di http://localhost:4200

### 3 - Hello World App

