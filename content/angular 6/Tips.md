/*
Title: Intro
Sort: 10
*/

#### Monorepo Style Folder

Makin besar aplikasi angular kita, makin sering terlihat begini:
```
// import from component.ts
import { MyService } from '../../../../my.service';

// import from service.ts
import { HelloWorldComponent } from '../shared/deeply/nested/hello-world/hello-world.component';
```

Ugly bukan? Solusinya bisa kita bikin rapi:
```
import { MyService } from '@services/my.service';
import { HelloWorldComponent } from '@components/hello-world.component';
```

Agar bisa seperti itu kita harus definisikan.
```
// tsconfig.json in the root dir

{
  "compileOnSave": false,
  "compilerOptions": {

    // omitted...

    "baseUrl": "src",
    "paths": {
      "@services/*": ["app/path/to/services/*"],
      "@components/*": ["app/somewhere/deeply/nested/*"],
      "@environments/*": ["environments/*"]
    }
  }
}
```
