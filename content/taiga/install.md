/*
Title: Instalasi
Sort: 1
*/


## Instalasi on CentOS 7

Di dokumentasi resminya, instalasi taiga baru tersedia untuk Ubuntu. 
Untuk Centos hanya ada dokumentasi partikelir itu pun khusus Centos 6 dan masih kurang akurat. 
Dengan demikian dibuatlah dokumentasi instalasi hasil riset trial dan error untuk Centos 7.

Taiga terdiri dari dua tier, front end dan backend. Untuk frontend, taiga menggunakan static files
dilayani oleh nginx. Sedangkan untuk backend digunakan python django dan kawan2nya, dilayani oleh circus.

### Instalasi Back End

#### Prepare Server for Development Tools 

```
yum update -y
yum groupinstall "Development Tools" -y
yum install libxslt-devel libxml2-devel libXt-devel curl git tmux -y
yum install wget -y
```

#### Prepare Server for Postgres 9.4

```
TAIGA_POSTGRES_BD=taiga
TAIGA_POSTGRES_USER=taiga
TAIGA_POSTGRES_PASSWORD=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-20};echo;`

rpm -ivh http://yum.postgresql.org/9.4/redhat/rhel-6-x86_64/pgdg-centos94-9.4-1.noarch.rpm
sed -i 's/^gpgkey.*/&\nexclude=postgresql*/' /etc/yum.repos.d/CentOS-Base.repo

yum -y install postgresql94 postgresql94-contrib postgresql94-server postgresql94-devel postgresql94-libs
/usr/pgsql-9.4/bin/postgresql94-setup initdb

sed -i "/^host/s/ident/md5/g" /var/lib/pgsql/9.4/data/pg_hba.conf

/sbin/chkconfig --level 35 postgresql-9.4 on
service postgresql-9.4 start

echo -e "$TAIGA_POSTGRES_PASSWORD\n$TAIGA_POSTGRES_PASSWORD\n" | su - postgres -c "createuser --pwprompt $TAIGA_POSTGRES_USER"
su - postgres -c "createdb $TAIGA_POSTGRES_BD -O $TAIGA_POSTGRES_USER"

```

#### Prepare Python

```
wget --directory-prefix=/tmp http://repo.continuum.io/miniconda/Miniconda3-3.7.0-Linux-x86_64.sh
bash /tmp/Miniconda3-3.7.0-Linux-x86_64.sh -b -p /usr/local/miniconda3

cat > /etc/profile.d/miniconda.sh << EOF
if [[ \$PATH != */usr/local/miniconda3/bin* ]]
then
    export PATH=/usr/local/miniconda3/bin:\$PATH
fi
EOF

source /etc/profile.d/miniconda.sh

```

#### Installing Taiga Backend

TAIGA_POSTGRES_PASSWORD = 6AeomaWfuA1IA0CXeIUP
TAIGA_SECRET_KEY = SGHTRThWHOZ93QqovHdn

Buat user Taiga:
```
TAIGA_SECRET_KEY=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-20};echo;`

adduser taiga

DIR="/var/log/taiga /opt/taiga-back /opt/taiga-events"
for NAME in $DIR
do
if [ ! -d $NAME ]; then
   mkdir $NAME
   chown taiga.taiga $NAME
fi
done
```

Unduh source taiga backend:
```
su - taiga
git clone https://github.com/taigaio/taiga-back.git /opt/taiga-back
cd /opt/taiga-back
git checkout stable
```

Buat environment...
```
conda create --yes -n taiga python
source activate taiga
conda install --yes pip
```


Install taiga.io requirements...
```
export PATH=$PATH:/usr/pgsql-9.4/bin/
pip install -r requirements.txt
exit
```

Kemungkinan server belum menyediakan dukungan jpeg, akan jadi error saat upload foto profile. Untuk mencegahnya:
```
yum install libjpeg-devel
pip uninstall pillow
pip install pillow
```


Konfigurasi backend...
```
cat > /opt/taiga-back/settings/local.py << EOF
from .development import *

DATABASES = {
    'default': {
        'ENGINE': 'transaction_hooks.backends.postgresql_psycopg2',
        'NAME': '$TAIGA_POSTGRES_BD',
        'USER': '$TAIGA_POSTGRES_USER',
        'PASSWORD': '$TAIGA_POSTGRES_PASSWORD'
    }
}

MEDIA_URL = "http://taiga.nufaza.com:8008/media/"
STATIC_URL = "http://taiga.nufaza.com:8008/static/"
ADMIN_MEDIA_PREFIX = "http://taiga.nufaza.com:8008/static/admin/"
SITES["front"]["scheme"] = "http"
SITES["front"]["domain"] = "taiga.nufaza.com"

SECRET_KEY = "$TAIGA_SECRET_KEY"

DEBUG = False
TEMPLATE_DEBUG = False

PUBLIC_REGISTER_ENABLED = True

DEFAULT_FROM_EMAIL = "no-reply@nufaza.com"
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EOF
```

Ubah ownership..
```
chown taiga.taiga /opt/taiga-back/settings/local.py
```


Isi database dengan data awal ...
```
su - taiga
source activate taiga
cd /opt/taiga-back
python manage.py migrate --noinput
python manage.py loaddata initial_user
python manage.py loaddata initial_project_templates
python manage.py loaddata initial_role
python manage.py collectstatic --noinput
exit
```

Di database awal tersedia user "admin" dengan password "123123".


#### Circus and gunicorn

Install pip & circus:
```
conda install --yes pip
pip install circus
```

Cek conda:
```
su - taiga -c "conda info -e"
```

Hasilnya harusnya:
```
# conda environments:
#
taiga                    /home/taiga/envs/taiga
root                  *  /usr/local/miniconda3
```

Jalankan lagi
```
cat > /etc/circus.ini << EOF
[circus]
check_delay = 5
endpoint = tcp://127.0.0.1:5555
pubsub_endpoint = tcp://127.0.0.1:5556
statsd = true

[watcher:taiga]
working_dir = /opt/taiga-back
cmd = gunicorn
args = -w 3 -t 60 --pythonpath=. -b 127.0.0.1:8001 taiga.wsgi
uid = taiga
numprocesses = 1
autostart = true
send_hup = true
stdout_stream.class = FileStream
stdout_stream.filename = /var/log/taiga/gunicorn.stdout.log
stdout_stream.max_bytes = 10485760
stdout_stream.backup_count = 4
stderr_stream.class = FileStream
stderr_stream.filename = /var/log/taiga/gunicorn.stderr.log
stderr_stream.max_bytes = 10485760
stderr_stream.backup_count = 4

[env:taiga]
PATH = \$PATH:/home/taiga/envs/taiga/bin
TERM=rxvt-256color
SHELL=/bin/bash
USER=taiga
LANG=en_US.UTF-8
HOME=/home/taiga
PYTHONPATH=/home/taiga/envs/taiga/lib/python3.4/site-packages
EOF
```


Autostart service:

Edit  /etc/systemd/system/circus.service 

```
[Unit]
Description=Circus process manager
After=syslog.target network.target nss-lookup.target

[Service]
Type=simple
ExecReload=/usr/bin/circusctl reload
ExecStart=/usr/bin/circusd /etc/circus/circus.ini
Restart=always
RestartSec=5

[Install]
WantedBy=default.target
```

Load the service, enable, and start
```
systemctl --system daemon-reload
systemctl enable circus
systemctl start circus
```

Check it with:
```
systemctl status circus
```

Add in firewall
```
firewall-cmd --permanent --zone=public --add-port=8001/tcp
firewall-cmd --reload
```


### Frontend Installation

#### Get the source

```
su - taiga
cd ~
git clone https://github.com/taigaio/taiga-front-dist.git taiga-front-dist
cd taiga-front-dist
git checkout stable
```

#### Configure

Salin `taiga-front-dist/dist/js/conf.example.json` ke `taiga-front-dist/dist/js/conf.json`
```
cp dist/js/conf.example.json dist/js/conf.json
```

Edit konfigurasi  ~/taiga-front-dist/dist/js/conf.json
```
{
    "api": "http://taiga.nufaza.com:8008/api/v1/",
    "eventsUrl": "ws://taiga.nufaza.com:8008/events",
    "debug": "true",
    "publicRegisterEnabled": true,
    "feedbackEnabled": true,
    "privacyPolicyUrl": null,
    "termsOfServiceUrl": null,
    "maxUploadFileSize": null,
    "contribPlugins": []
}
```

Buat direktori log
```
mkdir logs
chown nginx:nginx /home/taiga/logs
```


#### Install nginx

Nginx melayani static file untuk taiga-front. Di sini kita gunakan port 8008. 
Nginx di sini juga berfungsi sebagai proxy untuk mengakses port-port yg dilayani taiga-back (8001, 5555, 5556, 5557) sehingga tidak usah terekspos dari luar.

```
yum install epel-release
yum install -y nginx
sudo systemctl start nginx
```

Edit /etc/nginx/conf.d/taiga.conf

```
server {
    listen 8000 default_server;
    server_name _;

    large_client_header_buffers 4 32k;
    client_max_body_size 50M;
    charset utf-8;

    access_log  /var/log/nginx/nginx.access.log;
    error_log  /var/log/nginx/nginx.error.log;

    # Frontend
    location / {
        root /home/taiga/taiga-front-dist/dist/;
        try_files $uri $uri/ /index.html;
    }

    # Backend
    location /api {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8001/api;
        proxy_redirect off;
    }

    # Django admin access (/admin/)
    location /admin {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8001$request_uri;
        proxy_redirect off;
    }

    # Static files
    location /static {
        alias /opt/taiga-back/static;
    }

    # Media files
    location /media {
        alias /opt/taiga-back/media;
    }
}
```

Tambahkan rule di firewall dan reload.
```
firewall-cmd --permanent --zone=public --add-port=8000/tcp
firewall-cmd --reload
```

Hapus default website nginx di /etc/nginx/nginx.conf
```
...
    #server {
    #    listen       8000 default_server;
    #    listen       [::]:8000 default_server;
    #    server_name  _;
    #    root         /usr/share/nginx/html;
    #
        # Load configuration files for the default server block.
    #    include /etc/nginx/default.d/*.conf;
    #
    #    location / {
    #    }
    #
    #    error_page 404 /404.html;
    #        location = /40x.html {
    #    }
    #
    #    error_page 500 502 503 504 /50x.html;
    #        location = /50x.html {
    #    }
    #}
...

```

Cek configuration udah ok belum
```
sudo nginx -t
```

Restart nginx
``` 
sudo systemctl restart nginx
```

Cek via browser apakah taiga sudah jalan. Jika internal error, berarti permisi direktori belum benar.
Perbaiki permission home direktori taiga:
```
chmod a+rx /home/taiga
```

Jika masih ada  forbidden, cek /var/log/audit/audit.log. Jika muncul error semacam ini:
```
type=AVC msg=audit(1451377170.318:5072): avc:  denied  { open } for  pid=25482 comm="nginx" path="/home/taiga/taiga-front-dist/dist/index.html" dev="dm-2" ino=134424475 scontext=system_u:system_r:httpd_t:s0 tcontext=unconfined_u:object_r:user_home_t:s0 tclass=file
type=SYSCALL msg=audit(1451377170.318:5072): arch=c000003e syscall=2 success=no exit=-13 a0=7ff24e6892ed a1=800 a2=0 a3=0 items=0 ppid=25481 pid=25482 auid=4294967295 uid=992 gid=989 euid=992 suid=992 fsuid=992 egid=989 sgid=989 fsgid=989 tty=(none) ses=4294967295 comm="nginx" exe="/usr/sbin/nginx" subj=system_u:system_r:httpd_t:s0 key=(null)
```
berarti permission masih menghadang dari SELinux:

Tambahkan nginx ke selinux:
```
grep nginx /var/log/audit/audit.log | audit2allow -M nginx
semodule -i nginx.pp
```

Kalau command tadi error, berarti audit2allow belum terinstall. Instal dengan cara:
```
yum install -y policycoreutils-{python,devel}
```


#### Websocket / Events installation

This step is completelly optional and can be skipped

Taiga-events is the Taiga websocket server, it allows taiga-front showing realtime changes in backlog, taskboard, kanban and issues listing.

Taiga events needs rabbitmq (the message broker) to be installed

Installing rabbitmq
```
sudo  apt-get install rabbitmq-server
```

Creating a taiga user and virtualhost for rabbitmq
```
sudo rabbitmqctl add_user taiga PASSWORD
sudo rabbitmqctl add_vhost taiga
sudo rabbitmqctl set_permissions -p taiga taiga ".*" ".*" ".*"
```

Update your taiga-back settings to include in your local.py the lines:
```
EVENTS_PUSH_BACKEND = "taiga.events.backends.rabbitmq.EventsPushBackend"
EVENTS_PUSH_BACKEND_OPTIONS = {"url": "amqp://taiga:PASSWORD@localhost:5672/taiga"}
```

The next step is downloading the code from github and installing their dependencies:

Download the code
```
cd ~
git clone https://github.com/taigaio/taiga-events.git taiga-events
cd taiga-events
```

Install all the javascript dependencies needed
```
sudo apt-get install -y nodejs nodejs-legacy npm
npm install
sudo npm install -g coffee-script
```

Copy and edit the config.json file you should update your rabbitmq uri and the secret key.
```
cp config.example.json config.json
```

Your config.json should be like:
```
{
    "url": "amqp://taiga:PASSWORD@localhost:5672/taiga",
    "secret": "mysecret",
    "webSocketServer": {
        "port": 8888
    }
}
```

Now you have to add taiga-events to circus configuration. See Circus and gunicorn section.

Taiga taiga-events configuration block for circus on ~/circus.ini
```
[watcher:taiga-events]
working_dir = /home/taiga/taiga-events
cmd = /usr/local/bin/coffee
args = index.coffee
uid = taiga
numprocesses = 1
autostart = true
send_hup = true
stdout_stream.class = FileStream
stdout_stream.filename = /home/taiga/logs/taigaevents.stdout.log
stdout_stream.max_bytes = 10485760
stdout_stream.backup_count = 12
stderr_stream.class = FileStream
stderr_stream.filename = /home/taiga/logs/taigaevents.stderr.log
stderr_stream.max_bytes = 10485760
stderr_stream.backup_count = 12
```

Then you have to reload your circus configuration restart the other services and start taiga-events:
```
circusctl reloadconfig
circusctl restart taiga
circusctl restart taiga-celery
circusctl start taiga-events
```

Nginx need extra configuration too for taiga-events

Add specific configuration for taiga-events on /etc/nginx/sites-available/taiga.
```
server {
    ...
    location /events {
       proxy_pass http://127.0.0.1:8888/events;
       proxy_http_version 1.1;
       proxy_set_header Upgrade $http_upgrade;
       proxy_set_header Connection "upgrade";
       proxy_connect_timeout 7d;
       proxy_send_timeout 7d;
       proxy_read_timeout 7d;
    }
    ...
}
```

And finally, reload nginx with 
```
sudo service nginx reload
```



#### References

https://gist.github.com/ss-abramchuk/fd77fe4aa823d92c15e9   -- Taiga on Centos 6 
http://taigaio.github.io/taiga-doc/dist/setup-production.html -- Taiga on Ubuntu 14
http://circus.readthedocs.org/en/latest/for-ops/deployment/ -- Circus script
https://gist.github.com/jasonthomas/5785710
https://gist.github.com/ssplatt/42393b7d9c876a027aaa