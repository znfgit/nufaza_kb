/*
Title: Daftar Istilah
Sort: 3
*/

Berikut adalah daftar istilah yang harus dipahami anggota technical staff Nufaza,Inc.

### Atomic

Biasanya dipakai dalam urusan storage. Suatu sistem data storage dikatakan atomic jika dia dapat menjamin bahwa hanya satu operasi (update dsb) pada suatu variabel/data yang dapat dilakukan pada suatu saat.

Misal ada suatu variabel count dengan nilai 10. Di suatu saat ada dua client yg ingin menambahkan satu/increment nilai tersebut. Maka proses berikut TIDAK atomic:

```
x = GET count
x = x + 1
SET count = x
```

Bisa terjadi bahwa karena hampir bersamaan, Client A dan Client B GET count pada saat nilainya sama-sama 10. Client A menambahkan 1 pada 10, begitu juga Client B. Hasil akhirnya menjadi 11. Padahal seharusnya nilai count = 12.

