/*
Title: Marketing Circles Roles
Sort: 4
*/

- [Lead Link](#lead-link)
- [Rep Link](#rep-link)
- [Sekretaris GCC](#sekretaris-gcc)
- [Fasilitator GCC](#fasilitator-gcc)
- [Marketing](#marketing)
- [Client Engagement](#client-engagement)
- [Asisten Pelatih](#asisten-pelatih)
- [Konsultan](#konsultan)
- [Master Trainer](#master-trainer)
- [Asisten Pelatih](#asisten-pelatih)
- [Onsite Operator](#onsite-operator)
- [Helpdesk](#helpdesk)


### Lead Link

#### Tujuan
Nufaza sebagai organisasi yang berhasil

#### Domain
Penugasan di MCC

#### Akuntabilitas

1. Menetapkan struktur pemerintahan *(governance)* dalam GCC untuk mengekspresikan tujuan dan memberlakukan akuntabilitasnya.
2. Menugaskan partner ke dalam peran, monitoring kecocokan partner dalam perannya, menawarkan umpan balik untuk meningkatkan kecocokan, dan menugaskan ulang peran bagi partner sesuai kecocokannya.
3. Mengalokasikan SDM di dalam lingkaran untuk proyek dan/atau peran.
4. Menetapkan prioritas dan strategi untuk lingkaran.
5. Mendefinisikan metrik untuk lingkaran


### Rep Link

#### Tujuan
Menyalurkan dan menyelesaikan *Tension* yang relevan untuk diproses dalam MCC.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Menghilangkan kendala dalam organisasi yang lebih luas yang dapat membatasi (*sub-circle*)
2. Berusaha memahami ketegangan (tension) yang disampaikan oleh anggota *sub-circle*, dan menentukan mana yang harus diproses di lingkaran yang lebih tinggi.
3. Menyediakan visibilitas pada *super-circle* mengenai kesehatan dari *sub-circle*, termasuk pelaporan metrik atau *checklist item* yang ditugaskan pada *sub-circle*. 


### Sekretaris MCC

#### Tujuan
Mengelola pencatatan dokumen resmi dalam MCC dan proses pengarsipannya.

#### Domain
Semua dokumen resmi (konstitusional) di MCC

#### Akuntabilitas
1. Menjadwalkan pertemuan yang harus dilakukan dalam lingkaran dan memberitahukan (mengundang) semua anggota lingkaran mengenai jadwal dan lokasi pertemuan yang telah ditetapkan.
2. Menangkap output dari pertemuan yang harus dilakukan dalam pertemuan yang diwajibkan oleh lingkaran dan memelihara kompilasi dari pandangan dari pemerintahan dalam lingkaran, checklist items, dan metrik.
3. Menafsirkan Pemerintahan dan Konstitusi atas permintaan


### Fasilitator MCC

#### Tujuan
Berjalannya pemerintahan dalam GCC sesuai Konstitusi Nufaza-cracy.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Memfasilitasi pertemuan-pertemuan dalam lingkaran yang wajib dilaksanakan.
2. Melakukan audit terhadap pertemuan dan catatan dari sub lingkaran sesuai kebutuhan dan melakukan inisiasi proses restorasi yang didefinisikan dalam konstitusi saat menemukan *breakdown* dalam proses.


### Marketing

#### Tujuan
Perusahaan mendapatkan klien dan proyek

#### Domain
- Network di luar perusahaan (potential client)
- Existing client

#### Akuntabilitas
1. Melakukan networking dengan tujuan untuk menjual jasa dan produk yang dihasilkan oleh Nufaza
2. Membuat draft proposal proyek
3. Past client engagement
4. Membuat penawaran harga untuk pelanggan
5. Menyusun standar dan kebijakan untuk proses marketing


### Konsultan

#### Tujuan
Pelayanan konsultasi prima untuk klien

#### Domain
Sesuai kontrak

#### Akuntabilitas
1. Memberi layanan konsultasi pada klien yang membutuhkan


### Master Trainer

#### Tujuan
Permintaan Training aplikasi nufaza terlaksana dengan baik

#### Domain
Sesuai dengan kemampuan melatih aplikasi tertentu

#### Akuntabilitas
1. Menyampaikan materi training pada peserta kegiatan pelatihan sesuai standar training yang telah ditetapkan.
Membuat materi slide presentasi yang dapat mengoptimalkan setiap pelatihan dan mengintegrasikan proses pembelajaran dengan penyampaian pelatihan.
2. Mengarahkan asisten pelatih dalam pelatihan.
3. Memberikan saran kepada stake holder lain yang berkaitan dengan pelatihan.


### Asisten Pelatih

#### Tujuan
Membantu Pelatih dalam mewujudkan pelatihan aplikasi-aplikasi Nufaza yang transformatif sebagai bagian dari layanan untuk klien.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Menghilangkan gangguan pada saat training yang dapat membatasi kehadiran dan fokus dari peserta training.
2. Membantu pelatih dalam menyelesaikan masalah yang berkaitan dengan venue dan logistik selama pelatihan.
3. Mengorientasikan dan memandu peserta training sesuai arahan pelatih utama.
4. Setting up and tearing down training venue at the direction of Training Operations and Trainer
5. Monitoring training delivery for useful updates to training setup and design standards, and creating or updating relevant materials or guidelines to capture learning


### Onsite Operator

#### Tujuan
Menyediakan layanan operator onsite bagi pengguna aplikasi Nufaza

#### Domain
Sesuai dengan aplikasi yang ditugaskan

#### Akuntabilitas
1. Menjadi operator onsite bagi pengguna aplikasi nufaza yaitu mengoperasikan aplikasi dan input data
2. Melakukan troubleshooting dari masalah-masalah yang dihadapi oleh pengguna aplikasi dan membuat pelaporan bug yang ditemukan.
3. Perbaikan bug
4. Mengintegrasikan laporan umpan balik dari pengguna aplikasi untuk meningkatkan kepuasan pengguna.


### Helpdesk

#### Tujuan
Menyediakan layanan helpdesk bagi pengguna aplikasi Nufaza

#### Domain
Sesuai dengan aplikasi yang ditugaskan

#### Akuntabilitas
1. Merespon permintaan bantuan (support) dari pengguna aplikasi.
2. Melakukan troubleshooting dari masalah-masalah yang dihadapi oleh pengguna aplikasi dan membuat pelaporan bug yang ditemukan.
3. Menambah dan memutakhirkan FAQ untuk setiap Aplikasi Nufaza
4. Mengintegrasikan laporan umpan balik dari pengguna aplikasi untuk meningkatkan kepuasan pengguna.
5. Dokumentasi setiap versi aplikasi (installer) dan change log.
6. Membuat screenshot untuk setiap versi aplikasi
7. Memutakhirkan manual aplikasi.
8. Membuat dan memutakhirkan video tutorial penggunaan aplikasi.