/*
Title: Development Circle Roles
Sort: 2
*/

- [Lead Link](#lead-link)
- [Rep Link](#rep-link)
- [Sekretaris DC](#sekretaris-dc)
- [Fasilitator DC](#fasilitator-dc)
- [Technical Mentor](#technical-mentor)
- [Framework Developer](#framework-developer)
- [Project Owner](#project-owner)
- [Scrum Master](#scrum-master)
- [Framework Developer](#framework-developer)
- [System Architect](#system-architect)
- [Feature Developer](#feature-developer)
- [UI Designer](#ui-designer)
- [Test Smith](#test-smith)
- [Manual Tester](#manual-tester)
- [Technical Writer](#technical-writer)
- [Network Administrator](#network-administrator)
- [Server Administrator](#server-administrator)
- [Source Control Administrator](#source-control-administrator)


### Lead Link

#### Tujuan
Nufaza sebagai organisasi yang berhasil

#### Domain
Penugasan di DC

#### Akuntabilitas

1. Menetapkan struktur pemerintahan *(governance)* dalam GCC untuk mengekspresikan tujuan dan memberlakukan akuntabilitasnya.
2. Menugaskan partner ke dalam peran, monitoring kecocokan partner dalam perannya, menawarkan umpan balik untuk meningkatkan kecocokan, dan menugaskan ulang peran bagi partner sesuai kecocokannya.
3. Mengalokasikan SDM di dalam lingkaran untuk proyek dan/atau peran.
4. Menetapkan prioritas dan strategi untuk lingkaran.
5. Mendefinisikan metrik untuk lingkaran


### Rep Link

#### Tujuan
Menyalurkan dan menyelesaikan *Tension* yang relevan untuk diproses dalam GCC.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Menghilangkan kendala dalam organisasi yang lebih luas yang dapat membatasi (*sub-circle*)
2. Berusaha memahami ketegangan (tension) yang disampaikan oleh anggota *sub-circle*, dan menentukan mana yang harus diproses di lingkaran yang lebih tinggi.
3. Menyediakan visibilitas pada *super-circle* mengenai kesehatan dari *sub-circle*, termasuk pelaporan metrik atau *checklist item* yang ditugaskan pada *sub-circle*. 


### Sekretaris DC

#### Tujuan
Mengelola pencatatan dokumen resmi dalam DC dan proses pengarsipannya.

#### Domain
Semua dokumen resmi (konstitusional) di DC

#### Akuntabilitas
1. Menjadwalkan pertemuan yang harus dilakukan dalam lingkaran dan memberitahukan (mengundang) semua anggota lingkaran mengenai jadwal dan lokasi pertemuan yang telah ditetapkan.
2. Menangkap output dari pertemuan yang harus dilakukan dalam pertemuan yang diwajibkan oleh lingkaran dan memelihara kompilasi dari pandangan dari pemerintahan dalam lingkaran, checklist items, dan metrik.
3. Menafsirkan Pemerintahan dan Konstitusi atas permintaan


### Fasilitator DC

#### Tujuan
Berjalannya pemerintahan dalam DC sesuai Konstitusi Nufaza-cracy.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Memfasilitasi pertemuan-pertemuan dalam lingkaran yang wajib dilaksanakan.
2. Melakukan audit terhadap pertemuan dan catatan dari sub lingkaran sesuai kebutuhan dan melakukan inisiasi proses restorasi yang didefinisikan dalam konstitusi saat menemukan *breakdown* dalam proses.


### Technical Mentor

#### Tujuan
Meningkatkan kualitas aplikasi Nufaza melalui developer-developer yang handal.

#### Domain

#### Akuntabilitas
1. Mengawasi dan meningkatkan proses peningkatan produktifitas developer
2. Melakukan mentoring pada developer tentang masalah teknis dan pengembangan software


### Framework Developer

#### Tujuan
Mengembangkan arsitektur *Xond Framework* yang memungkinkan pengembangan aplikasi cepat (RAD) dan *integrity of the codebase*

#### Domain
Xond Framework

#### Akuntabilitas
1. Mendefinisikan dan mengevolusikan teknologi Xond 3, termasuk bahasa (languages), pustaka (libraries), dan kerangka kerja (frameworks), termasuk standar cara menggunakannya.
2. Melakukan monitoring terhadap keuntungan versus kerugian penggunaan teknologi terpilih sesuai dengan strategi dan prioritas lingkaran
3. Menyusun prioritas task yang ada dalam backlog


### Project Owner

#### Tujuan
Menjadi *Customer Proxy* di Nufaza

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas

1.  Menyusun *Project Charter*
2.  Diskusi dengan klien dan *stake holder* dan menyusun *user story* dari hasil diskusi tersebut
2.  *Product Backlog Grooming and Refinement* yaitu menyusun dan memelihara team backlog (*user stories, defects, refactors, infrastructure*).
3.  Bekerjasama dengan Scrum Master dan Anggota Tim dalam *Iteration Planning*  mendefinisikan dan menyusun prioritas *Product Backlog* untuk setiap Iterasi.
4.  Berpartisipasi dalam *Scrum Review*/Demo Aplikasi dan *Scrum Retrospective*)
5.  Diskusi dengan klien untuk mendapatkan *feedback* aplikasi yang akan  dijadikan bahan untuk menyusun *sprint backlog* pada iterasi berikutnya.


### Scrum Master

#### Tujuan
Mengimplementasikan **Scrum** di Nufaza

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas

1.  Melakukan *coaching* pada anggota tim mengenai prinsip-prinsip **Scrum**.
2.  Mengidentifikasi dan menghilangkan *impediment*.
3.  Memastikan bahwa team bekerja dengan produktif.
4.  Melindungi team dari gangguan eksternal.
5.  Memfasilitasi pertemuan-pertemuan **Scrum** (*backlog grooming, planning poker, sprint planning, daily stand-up, sprint review, sprint retrospective*).
6.  Melakukan updating pada aplikasi Scrum.
7.  Bekerjasama dengan fasilitator dalam hal *Tactical Meeting*.


### System Architect

#### Tujuan
Aplikasi dihasilkan sesuai dengan bisnis proses dan kebutuhan klien dan mudah di-develop oleh developer.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Riset dan analisis bisnis proses kegiatan yang akan diaplikasikan
2. Bekerja sama dengan **Project Owner** dalam membuat *User Story*
3. Membuat desain database aplikasi menggunakan Power Designer
4. Membuat penyempurnaan desain database aplikasi menggunakan Power Designer dan mendokumentasikan *versioning*-nya
5. Membuat diagram-diagram lain yang dibutuhkan


### Feature Developer

#### Tujuan
Mewujudkan *user story* menjadi aplikasi yang berfungsi dan bernilai bagi klien.

#### Domain
Sesuai penugasan yang diberikan oleh *Development Circle Lead Link*

#### Akuntabilitas
1. Melakukan *coding* aplikasi dengan mengimplementasikan user stories, bug fixes, dan tugas-tugas pengembangan aplikasi sejalan dengan prioritas yang ditetapkan oleh **Project Owner**, standar teknis yang ditetapkan oleh **Framework Developer** dan arahan **Scrum Master**.
2. Melakukan pengetesan fungsi-fungsi baru sebelum aplikasi di-deploy.
3. Mengikuti **Planning Poker** untuk mengestimasi tingkat kesulitan setiap *user story*
4. Mengikuti pertemuan-pertemuan scrum (*backlog grooming, planning poker, daily stand-up, scrum review, scrum retrospective*)
5. Melaporkan kendala-kendala dalam pengembangan aplikasi terhadap *Scrum Master* dan *Framework Developer*.


### UI Designer

#### Tujuan
Menyediakan desain antar muka aplikasi yang *user friendly*

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Membuat *wireframe* dari aplikasi
2. Membuat *prototype* antar muka (user interface) dari aplikasi
3. Mendesain icon dan grafis lain yang diperlukan oleh aplikasi


### Test Smith

#### Tujuan
Umpan balik yang cepat dan dapat diandalkan untuk developers

#### Domain
Semua aplikasi produk Nufaza

#### Akuntabilitas
1. Mengadiminstrasikan *CI Environment*.
2. Menyiapkan framework testing yang efektif.
3. Membantu **Feature Developer** dalam kegagalan kode/test yang sulit di-*debug*.


### Manual Tester

#### Tujuan
Memastikan semua fitur aplikasi sudah berjalan dengan baik sebelum rilis.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Melakukan pengetesan fitur-fitur dalam aplikasi dengan cara uji coba secara manual sebelum rilis atau peluncuran aplikasi.
2. Membuat pelaporan *bug* dan menyusun prioritas perbaikannya (diutamakan bug yang membuat aplikasi tidak bisa digunakan).


### Technical Writer

#### Tujuan
Memastikan setiap aplikasi terdokumentasikan dengan baik

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Menyusun dokumen laporan pelaksanaan pekerjaan
2. Menyusun dokumentasi aplikasi sesuai standar *agile software engineering*
3. Menyusun dokumentasi aplikasi pada *knowledge base* dengan *markdown* 


### Network Administrator

#### Tujuan
Jaringan cepat dan lancar

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Setting internet jaringan
2. Setting IP jaringan
3. Setting keamanan jaringan (VPN)


### Server Administrator

#### Tujuan
Server aman dan lancar digunakan

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Pengelolaan server (akun password)
2. Back-up server


### Source Control Administrator

#### Tujuan
Kontrol versi aplikasi yang *seamless* untuk semua aplikasi yang dikembangkan oleh Nufaza.

#### Domain
Akun admin di https://bitbucket.org/

#### Akuntabilitas
1. Mendukung **Feature Developer** dalam menggunakan sistem *source control* secara efektif dan efisien.
2. Mengelola *source control* sesuai dengan standar yang telah ditetapkan oleh **Framework Developer**.