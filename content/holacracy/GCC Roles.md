/*
Title: General Company Circles Roles
Sort: 1
*/

- [Lead Link](#lead-link)
- [Rep Link](#rep-link)
- [Sekretaris GCC](#sekretaris-gcc)
- [Fasilitator GCC](#fasilitator-gcc)
- [Keuangan](#keuangan)
- [Petugas Akuntansi](#petugas-akutansi)
- [Pembukuan](#pembukuan)
- [Admin Pajak](#admin pajak)
- [Administrasi Proyek](#administrasi proyek)
- [Administrasi Penagihan](#administrasi penagihan)
- [Visual dan Desain](#visual-dan-desain)
- [Office Manager](#office-manager)
- [Office Helper](#office-helper)
- [Keamanan Kantor](#keamanan-kantor)


### Lead Link

#### Tujuan
Nufaza sebagai organisasi yang berhasil

#### Domain
Penugasan di GCC

#### Akuntabilitas

1. Menetapkan struktur pemerintahan *(governance)* dalam GCC untuk mengekspresikan tujuan dan memberlakukan akuntabilitasnya.
2. Menugaskan partner ke dalam peran, monitoring kecocokan partner dalam perannya, menawarkan umpan balik untuk meningkatkan kecocokan, dan menugaskan ulang peran bagi partner sesuai kecocokannya.
3. Mengalokasikan SDM di dalam lingkaran untuk proyek dan/atau peran.
4. Menetapkan prioritas dan strategi untuk lingkaran.
5. Mendefinisikan metrik untuk lingkaran


### Rep Link

#### Tujuan
Menyalurkan dan menyelesaikan *Tension* yang relevan untuk diproses dalam GCC.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Menghilangkan kendala dalam organisasi yang lebih luas yang dapat membatasi (*sub-circle*)
2. Berusaha memahami ketegangan (tension) yang disampaikan oleh anggota *sub-circle*, dan menentukan mana yang harus diproses di lingkaran yang lebih tinggi.
3. Menyediakan visibilitas pada *super-circle* mengenai kesehatan dari *sub-circle*, termasuk pelaporan metrik atau *checklist item* yang ditugaskan pada *sub-circle*. 


### Sekretaris GCC

#### Tujuan
Mengelola pencatatan dokumen resmi dalam GCC dan proses pengarsipannya.

#### Domain
Semua dokumen resmi (konstitusional) di GCC

#### Akuntabilitas
1. Menjadwalkan pertemuan yang harus dilakukan dalam lingkaran dan memberitahukan (mengundang) semua anggota lingkaran mengenai jadwal dan lokasi pertemuan yang telah ditetapkan.
2. Menangkap output dari pertemuan yang harus dilakukan dalam pertemuan yang diwajibkan oleh lingkaran dan memelihara kompilasi dari pandangan dari pemerintahan dalam lingkaran, checklist items, dan metrik.
3. Menafsirkan Pemerintahan dan Konstitusi atas permintaan


### Fasilitator GCC

#### Tujuan
Berjalannya pemerintahan dalam GCC sesuai Konstitusi Nufaza-cracy.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Memfasilitasi pertemuan-pertemuan dalam lingkaran yang wajib dilaksanakan.
2. Melakukan audit terhadap pertemuan dan catatan dari sub lingkaran sesuai kebutuhan dan melakukan inisiasi proses restorasi yang didefinisikan dalam konstitusi saat menemukan *breakdown* dalam proses.


### Legal

#### Tujuan
Perusahaan bisa berjalan sesuai dengan aturan hukum yang berlaku

#### Domains
1. Peraturan yang berlaku di perusahaan dan hubungannya dengan sistem hukum yang berlaku
2. Hubungan/relasi dengan legal pihak ketiga
3. Dokumen kontrak perusahaan dengan pihak ketiga
4. Perpanjangan izin perusahaan
5. Keanggotaan perusahaan dalam asosiasi
6. Dokumen kontrak partner (kerjasama dengan leadlink people&partner)


### Keuangan

#### Tujuan
Keuangan Perusahaan Sehat, semua transaksi keuangan sesuai prosedur.

#### Domain
1. Rekening Giro Perusahaan (Penerimaan)
2. Rekening Pengeluaran Perusahaan

#### Akuntabilitas
1. Pembukaan dan penutupan rekening perusahaan
2. Mendukung kelancaran transaksi internal (pembayaran gaji, restitusi, reimburs kas kecil)
3. Mendukung pendanaan proyek
4. Bertanggungjawab atas keamanan keuangan perusahaan
5. Verifikasi dan validasi pencatatan transaksi keuangan (zahir)
6. Verifikasi dan validasi pelaporan keuangan (Neraca, Laba/Rugi, Proyeksi Arus Kas *(Cashflow Projection)* dan Realisasi Anggaran)


### Petugas Akuntansi

#### Tujuan
Memastikan bahwa pencatatan, pengklasifikasian dan penggolongan transaksi keuangan sudah sesuai dengan kebijakan keuangan perusahaan yang berlaku.

#### Domain
1. Zahir
2. Laporan keuangan

#### Akuntabilitas
1. Menginput transaksi keuangan ke dalam Sistem Keuangan Perusahaan (Zahir).
2. Membuat Laporan Keuangan (Neraca, Laba/Rugi, Proyeksi Arus Kas *(Cashflow Projection)* dan Realisasi Anggaran)
3. Rekonsiliasi saldo keuangan.


### Pembukuan

#### Tujuan
Tansaksi keuangan dikelola dengan baik

#### Domain
Petty Cash (Bandung atau Jakarta)

#### Akuntabilitas
1. Mencatat pemasukan uang kas dan menyetor ke bank
2. Mengelola Dana Kas Kecil *(Petty Cash)*
3. Menerima dan mengarsipkan bukti transaksi pemasukan dan pengeluaran
4. Memproses permintaan reimbursement dari partner sesuai dengan SOP yang telah ditetapkan.


### Admin Pajak

#### Tujuan
Kewajiban perpajakan perusahaan terlaksana dengan baik

#### Domain
- Laporan Pajak Badan Bulanan
- Laporan Pajak Badan Tahunan
- Laporan Pph Perorangan Tahunan
- Aplikasi e-Faktur

#### Akuntabilitas
1. Menghitung pajak terhutang sesuai dengan peraturan perundangan yang berlaku
2. Penyusunan faktur pelaporan pajak bulanan
2. Pembayaran pajak
3. Pelaporan pajak tahunan
4. Pelaporan ke kantor pajak
5. Pengarsipan dokumen perpajakan


### Administrasi Proyek

#### Tujuan
Setiap proyek terikat kontrak

#### Domain
Administrasi proyek sesuai penugasan

#### Akuntabilitas
1. Bekerja sama dengan klien dalam hal administrasi proyek (mis. membantu pembuatan KAK, HPS)
2. Bekerja sama dengan pihak ketiga yang diperlukan dalam hal administrasi proyek (mis. bendera)
3. Membuat dokumen tender (PQ, Dokumen Penawaran, CV dsb) atau proposal penawaran
4. Memastikan bahwa kontrak kerja untuk setiap proyek yang dikerjakan oleh perusahaan ada dan sah
5. Penyimpanan dan pengarsipan dokumen administrasi proyek
6. Mengolola akun LPSE perusahaan


### Administrasi Penagihan

#### Tujuan
Setiap proyek dapat ditagih dengan baik

#### Domain
Administrasi penagihan sesuai penugasan

#### Akuntabilitas
1. Membuat dokumen penagihan *(invoice)* sesuai dengan kebijakan sudah ditetapkan.
2. Monitoring *Account Receivable Aging Reports*
3. Mengingatkan PO, SM, dan Development Team mengenai tengat waktu penagihan dan syarat penagihan sesuai termin pekerjaan.
4. Memberikan informasi pada Keuangan mengenai jumlah yang harus ditagih.
5. Melaksanakan penagihan dengan memprioritaskan penagihan yang sudah jatuh tempo atau terlambat.
6. Mengecek kebenaran antara pembayaran yang diterima dan jumlah yang tertera pada dokumen penagihan *(invoice)* kemudian melapor pada keuangan


### Visual dan Desain

#### Tujuan
Komunikasi visual yang menarik dan jelas

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Mendesain tampilan visual dari material yang diterbitkan oleh perusahaan (misalnya cover laporan, sampul CD aplikasi dst)
2. Menyediakan ilustrasi untuk pelatihan dan marketing
3. Membantu dan melatih partner dalam hal visual dan desain

### Office Manager

#### Tujuan
Setiap proyek terikat kontrak

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Melakukan rekapitulasi absensi bulanan karyawan
2. Membeli peralatan kantor dan ATK
3. Mengawasi pelaksanaan K3 di kantor
4. Inventarisasi peralatan kantor
5. Arsip dokumen kantor


### Office Helper

#### Tujuan
Kebersihan, ketertiban, keindahan kantor

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Melakukan tugas rutin dan non rutin untuk kebersihan, ketertiban dan keindahan kantor (K3) sesuai dengan jadwal yang telah ditetapkan
2. Melakukan tugas belanja logistik dan konsumsi (makan karyawan)
3. Membantu staf kantor lainnya dalam tugas-tugas
4. Merawat inventaris kantor


### Keamanan Kantor

#### Tujuan
Kantor aman

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Melaksanakan tugas jaga malam di kantor Bandung sesuai jadwal (memastikan kantor terkunci)
2. Koordinasi antara petugas jaga malam agar tidak terjadi kekosongan pada jadwal jaga malam