/*
Title: People Partner Circles Roles
Sort: 3
*/

- [Lead Link](#lead-link)
- [Rep Link](#rep-link)
- [Sekretaris GCC](#sekretaris-gcc)
- [Fasilitator GCC](#fasilitator-gcc)
- [Badge Bootstrapper](#badge-bootstrapper)
- [Badge Librarian](#badge-librarian)
- [Compensation Architect](#compensation-architect)
- [Compensation Guardian](#compensation-guardian)
- [Compensation Administrator](#compensation-administrator)
- [Partner Partner](#partner-partner)
- [Partner Insurance](#partner-insurance)


### Lead Link

#### Tujuan
Nufaza sebagai organisasi yang berhasil

#### Domain
Penugasan di PPC

#### Akuntabilitas

1. Menetapkan struktur pemerintahan *(governance)* dalam GCC untuk mengekspresikan tujuan dan memberlakukan akuntabilitasnya.
2. Menugaskan partner ke dalam peran, monitoring kecocokan partner dalam perannya, menawarkan umpan balik untuk meningkatkan kecocokan, dan menugaskan ulang peran bagi partner sesuai kecocokannya.
3. Mengalokasikan SDM di dalam lingkaran untuk proyek dan/atau peran.
4. Menetapkan prioritas dan strategi untuk lingkaran.
5. Mendefinisikan metrik untuk lingkaran


### Rep Link

#### Tujuan
Menyalurkan dan menyelesaikan *Tension* yang relevan untuk diproses dalam GCC.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Menghilangkan kendala dalam organisasi yang lebih luas yang dapat membatasi (*sub-circle*)
2. Berusaha memahami ketegangan (tension) yang disampaikan oleh anggota *sub-circle*, dan menentukan mana yang harus diproses di lingkaran yang lebih tinggi.
3. Menyediakan visibilitas pada *super-circle* mengenai kesehatan dari *sub-circle*, termasuk pelaporan metrik atau *checklist item* yang ditugaskan pada *sub-circle*. 


### Sekretaris PPC

#### Tujuan
Mengelola pencatatan dokumen resmi dalam GCC dan proses pengarsipannya.

#### Domain
Semua dokumen resmi (konstitusional) di GCC

#### Akuntabilitas
1. Menjadwalkan pertemuan yang harus dilakukan dalam lingkaran dan memberitahukan (mengundang) semua anggota lingkaran mengenai jadwal dan lokasi pertemuan yang telah ditetapkan.
2. Menangkap output dari pertemuan yang harus dilakukan dalam pertemuan yang diwajibkan oleh lingkaran dan memelihara kompilasi dari pandangan dari pemerintahan dalam lingkaran, checklist items, dan metrik.
3. Menafsirkan Pemerintahan dan Konstitusi atas permintaan


### Fasilitator PPC

#### Tujuan
Berjalannya pemerintahan dalam GCC sesuai Konstitusi Nufaza-cracy.

#### Domain
Sesuai dengan penugasan proyek (dari *lead link*)

#### Akuntabilitas
1. Memfasilitasi pertemuan-pertemuan dalam lingkaran yang wajib dilaksanakan.
2. Melakukan audit terhadap pertemuan dan catatan dari sub lingkaran sesuai kebutuhan dan melakukan inisiasi proses restorasi yang didefinisikan dalam konstitusi saat menemukan *breakdown* dalam proses.


### Badge Bootstrapper

#### Tujuan
Melakukan penganugrahan lencana (*badge*) secara konservatif dan bijak untuk mem-*bootstrap* cukup banyak orang yang diperlukan ke dalam lencana (*badge*) baru

#### Domain
Sesuai dengan badge yang dikuasai oleh Badge Bootstrapper

#### Akuntabilitas
Melakukan asesmen terhadap partner yang menominasikan dirinya untuk lencana (badge) tertentu ketika lencana tersebut belum banyak dimiliki dan bekerja dengan partner lain yang sudah dapat melaksanakan asesmen secara rasional.


### Badge Librarian

#### Tujuan
Membuat lencana yang merepresentasikan kebutuhan organisasi.

#### Domain
Database Lencana Resmi Nufaza

#### Akuntabilitas
1. Memelihara dan menerbitkan Database Lencana Resmi Nufaza 
2. Melakukan review dan asesmen terhadap proposal penambahan dan perubahan pada Perpustakaan Lencana ketika diusulkan oleh partner dalam organisasi. 
3. Membantu peran lain dan partner dalam menyusun proposal lencana sesuai dengan permintaan dan konfirmasi (*a Role with the power to assign Partners to Roles believes the potential Badge represents a significant consideration in the choice of assignment*)


## Compensation Architect

#### Tujuan
Sistem kompensasi/remunerasi yang pas untuk perusahaan dan para partner

#### Domain

#### Akuntabilitas
1. Mendesain, mengimplementasikan, dan mengevolusikan sistem dalam organisasi secara umum dan proses lainnya yang berkaitan untuk menentukan sistem kompensasi untuk partner
2. Mendefinisikan dan menerbitkan sistem kompensasi dengan sistim level dengan kriteria general atau contoh profil untuk antara set kepemilikan lencana dan level kompensasi yang diharuskan


### Compensation Guardian

#### Tujuan
Memastikan bahwa lencana merepresentasikan kebutuhan organisasi dan sesuai dengan kompensasinya.

#### Domain
Database Lencana Resmi Nufaza

#### Akuntabilitas
1. Melakukan asesmen terhadap proposal terhadap valuasi yang sudah didokumentasikan dalam Badge Valuation.


### Compensation Administrator

#### Tujuan
Dokumentasi akurat mengenai badge/lencana yang dimiliki setiap partner dan level kompensasi yang dihasilkan dari kepemilikan lencana tersebut.

#### Domain
1. Partner Badge Record
2. **Badge Valuation Database**

#### Akuntabilitas
1. Mendefinisikan dan menerbitkan sumber resmi mengenai kepemilikan lencana setiap partner.
2. Melakukan review terhadap bukti fisik yang dikirimkan oleh setiap partner mengenai kepemilikan lencana baru atau pencopotan lencana sesuai dengan proses atau mekanisme yang didefinisikan untuk setiap lencana dan melakukan pemutakhiran pencatatan lencana dan kompensasinya jika sudah terbukti bahwa lencana didapatkan atau dicopot.
3. Melakukan penelusuran terhadap trigger berbasis waktu dan kejadian (event) yang menyebabkan sebuah badge menjadi kadaluarsa, jika terjadi kadaluarsa memberitahukan kepada pihak yang terdampak dan melakukan pemutakhiran pada pencatatan.
4. Menerbitkan dan memelihara **Badge Valuation Database** yaitu mapping antara set beberapa lencana dengan level kompensasi.
5. Mengidentifikasi dan menerbitkan target tingkatan kompensasi untuk setiap partner berdasarkan lencana yang dimiliki oleh partner tersebut pada **Badge Valuation Database**.
6. Melakukan assesmen terhadap partner selama dua, empat, dan enam bulan dalam partnership dan pada waktu-waktu yang diminta oleh Tenured Partner


### Partner Partner

#### Tujuan
Menyediakan bantuan untuk partner

#### Domain
---

#### Akuntabilitas
1. Menjawab pertanyaan dari partner mengenai Holacracy, aktifitas di perusahaan, partnership dan bagaimana semuanya bekerja.
2. Diskusi dengan partner apabila diminta untuk membantu mereka membangun perspektif bagaimana cara memproses tension, termasuk membedakan antara tension yang harus diproses secara personal ataupun secara organisasi.
3. Mendukungn partner untuk mencari jalur pengembangan profesional dalam partnership.
4. Mengumpulkan *feedback* mengenai partner tertentu setelah 3 bulan masa percobaan
5. Menginisiasi dialog dengan partner baru saat masa transisi ke perusahaan.


### Partner Insurance

#### Tujuan
Pendaftaran Asuransi Kesehatan Partern

#### Domain
---

#### Akuntabilitas
1. Administrasi pendaftaran BPJS Kesehatan
2. Administrasi pendaftaran BPJS Ketenagakerjaan