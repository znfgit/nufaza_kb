/*
Title: Intro
Sort: 1
*/

### About

Propel 2 adalah versi yang relatif aktif dipakai sebagai alternatif ORM di PHP. Sebenarnya belum rilis juga, tapi yg mau pakai sebaiknya selalu mengggunakan versi 2 ini.                                                                                                                       
### Installing

Untuk symfony 4.1 instalasi versi yang spesifik berikut sudah dicoba:
```
composer require propel/propel:dev-master
composer require propel/propel-bundle:4.0.x-dev
```                                                                        

### Configuring

Buatlah config/propel.yaml

```
propel:

  paths:
      # Directory where the project files (`schema.xml`, etc.) are located.
      # Default value is current path #
      projectDir: current_path

      # The directory where Propel expects to find your `schema.xml` file.
      schemaDir: config/propel

      # The directory where Propel should output classes, sql, config, etc.
      # Default value is current path #
      outputDir: config/propel

      # The directory where Propel should output generated object model classes.
      phpDir: model

      # The directory where Propel should output the compiled runtime configuration.
      phpConfDir: config/propel

      # The directory where Propel should output the generated DDL (or data insert statements, etc.)
      sqlDir: config/propel

  database:
      connections:
          your_app:
              adapter: sqlsrv
              classname: Propel\Runtime\Connection\DebugPDO
              dsn: sqlsrv:server=localhost;Database=your_app;MultipleActiveResultSets=false
              user: sa
              password: your_password
              attributes:

  reverse: 
      connection: your_app

  runtime:
      defaultConnection: your_app
      connections:
          - your_app

  generator:
      defaultConnection: your_app
      connections:
          - your_app
```

### Reverse Engineering

Sesudah konfigurasi, kita bisa mulai melakukan reverse, dengan mengarahkan config directory dan outputnya.
```
    vendor/bin/propel reverse --config-dir config/packages --output-dir config/propel anisa
```
Jika --output-dir tidak diset maka reverse akan membuat folder `generated-reversed-database` yang agak ugly ya.

Jika keluar pesan `Schema reverse engineering finished.` berarti sudah oke.
Cek hasilnya di folder config/propel.

### Building

Untuk build, simpel saja:
```
vendor/bin/propel build --config-dir config/packages --output-dir=model -v
```

### Config Autoloading
Tambahkan di bawah `"autoload"`(asumsi propel konek ke 2 database):
```
    "classmap": ["model/MyApp", "model/Testproj"]
```
Jadi semacam ini:
```
    "autoload": {
        "psr-4": {
            "App\\": "src/"
        },
        "classmap": ["model/DbOne", "model/DbTwo"]
    },
```
Atau begini saja
```
"classmap": ["model"]
```
