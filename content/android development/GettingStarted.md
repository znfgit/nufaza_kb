/*
Title: Getting Started
Sort: 1
*/

## Instalasi Android Studio

### Download

Downloadnya di sini:
http://developer.android.com/sdk/index.html

### Mengupdate

Membuat project baru pada android studio biasa memulai dengan error rendering.
Solusi paling cepat untuk masalah ini adalah Help > Check For Update,
kemudian update-lah Android Studio dengan yang terbaru.


## Membuat Project Baru

Isi Application name dengan company domain.
Sebagai standarnya, buat company domain dengan subdomain identitas organisasi.
Misalnya:  dev.nufaza.com.  
Nama aplikasi sederhana saja karena akan dipakai di mana2.
Misalnya: Latihan

Utk menentukan minimum SDK baca ini:
https://developer.android.com/about/dashboards/index.html
https://www.appbrain.com/stats/top-android-sdk-versions

Kalau nggak bikin major upgrade aplikasi atau complete rewrite atau fitur hardware baru, DISARANKAN TIDAK MENGUBAH Compiled SDK Version.

## Memilih Layout
Pilih dulu yg Empty biar gak pusing
Kasih nama MainActivity utk class utama, dan main_activity utk layoutnya.

## Running the First Time
Jalankan via emulator atau langsung ke HP via tombol Run.
Maka akan muncul "Hello World" di tengah2.

## Gradle
Adalah aplikasi untuk manajemen build dan dependency. Gradle ada.

#### Versioning
Ada di build.gradle (Module: app) di bawah
```
android {
    ...
    defaultConfig {
        ...
        versionCode 1
        versionName "1.0.0-Alpha"
```

## Mengubah Isi Aplikasi

Tambahkan code ini di main_activity.xml di bawah TextView
    ```
    <TextView
        android:id="@+id/helloText"
    ```
Ini menempelkan id pada komponen TextView sehingga dapat dikontrol oleh MainActivity.

Kita buat code di MainActivity utk mengontrol komponen:

    protected void onCreate(Bundle savedInstanceState) {
        ...
        TextView tv = (TextView) findViewById(R.id.helloText);
        tv.setText("Abah Zuckachev was here");

Kemudian jalankan ulang app.



## Troubleshooting

Jika ketemu error
```
The following classes could not be instantiated:
    - android.support.v7.internal.app.WindowDecorActionBar
```
Maka edit `build.gradle` for Module:app, cari:
```
dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    testCompile 'junit:junit:4.12'
    compile 'com.android.support:appcompat-v7:23.0.1'
    compile 'com.android.support:design:23.0.1'
}
```
Kemudian build lagi.


## IDE

#### Code Explorer
Layout paling enak untuk melihat code explorer ini adalah "Android".
Lihat kiri atas Ada tombol di sebelahnya ada panah 2 arah, dgn tulisan "Project" ganti dengan "Android"

#### Tips Editor
Gunakan theme gelap agar mata gak cape. Caranya ke File > Settings > Appearance & Behaviour > Theme --> Ganti Darcula

Agar font bisa ubah2 ukuran ke
File > Settings > Editor > General --> Check "Change font size with Ctrl+Mouse Wheel"


## Debugging dengan HP

### Download dan install dulu USB Driver

Buka di sini, ada linknya:
http://developer.samsung.com/technical-doc/view.do?v=T000000117

### Mengaktifkan USB Development

Step-by-stepnya:
-   Pasang USB
-   Settings > About Phone > Software
-   Click build number 7x
-   Kembali, akan ada menu Developer Mode
-   Enable USB debugging. Colokin HP ke USB.
-   Edit lagi `build.gradle`, tambahkan di bawah buildTypes:
    ```
        ...

        buildTypes {
            debug {
                debuggable true
            }
            ...
    ```

## Hasil build
Ada di sini:
C:\Projects\android\Latihan\app\build\outputs\apk\

## Menggunakan Vector Icons

Vector icons ready use ada di:
`%HOMEPATH%\AppData\Local\Android\sdk\platforms\android-23\data\res\drawable\`

Kalau ingin lebih lengkap, bisa pakai icons siap pakai utk material design:
`$ git clone https://github.com/google/material-design-icons`

Terus diimportnya pakai Vector Asset Studio. Di Android studio cari res, klik kanan > Vector Asset.
Terus buka Local SVG file hasil clone tadi.
