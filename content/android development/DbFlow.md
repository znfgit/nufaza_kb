/*
Title: Menggunakan ORM: dbflow 
Sort: 3
*/

## Menginstall

Buka dokumentasi instalasi di 
https://github.com/SublimeText-Markdown/MarkdownEditing
untuk update cara instalasi.

Ubah `ProjectFolder\build.gradle` menjadi semacam ini (read comments utk tambahan yg dilakukan):

```
// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:1.5.0'

        // Add support for dbflow
        classpath 'com.neenbedankt.gradle.plugins:android-apt:1.8'

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        jcenter()

        // Add support for dbflow
        maven { url "https://jitpack.io" }
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}
```

Ubah `ProjectFolder\app\build.gradle` menjadi semacam ini:

```
apply plugin: 'com.android.application'

// Abh add dbflow
apply plugin: 'com.neenbedankt.android-apt'
def dbflow_version = "3.0.0-beta4"

android {
    compileSdkVersion 23
    buildToolsVersion "23.0.2"

    defaultConfig {
        applicationId "net.ditpsd.rehab"
        minSdkVersion 15
        targetSdkVersion 23
        versionCode 1
        versionName "1.0"
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    testCompile 'junit:junit:4.12'
    compile 'com.android.support:appcompat-v7:23.1.1'
    compile 'com.android.support:design:23.1.1'

    // Abh add dbflow
    apt "com.github.Raizlabs.DBFlow:dbflow-processor:${dbflow_version}"
    // sql-cipher database encyrption (optional)
    compile "com.github.Raizlabs.DBFlow:dbflow-sqlcipher:${dbflow_version}"
}

```

Kemudian Build > Rebuild Project.

## Mencoba

### Aplikasi

Pertama bikin dulu Application class. Di src buatlah, misal nama app anda Rehab, maka Classnya:
```
public class RehabApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
    }
    
    @Override
    public void onTerminate() {
        super.onTerminate();
        FlowManager.destroy();
    }
}
```

Buka AndroidManifest.xml, tambahkan atribut name pada tag <application> :
```
    ...
    <application
        android:name=".RehabApplication"
    ...
```


### Database 

Buat Database Classs
```
package net.ditpsd.rehab;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = RehabDatabase.NAME, version = RehabDatabase.VERSION)

public class RehabDatabase {
    public static final String NAME = "Rehab";
    public static final int VERSION = 1;

}
```

### Membuat Model

Buatlah model classnya. Sebagai contoh, "Sekolah"
```
package net.ditpsd.rehab.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import net.ditpsd.rehab.RehabDatabase;

@Table(database = RehabDatabase.class)
public class Sekolah extends BaseModel{

    @PrimaryKey
    public String sekolah_id;

    @Column
    public String nama_sekolah;

    @Column
    public String alamat_sekolah;

    @Column
    public int jumlah_ruang_rusak;

    @Column
    public int jumlah_ruang;

}
```

### Rebuild
Dilakukan agar DBFlow menggenerate file2 berdasarkan file-file database dan model tadi.

### Mengisi data
Kita coba mengisi data dummy di awal aplikasi dimulai. Di RehabApplication tambahkan sehingga menjadi seperti ini:

```
package net.ditpsd.rehab;

import android.app.Application;
import android.widget.Toast;

import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Select;

import net.ditpsd.rehab.model.Sekolah;

import java.util.UUID;


public class RehabApplication extends Application {

    @Override
    public void onCreate() {

        super.onCreate();
        FlowManager.init(this);

        //SQLite.selectCountOf()
        long count = SQLite.selectCountOf().from(Sekolah.class).count();
        Toast.makeText(getApplicationContext(), "Ditemukan data " + count, Toast.LENGTH_LONG).show();

        if (count < 1) {
            Sekolah sekolah = new Sekolah();
            sekolah.sekolah_id = UUID.randomUUID().toString();
            sekolah.nama_sekolah = "SD Panjang Akal 1";
            sekolah.alamat_sekolah = "Jl. Sudirman 212 - Kota Cimahi";
            sekolah.jumlah_ruang_rusak = 12;
            sekolah.jumlah_ruang = 22;
            sekolah.save();

            sekolah = new Sekolah();
            sekolah.sekolah_id = UUID.randomUUID().toString();
            sekolah.nama_sekolah = "SD Gembira Ria 1945";
            sekolah.alamat_sekolah = "Jl. Gembira 313 - Kota Cimahi";
            sekolah.jumlah_ruang_rusak = 13;
            sekolah.jumlah_ruang = 19;
            sekolah.save();

            sekolah = new Sekolah();
            sekolah.sekolah_id = UUID.randomUUID().toString();
            sekolah.nama_sekolah = "SD Cerdas Sejahtera";
            sekolah.alamat_sekolah = "Jl. Peta 354 - Kota Cimahi";
            sekolah.jumlah_ruang_rusak = 21;
            sekolah.jumlah_ruang = 25;
            sekolah.save();

            sekolah = new Sekolah();
            sekolah.sekolah_id = UUID.randomUUID().toString();
            sekolah.nama_sekolah = "SD Akhlak Jaya";
            sekolah.alamat_sekolah = "Jl. Pejuang 45 - Kota Cimahi";
            sekolah.jumlah_ruang_rusak = 9;
            sekolah.jumlah_ruang = 13;
            sekolah.save();
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        FlowManager.destroy();
    }
}
```


### Mencek hasilnya
Jalankan emulator. Akan muncul "SD Card" di notification bar. Setup dulu. Pilih saja yang atas (For transferring photos and media).

Buka Tools > Android > Android Device Monitor.
Pilih aplikasi kita di sebelah kiri. Di sebelah kanan di tab File Explorer browse-lah ke data/data/{namespace aplikasi kita}. Di situ ada file database kita sesuai yang telah kita beri nama di class Database tadi. Tekan gambar disk untuk mengunduh dan memeriksa isinya.

## Mencoba


### Prepackaged Databases
Penting!
To include a prepackaged database for your application, simply include the ".db" file in src/main/assets/{databaseName}.db. On creation of the database, we copy over the file into the application for usage. Since this is prepackaged within the APK, we cannot delete it once it's copied over, leading to a large APK size (depending on the database file size).