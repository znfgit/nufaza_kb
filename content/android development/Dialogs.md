
Sekalian belajar menggunakan third party library.
Copas code ini, masukin ke build.gradle (app) di  bawah dependencies

```
dependencies {
    ...
    compile('com.github.afollestad.material-dialogs:core:0.8.5.6@aar'){
        transitive = true
    }
```

Dan juga masukkan ini di bawah allprojects:
```
allprojects {
    repositories {
        jcenter()
        maven { url "https://jitpack.io" }
    }
}
```

Kemudian kita tambahkan di code tadi:
```
      final MaterialDialog md = new MaterialDialog.Builder(MainActivity.this)
              .title("Info")
              .content("Anda menulis " + ed.getText() + ". Bener? ")
              .negativeText("Nggak Ah")
              .positiveText("Iyah")
              .show();

      md.getActionButton(DialogAction.NEGATIVE).setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Toast.makeText(getApplicationContext(), "Oh nggak yah?", Toast.LENGTH_LONG).show();
              md.dismiss();
          }
      });

      md.getActionButton(DialogAction.POSITIVE).setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Toast.makeText(getApplicationContext(), "Tuuuh bener kan?", Toast.LENGTH_LONG).show();
              md.dismiss();
          }
      });
```
