/*
Title: ListView
Sort: 2


## Memahami ListView

ListView itu komponen dasar untuk menampilkan segala sesuatu list di android.

### Membuat ListView

Cara bikin listview bisa dilakukan dari  _design view_ di Android Studio,
tinggal drag and drop dari daftar komponen ke activity yang diinginkan.

Sebagai contoh listview yg ditampilkan secara full sbb:
	```
    <ListView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:id="@+id/listview_sekolah"
        android:layout_alignParentTop="true"
        android:layout_alignParentLeft="true"
        android:layout_alignParentStart="true"
        />
	```        

### Menandai pilihan listview (Selection)

Tinggal tambahkan di dalam tagnya:
	```
	<ListView
		...
        android:choiceMode="singleChoice"
        android:listSelector="#666666"
    	...
    	/>
    ```

## Layout

### Menambahkan listview di bawah form
Tambahkan code sbb di main_activity

```
<ListView
		android:id="@+id/listViewNama"
		android:layout_width="match_parent"
		android:layout_height="298dp"
		android:layout_weight="1.02"/>
```
Jangan lupa perbesar parentnya.

### Menambahkan layout item
Menambahkan layout utk per item listView:
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
                             android:orientation="vertical"
                             android:layout_width="match_parent"
                             android:layout_height="match_parent">
    <TextView
        android:id="@+id/textViewNama"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="TextView"/>
</LinearLayout>
```

## Code

### Class Variable
Pertama buat class variable utk listview sbb:

```
// Ini temporary storage untuk listView.
List<String> listNama = new ArrayList<>(); // Harus diinisialisasi

// Ini listview diset sebagai class variable agar accessible anywhere
ListView listViewNama = null;  // Inisialisasi di onCreate
```

### Membuat Adapter Listview
Kedua, buat adapternya
```
// Buat adapter yang mendefinisikan bahwa adapter ini disuplay datanya dari listNama
    private class StrListAdapter extends ArrayAdapter<String> {
        public StrListAdapter() {
            super(MainActivity.this, R.layout.lv_item, listNama);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            // Pastikan ada view kalau null
            View itemView = convertView;

            if (itemView == null) {

                // Cari itemview yang mendefinisikan format penampilan data di tiap baris listview
                itemView = MainActivity.this.getLayoutInflater().inflate(R.layout.lv_item, parent, false);

                // Untuk setiap baris, ambil dari listview nama
                String namaStr = listNama.get(position);

                // Cari textview di format tadi, terus kita isi dengan data.
                TextView textViewNama = (TextView) itemView.findViewById(R.id.textViewNama);

                // Kemudian isikan textview tsb dengan string nama
                textViewNama.setText(namaStr);
            }

            return itemView;
        }
    }
```

### Initialization Code
Tambahkan di bawah onCreate, code untuk menginisialisasi ListView
```
listViewNama = (ListView) findViewById(R.id.listViewNama);
```

### Add Refresh Code
Tambahkan code utk me-refresh isi listview agar hasil isian terupdate.

```
private void populateListViewNama() {

        // Definisikan adapter untuk mengisi listview tersebut
        ArrayAdapter<String> lvAdapter = new StrListAdapter();

        // Kasihtahu listview bahwa adapternya adalah lvAdapter
        listViewNama.setAdapter(lvAdapter);
        listViewNama.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
    }
```

### Pada Action Button Jawab Yes
```
listNama.add(tv.getText().toString());
populateListViewNama();
```
