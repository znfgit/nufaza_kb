/*
Title: ADB
Sort: 4
*/

Android itu core OS-nya linux. Android development berkomunikasi dan menjalankan berbagai perintah baik ke emulated device maupun physical android device melalui linux commands. Jadi kita bisa bermain di sini jika punya dasar linux.

### Include adb.exe to path.
Agar lebih nyaman, masukkan directory adb ke path. Utk pemakai Android Studio sekitar tahun 2015-2016, basisnya IDEnya Jetbrains, dan Android SDK berikut ADB tools-nya disimpan di folder user penginstallnya. Kalau saya di sini:
`%HOMEPATH%\AppData\Local\Android\sdk\platform-tools\`.
Tambahkan path tadi ke system information: Control Panel\System and Security\System > Advanced System Settings >Environment Variables > Path. Sisipkan saja path di atas ke belakang stringnya.

### Playing with ADB Shell
Kita coba bermain-main dengan shell ke android device. Pertama tampilkan daftar device yang terkonek.
```
C:> adb devices
List of devices attached
emulator-5554   device
0515453432304434        device
192.168.56.101:5555     device
```
Setelah ditemukan, mari kita ambil shellnya:
```
C:> adb -s 0515453432304434 shell
shell@noblelte:/ $ _
```
Nah itu sudah linux shell. Silakan jalan2 di shell tersebut utk menelusuri mounted storage di device. Ingat, gunakan shell command linux mulai saat ini:
```
shell@noblelte:/storage $ cd /storage
shell@noblelte:/storage $ ls -al
d--------- system   system            2016-02-27 06:17 Private
drwx------ root     root              2016-02-27 06:17 UsbDriveA
drwx------ root     root              2016-02-27 06:17 UsbDriveB
drwx------ root     root              2016-02-27 06:17 UsbDriveC
drwx------ root     root              2016-02-27 06:17 UsbDriveD
drwx------ root     root              2016-02-27 06:17 UsbDriveE
drwx------ root     root              2016-02-27 06:17 UsbDriveF
dr-xr-xr-x root     root              2016-02-27 06:17 emulated
drwx--x--x system   system            2016-02-27 06:17 enc_emulated
drwx------ root     root              2016-02-27 06:17 extSdCard
lrwxrwxrwx root     root              2016-02-27 06:17 knox-emulated -> /mnt/shell/knox-emulated
lrwxrwxrwx root     root              2016-02-27 06:17 sdcard0 -> /storage/emulated/legacy
shell@noblelte:/storage $ _
```

Untuk clear screen gunakan `clear`

### Mengunduh File dari Device

Untuk mengunduh file dari device, gunakan perintah `adb pull`. Sebagai contoh di sini kami mengunduh database SQLite dari device.
```
adb -s 192.168.56.101:5555 pull /data/data/net.ditpsd.rehab/databases/Rehab.db
```

Catatan:
Untuk device fisik, ada kemungkinan path di atas tidak terdeteksi karena permission yang ketat. Oleh karena itu disarankan untuk development gunakanlah device yg sudah di-root. Atau gunakan emulator.