/*
Title: Mengambil Foto
Sort: 5
*/

Di contoh ini kami menguraikan code yang mengambil foto kemudian langsung menyimpannya ke external memory.

### Memahami Intent
Intent atau maksud/niat adalah terminologi Android untuk pelaksanaan task besar. Misal sebuah aplikasi ingin mengambil foto, atau menampilkan image, atau men-share image, dlsb yang dapat dilakukan aplikasi lain. Jadi aplikasi kita tidak usah menulis semua sendiri.

### Intent Image Capture
Di contoh ini yg kita pakai adalah intent dengan kode MediaStore.ACTION_IMAGE_CAPTURE. Pertama kita cek dulu apakah di sistem ada activity yg dapat melayani intent yg di maksud.

Di sini digunakan extra intent MediaStore.EXTRA_OUTPUT, sehingga logikanya sbb:
- Dibuat dulu imageFile kosong, caranya memanggil method createImageFile()
- Tendang activity untuk picture intent
- Setelah camera selesai, gambar langsung tersimpan di output file,
  tapi nama filenya bertambah panjang sedikit (belum diketahui kenapa).
- Method `onActivityResult` membantu pemanggilan penyimpanan thumbnail dan database

### Memanggil Intent
Di bawah ini method yg dipisah untuk menjalankan fungsi pengambilan gambar oleh aplikasi default kamera. Untuk menggunakannya, panggil saja fungsi ini dari salah satu tombol atau menu apa saja di aplikasi.

```java
static final int REQUEST_TAKE_PHOTO = 1;    // Menyeragamkan kode Intent

private void dispatchTakePictureIntent() {
    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    // Ensure that there's a camera activity to handle the intent
    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
        // Create the File where the photo should go
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
            Log.e("createImageFile()", ex.toString());
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            Toast.makeText(getApplicationContext(), "Writing to: " + photoFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(photoFile));
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
        }
    }
}
```

### Pembuatan File Image kosongan
Sedangkan method createImageFile() yg digunakan di atas:
```java
private File createImageFile() throws IOException {
    
    // Create an image file name
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    imageId = "foto_" + timeStamp;

    // Compose storage path, create the folders if it's not exist yet
    String storagePath = externalPath;
    File storageDir = new File(storagePath);
    if (!storageDir.isDirectory()) {
        storageDir.mkdirs();
    }

    // Debugging purposes
    // Toast.makeText(getApplicationContext(), "Storage dir: " + storageDir.toString(), Toast.LENGTH_LONG).show();
    // Create destination file
    File image = File.createTempFile(
            imageId,  /* prefix */
            ".jpg",         /* suffix */
            storageDir      /* directory */
    );

    // Save a file: path for use with ACTION_VIEW intents
    // mCurrentPhotoPath = "file:" + image.getAbsolutePath();
    //Debugging purposes
    //Toast.makeText(getApplicationContext(), "Image path: " + mCurrentPhotoPath, Toast.LENGTH_LONG).show();
    return image;
}
```

Jika anda ingin mengatur penempatan filenya, silakan atur sendiri pathnya id bagian storage path di atas. Misal pengelompokan dsb.

### Melakukan Proses Sesudah Intent Berhasil
Berikut ini adalah contoh menangkap berhasil tidaknya hasil intent, jika berhasil kita dapat melakukan beberapa task. Misalnya di bawah ini adalah penyimpanan thumbnail, update infonya ke database dan me-refresh list dengan gambar hasil foto (jika misal ditampilkan di listview dengan gambar).

```java
protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

        boolean saveThumbnailSuccess = saveThumbnailToInternalStorage();
        boolean attachImageToDbSucccess = attachImageToDatabase();

        // Update changes
        populateListSekolah();
        populateListViewSekolah();

        String message = "";
        if (saveThumbnailSuccess) {
            message += "Menyimpan thumbnail berhasil. ";
        }
        if (attachImageToDbSucccess) {
            message += "Menyimpan data image ke database berhasil. ";
        }
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
```
