/*
Title: Layouts
Sort: 2


## Memahami Layouts

Layout itu fundamental komponen untuk merakit aplikasi android. Semua kontrol, panel, gambar, dan segala macam tipe tampilan lain ditempelkan pada layout.

### Merakit UI pada LinearLayout

Pertama hapus dulu layout sebelumnya di main_activity.xml, kemudian ganti dengan ini:
```
<?xml version="1.0" encoding="utf-8"?>
<FrameLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="com.nufaza.abah.latihan.MainActivity">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="200dp"
        android:orientation="vertical">

        <TextView
            android:id="@+id/textHasil"
            android:textSize="40dp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="@string/resultText"/>

        <EditText
            android:id="@+id/textEditor"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:ems="10"
            android:inputType="text"
            android:hint="@string/hintEditor"
            android:text=""/>

        <Button
            android:id="@+id/btnGanti"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="@string/buttonReplaceText"/>
    </LinearLayout>

</FrameLayout>
```

Kemudian buat controller code untuk mengaitkan tombol, editor dengan textview.
```
	super.onCreate(savedInstanceState);
	setContentView(R.layout.main_activity);

	final TextView tv = (TextView) findViewById(R.id.textHasil);
	final EditText ed = (EditText) findViewById(R.id.textEditor);
	Button btn = (Button) findViewById(R.id.btnGanti);

	btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					tv.setText(ed.getText());
			}
	});
```

Mohon perhatikan `android:id="@+id/btnGanti"` ini untuk memberi identifikasi pada komponen, dan `android:text="@string/buttonReplaceText"` untuk menyimpan teks konstant. Mengapa tidak hardcoded saja ditulis teksnya misalnya "Ganti" gitu? Karena android secara default mengarahkan coder utk antisipasi internasionalisasi. Dengan ini utk bahasa lain kita tinggal bikin res/values/strings.xml yg lain.
