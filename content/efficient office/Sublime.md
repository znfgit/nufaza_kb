/*
Title: Sublime
Sort: 2
*/ 

Sublime adalah Text Editor yang tengah populer saat ini khususnya di kalangan programmer. Sublime sekarang sudah mencapai versi 3, namun disarankan untuk tidak menginstall yang terlalu baru, karena versi terakhir sampai ditulis sudah tidak lagi "Nag-ware", namun sudah "Pay-ware".

Menggunakan sublime biasa saja. 