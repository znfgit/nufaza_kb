/*
Title: Total Commander
Sort: 1
*/ 

Total Commander adalah sebuah aplikasi yang sangat membantu dalam menjalankan beberapa perintah pada Windows. Selain ringan, Total Commander memiliki banyak sekali fitur. Beberapa fitur tersebut antara lain: navigasi file yang super cepat, batch rename, dukungan FTP dan Network Neighbourhood dan masih banyak lagi. Selengkapnya bisa dilihat [disini](http://www.ghisler.com/featurel.htm "Daftar Fitur Total Commander"). Bagi seorang developer yang menginginkan produktivitas tinggi, ini adalah aplikasi wajib install.

Namun jika yang belum pernah menggunakannya sama sekali, mungkin akan menemui sedikit kesulitan dikarenakan banyaknya shortcut yang ada. Berikut ini adalah beberapa list shortcut (sekitar 112 shortcut) yang mungkin akan sangat membantu dalam menggunakan Total Commander:

### Daftar Shortcut  

| Shortcut | Fungsi |
|----------|--------|
| `F1` | Jendela Bantuan Total Commander |
| `F2` | Reload drive |
| `F3` | Melihat informasi lengkap file/folder yang terpilih (ukuran, isi, jumlah dll) di internal editor |
| `F4` | Mengedit file dengan text editor default |
| `F5` | Mengcopy file |
| `F6` | Rename atau memindahkan file |
| `F7` | Membuat folder baru |
| `F8` | Menghapus file ke Recycle Bin |
| `F9` | Memilih tool menu paling atas (menubar) |
| `F10` | Sama seperti `F9` |
| `Alt`+`F1` | Merubah drive yang ada di panel kiri |
| `Alt`+`F2` | Merubah drive yang ada di panel kanan |
| `Alt`+`F3` | Membuka file dengan internal text editor |
| `Alt`+`Shift`+`F3` | Sama seperti `Alt`+`F3` |
| `Alt`+`F4` | Keluar aplikasi |
| `Alt`+`F5` | Compress file/folder (ZIP, RAR, TAR, dll) |
| `Alt`+`Shift`+`F5` | Compress file/folder dan menghapus aslinya  |
| `Alt`+`F6` | Uncompress file |
| `Alt`+`F7` | Pencarian file |
| `Alt`+`F8` | Membuka history di command line |
| `Alt`+`F9` | Uncompress file. Sama seperti `Alt`+`F6` |
| `Alt`+`Shift`+`F9` | Mengetes arsip file (CRC Check) |
| `Alt`+`F10` | Membuka list folder dalam bentuk tree di jendela baru |
| `Alt`+`F11` | Membuka direktori bar di panel kiri |
| `Alt`+`Shift`+`F11` | Mengakses tombol toolbar dengan keyboard |
| `Alt`+`F12` | Membuka direktori bar di panel kanan |
| `Alt`+`Kanan`/`Kiri` | Membuka folder yang sudah pernah dikunjungi |
| `Alt`+`Bawah` | List history folder yang pernah dikunjungi |
| `Alt`+`Num`+`+` | Memilih semua file dengan extensi yang sama |
| `Alt`+`Num`+`-` | Menghapus semua file dengan extensi sama dari seleksi |
| `Alt`+`Shift`+`Enter` | Menampilkan semua size folder di list |
| `Alt`+`Enter` | Menampilkan jendela *properties* file/folder |
| `Shift`+`F1` | Merubah tampilan view explorer |
| `Shift`+`F2` | Membandingkan daftar file |
| `Shift`+`F3` | Melist file yang hanya berada dibawah kursor, hanya ketika beberapa file dipilih |
| `Shift`+`F4` | Membuat file baru dan dibuka dengan external editor |
| `Shift`+`F5` | Copy file (lalu rename) di direktori yang sama |
| `Shift`+`Ctrl`+`F5` | Membuat shortcut (.lnk) dari file yang dipilih |
| `Shift`+`F6` | Rename file di direktori yang sama |
| `Shift`+`F8`/`Delete` | Menghapus langsung ke recycle bin - tergantung konfigurasi |
| `Shift`+`F10` | Menampilkan context menu (seperti klik kanan mouse) |
| `Shift`+`Esc` | Minimize jendela Total Commander |
| `Shift`+`Num`+`+` | Seperti `Num`+`+`, hanya saja di `Num`+`+` cuma memilih file (dan kebalikannya) |
| `Shift`+`Num`+`-` | Menghapus seleksi dari file (`Num`+`-` dari file dan folder) |
| `Shift`+`Num`+`*` | Seperti `Num`+`*`, tapi file dan folder pada `Num`+`*` membalikan seleksi file (dan kebalikannya) |
| `Shift`+`Tab` | Berpindah antara current file list dan separate tree (jika di-enabled) |
| `Shift`+`Enter` | - Menjalankan command line/ program under cursor with preceding command /C dan leave the program'S window open. Only works if NOCLOSE.PIF is in your Windows direktori! |
| | - With ZIP file: use alternative choice of these (as chosen in Packer config): (Treat archives like directories call associated program, I.e. winzip atau quinzip) |
| | - Jika berada di dalam daftar history (`Ctrl`+`D`), buka direktori terpilih di tab baru. |
| `Ctrl`+`Num` + | Seleksi semua (dapat dikonfigurasi: hanya file atau file dan folder) |
| `Ctrl`+`Shift`+`Num` `+` | Seleksi semua (file dan folder jika `CTRL`+`Num`+`+` hanya menyeleksi file) |
| `Ctrl`+`Num` - | Deselect semua (file dan folder) |
| `Ctrl`+`Shift`+`Num` `-` | Deselect semua (hanya file) |
| `Ctrl`+`Page up` | Kembali ke direktori sebelumnya (seperti cd ..), atau `Backspace` |
| `Ctrl`+`<` | Loncat ke root direktori (European keyboards) |
| `Ctrl`+`\` | Loncat ke root direktori (US keyboard) |
| `Ctrl`+`Page down` | Membuka direktori/arsip (dan mengekstrak arsip .EXE secara otomatis) |
| `Ctrl`+`Kiri`/`Kanan` | Membuka direktori/arsip dan menampilkannya di panel sebelah kiri/kanan |
| `Ctrl`+`F1` | Tampilan panel 'brief' (hanya nama file) |
| `Ctrl`+`Shift`+`F1` | Tampilan thumbnails (preview gambar) |
| `Ctrl`+`F2` | Tampilan panel 'full' (semua detail file) |
| `Ctrl`+`Shift`+`F2` | Tampilkan komentar (komentar baru dibuat/diedit dengan `Ctrl`+`Z`) |
| `Ctrl`+`F3` | Urut berdasarkan nama |
| `Ctrl`+`F4` | Urut berdasarkan extensi |
| `Ctrl`+`F5` | Urut berdasarkan tanggal/waktu |
| `Ctrl`+`F6` | Urut berdasarkan ukuran |
| `Ctrl`+`F7` | Urut acak |
| `Ctrl`+`F8` | Merubah tampilan panel menjadi direktori tree (seperti di Windows Explorer) |
| `Ctrl`+`Shift`+`F8` | Menampilkan direktori tree di sebelah kiri panel |
| `Ctrl`+`F9` | Print file langsung tanpa membukanya (hanya dokumen) |
| `Ctrl`+`F10` | Tampilkan/filter semua file (\*.\*) |
| `Ctrl`+`F11` | Tampilkan/filter hanya program |
| `Ctrl`+`F12` | Tampilkan/filter berdasarkan inputan |
| `Ctrl`+`A` | Pilih semua |
| `Ctrl`+`B` | Menampilkan semua isi dari folder dan subfolder dari direktori aktif dalam satu list |
| `Ctrl`+`Shift`+`B` | Menampilkan semua isi dari folder dan subfolder dari direktori yang terpilih dalam satu list |
| `Ctrl`+`C` | Copy file ke clipboard |
| `Ctrl`+`X` | Cut file ke clipboard |
| `Ctrl`+`V` | Paste dari clipboard ke direktori aktif |
| `Ctrl`+`D` | Membuka bookmarks folder |
| `Ctrl`+`F` | Membuka jendela koneksi ke FTP server |
| `Ctrl`+`Shift`+`F` | Disconnect dari FTP server |
| `Ctrl`+`I` | Pindah ke target direktori |
| `Ctrl`+`L` | Menghitung space yang terpakai dari file/folder yang terpilih |
| `Ctrl`+`M` | Membuka jendela Multi-Rename |
| `Ctrl`+`Shift`+`M` | Merubah mode transfer FTP |
| `Ctrl`+`N` | Koneksi FTP baru dengan memasukan url atau host |
| `Ctrl`+`P` | Copy path aktif ke command line |
| `Ctrl`+`Q` | Melihat informasi lengkap file/folder yang terpilih (ukuran, isi, jumlah dll) di panel sebelah kanan/kiri  |
| `Ctrl`+`R` | Reload direktori |
| `Ctrl`+`S` | Membuka jendela Quick Filter (keluar dengan ESC atau `CTRL`+`F10`) |
| `Ctrl`+`Shift`+`S` | Membuka jendela Quick Filter dan mengaktifkan filter terakhir digunakan |
| `Ctrl`+`T` | Membuat tab baru dan pindah kedalamnya |
| `Ctrl`+`Shift`+`T` | Membuat tab baru, tapi tidak berpindah |
| `Ctrl`+`U` | Pertukaran antar panel kanan dan kiri |
| `Ctrl`+`Shift`+`U` | Pertukaran panel dan tab |
| `Ctrl`+`W` | Menutup tab yang sedang aktif |
| `Ctrl`+`Shift`+`W` | Menutup semua tab yang terbuka |
| `Ctrl`+`Z` | Buat/Edit komentar pada file |
| `Ctrl`+`Atas` | Membuka direktori dibawah kursor di tab baru |
| `Ctrl`+`Shift`+`Atas` | Membuka direktori dibawah kursor di jendela baru (tab baru) |
| `Ctrl`+`Tab` | Pindah ke tab selanjutnya |
| `Ctrl`+`Shift`+`Tab` | Pindah ke tab sebelumnya |
| `Ctrl`+`Alt`+(Huruf) | Pencarian cepat pada file di direktori aktif (Mendukung hotkeys `Ctrl`+`X`, `Ctrl`+`C`, `Ctrl`+`V` dan `Ctrl`+`A`; gunakan `Ctrl`+`S` untuk memfilter pencarian) |
| `Num`+`+` | Expand seleksi (dapat dikonfigurasi: hanya file atau file dan folder) |
| `Num`+`-` | Shrink seleksi |
| `Num`+`*` | Invert seleksi (bisa juga dengan `Shift`) |
| `Num`+`/` | Mengembalikan seleksi |
| `Tab` | Berpindah antara panel/jendela kanan dan kiri |
| `Insert` | Select file atau direktori. |
| `Space` | Select file atau direktori (seperti `Insert`). If `Space` is used on an unselected direktori under the cursor, the contents in this direktori are counted dan the size is shown in the "full" view instead of the string . This can be disabled through 'Configuration' - 'Options' - 'Operation' - 'Selection with Space'. |
| `Enter` | Membuka direktori/menjalankan program/mengeksekusi command line jika tidak kosong |

*Sumber: [Shortcutworld.com](http://www.shortcutworld.com/en/win/Total-Commander_8.html "112 Keyboard Shortcuts for Total Commander 8").

### Navigasi

Untuk menelusuri struktur direktori menggunakan TC, berikut caranya
- Navigasi gunakan panah (atas bawah). 
- Bisa gunakan page down/up utk direktori panjang.
- Untuk ke paling atas gunakan Home, paling bawah End
- Untuk kembali ke folder parent, paling cepat gunakan kombinasi 
  Home-Enter Home-Enter
- Pindah sisi kanan dan kiri gunakan tombol Tab

Untuk pindah drive:
- Sisi kiri pakai Alt-F1
- Sisi kanan pakai Alt-F2
- Catatan: Jika salah satu sisi dipindah dgn drive yang sama, maka foldernya akan mengikuti direktori sisi lawannya.

Menggunakan tab : 
- tab baru gunakan Ctrl-Tab
- pindah antar tab di salah satu side gunakan Ctrl-Tab (kanan) dan 
  Ctrl-Shift-Tab (kiri)

### Bookmark Direktori

Saat bekerja kita sering mengakses direktori tertentu. Untuk keperluan ini TC menyediakan bookmark.
- Bookmark direktori gunakan Ctrl-D. 
- Agar bookmark bisa menggunakan shorcut letter kasih tanda & sebelum tombol pada title. 
  Contoh: %localhost --> akan menggunakan huruf L, sehingga untuk mengakses direktori tersebut cukup lakukan Ctrl-D, L.

### Operasi file

Berikut cara operasi pada file
- Copy gunakan F5
- Copy dengan queue (antrian) perlu agar file di hardisk tetap rapi. 
  Gunakan F5, F2
- Move folder gunakan F6
- Rename folder gunakan Shift-F6
- Membuat folder gunakan F7
- Menghapus file/folder ke recycle bin F8
- Menghapus file/folder selamanya gunakan Shift-F8
- Ngopi dengan cepat gunakan tombol Menu lalu send to. Ini efektif untuk 
  kopi file ke flashdisk.

### Selection
TC mempermudah pemilihan multiple file/folder. 

Selection manual:
- Select bisa menggunakan spasi, insert 
- Deselect milih bisa menggunakan yg sama, 
- Bisa dengan tekan shift terus arahkan panah

Selection by filter:
- Select Gunakan + (di laptop biasanya pakai Fn-/)
- Deselect Gunakan - (di laptop biasanya pakai Fn-;)

Action
- Setelah select, pencet tombol "Menu" (sebelah Alt yg kanan gambarnya kotak). Efeknya sama dengan klik kanan

Multi rename tool
- Pilih dulu file2 yg  akan diubah, tekan Ctrl-M. Lalu search replace 
  sesuai dengan pola yg ada.

View/edit file:
- Untuk text, pencet F4. Akan keluar editor notepad. Kalau notepad ingin diganti, 
  buka Configuration > Options > Cari menu Edit/View > Ganti di field editor arahkan ke aplikasi yg diinginkan.
- Untuk view text file pencet F3

Menyalin path pada direktori yg sedang aktif:
- Ctrl-P, Panah kiri, Ctrl-X
- Kemudian di aplikasi yg memerlukan, Ctrl-V

Searching: Gunakan Alt-F7
- Mencari file berdasarkan filter nama file isi di field "Search for:" misal.. "*.php"
- Mencari file berdasarkan isinya, bisa isi di field "Find text:"

Kompresi file
- Pertama select dulu folder atau beberapa file.
- Tombol Menu > Pack file --> zip
- Kalau sudah install 7z, tombol Menu > Add to 7z archive.
- Catatan: 
    kalau folder yg dikompress, maka ketika di-"extract here" maka yg keluar adalah folder yg tadi.
    kalau file-file yg dikompress langsung, maka ketika di-extract here, keluar file-filenya di folder ybs.

Toolbar
- Di toolbar ada tombol2 yg berguna. Ada thumbnail view, ada all file details. 
- Thumbail view berguna utk preview file-file gambar untuk pengelolaan file gambar. Misal utk memilah file by event.
- All file details perlu agar bisa sorting per extension, ukuran atau tanggal.
