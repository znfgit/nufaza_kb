/*
Title: Intro
Sort: 1
*/

### Installing
Installing microframework
`composer create-project symfony/skeleton symfony4_training`

Installing website ready
`composer create-project symfony/website-skeleton symfony4_training`

### Local Server
Menyalakan local server
```
cd symfony4_training
php bin/console 
composer require server
php bin/console server:start
```

### PHPStorm Plugin
Menggunakan PHPStorm Plugins
Settings > Plugins > Browse Repositories
Symfony Plugin
PHP Annotations

### Route
Create first route - routes.yaml
```
hello_page:
  path: /hello
  controller: Symfony\Bundle\FrameworkBundle\Controller\TemplateController::templateAction
  defaults:
    template: 'hello_page.html.twig'
```

### Annotation Route
Create controller annotation route. Kalau belum jalan install dulu.
`composer require annotations`

### Creating Controller
Membuat controller simpel
`php bin/console make:controller`, kemudian di prompt ketik `> WelcomeController`


```
    /**
     * @Route("/", name="welcome")   <-- change this to root
     */
    public function index()
    {
```

Check routes
`php bin/console debug:router`

Create response
```
return new Response('Hello', Response::HTTP_OK);
```

Automatic twig template

### Flex

Flex Aliases. Contoh
`composer require sec-checker --dev`
translate to 
`composer install sensiolabs/security-checker`

Services
`php /bin/console debug:autowiring`
`composer require symfony/apache-pack`


### Console

Example

Buat dulu Hash.php
```
<?php
/**
 * Created by PhpStorm.
 * User: Abah
 * Date: 19/03/2018
 * Time: 06.46
 */

namespace App;

class Hash{

    /**
     * Receives a string password and hashes it.
     *
     * @param string $password
     * @return string $hash
     */
    public static function hash($password){
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Verifies if an hash corresponds to the given password
     *
     * @param string $password
     * @param string $hash
     * @return boolean If the hash was generated from the password
     */
    public static function checkHash($string, $hash){
        if( password_verify( $string, $hash ) ){
            return true;
        }
        return false;
    }

}
```

Kemudian konfigurasin command-nya
```
<?php
/**
 * Created by PhpStorm.
 * User: Abah
 * Date: 19/03/2018
 * Time: 06.46
 */

namespace App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

use App\Hash;

class HashCommand extends Command{

    protected function configure(){
        $this->setName("hash:hash")
            ->setDescription("Hashes a given string using Bcrypt.")
            ->addArgument('Password', InputArgument::REQUIRED, 'What do you wish to hash)');
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        $hash = new Hash();
        $input = $input->getArgument('Password');

        $result = $hash->hash($input);

        $output->writeln('Your password hashed: ' . $result);

    }

}
```

cek 
`php bin/console`

maka akan keluar hash:hash

