/*
Title: ORM
Sort: 3
*/

ORM yang dijadikan standar di Symfony 4 adalah Doctrine.
Berikut cara pakainya.

### Installing
```
composer require symfony/orm-pack
composer require symfony/maker-bundle --dev
```

### Konfigurasi 
Ubah di doctrine.yaml di bawah dbal:

#### MySQL
        driver: 'pdo_mysql'
        server_version: '5.7'
        charset: utf8mb4
        default_table_options:
            charset: utf8mb4
            collate: utf8mb4_unicode_ci
        url: 'mysql://db_user:db_password@127.0.0.1:3306/db_name'

#### SQLServer
        driver: 'pdo_sqlsrv'        
        host: 'localhost'
        dbname: 'xxxx'
        user: 'xx'
        password: 'xxxxxx'

### Membuat DB
Asumsi database yang ditulis dalam setting belum dibuat:
```
bin/console doctrine:database:create
```

## Membuat Entitas
Membuat entity secara otomatis (pakai wizard pertanyaan2):
```
bin/console make:entity
```

Menjadikan tabel yg dimaksud
```
bin/console doctrine:migrations:migrate
```

## Mengubah Entitas

Mengupdate entitas sama, pakai command ini juga:
```
php bin/console make:entity

Class name of the entity to create or update
> Product

 to stop adding fields):
> description

Field type (enter ? to see all types) [string]:
> text

Can this field be null in the database (nullable) (yes/no) [no]:
> no

 to stop adding fields):
>
(press enter again to finish)
```

Membuat migration script
```
php bin/console make:migration
```

Mengeksekusinya
```
bin/console doctrine:migrations:migrate
```

