/*
Title: Auth
Sort: 2
*/

## Intro
Ceritanya bikin authentikasi via database.

## Buat Project

Bikin projectnya dulu dari skeleton:
```
composer create-project symfony/skeleton bookshop-api
cd bookshop-api
```

## Persiapan
Agar bisa baca profiler
```
composer require --dev symfony/profiler-pack
```
Agar log aktif
```
composer require symfony/monolog-bundle
```

Install doctrine
```
composer require symfony/orm-pack
composer require symfony/maker-bundle --dev
```

Edit koneksi
File `bookshop-api\config\packages\doctrine.yaml` di bawah dbal ganti jadi:
```
        driver: 'pdo_sqlsrv'        
        dbname: 'bookshop'
        user: 'sa'
        password: '3d0mondostra\/4'
```

Create database kalau belum dibuat:
`php bin/console doctrine:database:create`

Bikin user entity duluan di file `src/Entity/User.php`:
```
<?php
// src/Entity/User.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized, array('allowed_classes' => false));
    }
}
?>
```

Bikin juga reponya di file `src/Repository/UserRepository.php`:
```
<?php
// src/Repository/UserRepository.php
namespace App\Repository;

use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
?>
```

## Install Security

Install dulu bundlenya:
```
composer require symfony/security-bundle
```

Edit `bookshop-api\config\packages\security.yaml`:
```
security:
    encoders:        
        Symfony\Component\Security\Core\User\User:
            algorithm: bcrypt
            cost: 12
    providers:
        in_memory:
            memory:
                users:
                    abah:
                        password: $2y$12$ez1m1.5sSb0JNN7wzSmWBu1OmkPK1Xww7epug6R0YfU/yEQQNy.bq
                        roles: 'ROLE_USER'
                    admin:
                        password: $2y$12$ez1m1.5sSb0JNN7wzSmWBu1OmkPK1Xww7epug6R0YfU/yEQQNy.bq
                        roles: 'ROLE_ADMIN'
    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            anonymous: true
            http_basic: ~
    access_control:
        - { path: ^/admin, roles: ROLE_ADMIN }
        # - { path: ^/profile, roles: ROLE_USER }


```
