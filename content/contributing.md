/*
Title: Berkontribusi
Sort: 2
*/

Jika anda ingin berkontribusi ke Knowledge Base ini, berikut tahap-tahapnya:

* Clone repository ini dari [Nufaza KB Repository](https://bitbucket.org/znfgit/nufaza_kb)
* Buat perubahan di bawah direktori content, kemudian commit dan push
* Submit request artikel di [Nufaza KB Issue Management](https://bitbucket.org/znfgit/nufaza_kb/issues?status=new&status=open)
