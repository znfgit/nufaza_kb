------
Derasy
------

Develop Extjs React Angular apps over Symfony

Rapid Application Development adhere to current standards.
https://www.php-fig.org/psr/
https://semver.org/

Unit tested by:
PHPUnit https://phpunit.de/
Coding standard validated by:
PHP Codesniffer 


Getting Started

To create app with Extjs frontend
`composer create-project derasy/extjs my_app`

To create app with Angular frontend
`composer create-project derasy/angular my_app`

To create app with React frontend
`composer create-project derasy/react my_app`





Package List
derasy/derasy
    reverse_model
    build_model

derasy/rest
    create
    retrieve
        multiple order
        multiple sort
        multiple filter
        sideways filter
        authentication filter                
    update
    delete

derasy/extjs
    front-end-build

derasy/semantic-ui-react
    front-end-build

derasy/angular
    front-end-build


## Data Model

ORM using Propel 2
No more Peer classes.
Configuration is in config/propel.yaml. But somehow if you're using PropelBundle, the config needs to be copied to config/packages/propel.yaml with some modification.

### MSSQL Reverse Engineering Bug
Derasy need to automatically patch 
`vendor\propel\propel\src\Propel\Generator\Reverse\MssqlSchemaParser.php`

### How to Query
Complete in
http://propelorm.org/documentation/reference/model-criteria.html

Virtual Column using with -> keren.

preSave ?? nah ini nih!
preInsert(), preUpdate(), preSave() or preDelete()
penting untuk ngupdate lastUpdate, updaterId dll


### Standard Domain Types
name
organization_name
email
etc

### Generation

Two step
- reverse engineer to physical entity info
- physical entity manipulation


### Configuration

Path in `config/propel.yaml`
Complete example in https://gist.github.com/cristianoc72/9060420

```
propel:
  paths:
      # Directory where the project files (`schema.xml`, etc.) are located.
      # Default value is current path #
      projectDir: current_path

      # The directory where Propel expects to find your `schema.xml` file.
      schemaDir: config/propel

      # The directory where Propel should output classes, sql, config, etc.
      # Default value is current path #
      outputDir: config/propel

      # The directory where Propel should output generated object model classes.
      phpDir: src/Model

      # The directory where Propel should output the compiled runtime configuration.
      phpConfDir: config/propel

      # The directory where Propel should output the generated DDL (or data insert statements, etc.)
      sqlDir: config/propel

  database:
      connections:
          mysource:
              adapter: sqlsrv
              classname: Propel\Runtime\Connection\DebugPDO
              dsn: sqlsrv:server=localhost;Database=my_app;MultipleActiveResultSets=false
              user: sa
              password: 3d0mondostra\/4
              attributes:
  runtime:
      defaultConnection: mysource
      connections:
          - mysource
  generator:
      defaultConnection: mysource
      connections:
          - mysource

```


### Reverse Engineer
`vendor\bin\propel database:reverse --output-dir=config/propel --schema-name=my_app.schema --namespace=\MyApp my_app`
`vendor\bin\propel database:reverse --output-dir=config/propel --schema-name=testproj.schema --namespace=\Testproj testproj`

### Building Model
`vendor\bin\propel model:build --schema-dir config/propel`

### Building SQL Struct
`vendor\bin\propel sql:build`

### Build Class Map

-   derasy.yaml
    -   skipping gen
        -   skip whole datasource
        -   skip whole schema
        -   skip whole table
        -   skip all occurance of column
    -   generation types
        -   output folder

-   Configuration
    

### Installing
Installing extjs app named MyApp
`composer create-project derasy/extjs my_app`

Installing angular 5 app named MyApp
`composer create-project derasy/angular my_app`

Installing react app named MyApp
`composer create-project derasy/react my_app`


### Unit Test
Run with Jetbrains UI

### Continuous Integration
Jenkins




