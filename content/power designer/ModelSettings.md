/*
Title: Model Settings
Sort: 3
*/

#### Setting Model

Tools > Model Options > Di Dropdown Notation Pilih Entity/Relationship
![pd_1_model_settings](../images/pd_1_model_settings.png)

Pilih juga tab Code, pilih lowercase
![pd_2_code_lowercase](../images/pd_2_code_lowercase.png)

