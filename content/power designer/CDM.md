/*
Title: Membuat CDM
Sort: 4
*/

#### Membuat CDM Diagram Baru

Setiap project bisa memiliki beberapa diagram dan sub diagram CDM. Untuk aplikasi kecil cukup buat satu CDM.
Di tree sebelah kiri di folder Project klik kanan terus Create New Conceptual Data Model.

Save all. Save juga sws di direktori yang sama.

Renamce Concept_1 menjadi CDM. Kemudian klik kanan di situ buat 3 diagram.

Berikut hasilnya:
[![cdm_1_membuat_diagram](../images/cdm_1_membuat_diagram.png)](../images/cdm_1_membuat_diagram.png)

#### Membuat Entitas

Untuk membuat entitas tekan tombol kotak entitas di menu bar, kemudian tekan di diagram:
[![cdm_2_membuat_entitas](../images/cdm_2_membuat_entitas.png)](../images/cdm_2_membuat_entitas.png)

Buatlah sebagai contoh dua objek, Pengguna dan Peran.
Untuk membedakan obyek dengan referensi, kita beri warna biru untuk obyek, dan kuning untuk referensi. Untuk memberi warna klik kanan di entitas > format ... > pilih fill > lalu pilih warna. Atau di toolbar ada tombol bucket.


#### Membuat Domain

Domain adalah abstraksi tipe data yang dapat membantu kita untuk me-reuse tipe-tipe data yang serupa. Misalnya unique identifier (UUID), nama_orang, alamat, dst.
[![cdm_3_membuat_domain](../images/cdm_3_membuat_domain.png)](../images/cdm_3_membuat_domain.png)


#### Mengisi Attributes/Field

Untuk mengisi field double click saja pada Entitas:
[![cdm_4_membuat_atribut](../images/cdm_4_membuat_atribut.png)](../images/cdm_4_membuat_atribut.png)

Poin-poin yang perlu diperhatikan pada pengisian atribut:
-   Nama atribut boleh huruf besar kecil
-   Kolom-kolom di sebelah kanan (M, P, D) artinya:
    M = Mandatory
    P = Primary Key
    D = Domain
-   Untuk kolom yg didapuk sebagai primary key tabel yang dimaksud jangan lupa
    centang P.
-   Untuk kolom yg tipe datanya tipikal sebagaimana sudah didefinisikan pada
    domain, jangan lupa dropdown di kolom domainnya.
-   Untuk kolom primary key dan tidak membutuhkan urutan serial disarankan 
    menggunakan tipe data UUID.
-   Untuk kolom yg khusus, tipe datanya tidak diulang di tabel lain, langsung saja
    pilih tipe datanya sesuai dengan nature kolom ybs.

#### Membuat Relasi

Untuk mengisi field double click saja pada Entitas:
[![cdm_5_membuat_relasi](../images/cdm_5_membuat_relasi.png)](../images/cdm_5_membuat_relasi.png)

Poin-poin yang perlu diperhatikan pada pembuatan relasi:
-   Jangan terbalik menentukan Cardinality
-   Jangan lupa mengatur mandatory
-   Untuk memastikan relasi sudah benar atau belum, baca baik-baik uraian di
    paling atas tab "Cardinality"
-   JANGAN membuat circular dependency (jika tidak paham keperluannya). 
    Mencentang "mandatory" di kedua sisi kolom akan mengakibatkan circular 
    dependency.

#### Cek Model

Sebelum melangkah ke pembuatan / generate Physical Data Model (PDM), kita
harus melakukan cek model. Cek model bisa menimbulkan error terhadap standar kesepakatan desain kita. Untuk _supress_ error, kita bisa ubah _check model parameters_ sbb:

Tekan Tools > Check Model > Di check model parameters uncheck:
-   Domain > Invalid Data Type
-   Data Item > Invalid Data Type
-   Data Item > Data Item Used Multiple Times

[![cmd_6_check_model](../images/cmd_6_check_model.png)](../images/cmd_6_check_model.png)
