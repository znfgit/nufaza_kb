/*
Title: Membuat Project
Sort: 2
*/


#### Membuat Project Baru

Agar terorganisir, perlu dibuat Project Baru. Caranya

New > Project

Arahkan Location ke direktori dokumen standar sesuai dengan clientnya
format:
d:\Documents\Projects\Nama Client\Nama App\PowerDesigner

Contoh:
d:\Documents\Projects\Client Baru\Aplikasi A\PowerDesigner\

Tulis nama Projectnya misalnya Aplikasi A
![project_1_membuat_project](../images/project_1_membuat_project.png)




