/*
Title: Membuat PDM
Sort: 5
*/

#### Menggenerate PDM dari CDM
Physical Data Model dapat digenerate dari CDM. Di sini semua relationship sudah tampil sebagai kolom fisik. Berikut cara menggenerate PDM dari CDM.

Untuk memulai generate, pilih Tools > Generate Physical Data Model. Di sini pilihlah database yang akan anda gunakan.
Sebagai contoh di bawah ini digunakan Postgresql 9.x dengan tujuan dapat memahami konsep unique identifier dan schema.
[![pdm_1_pdm_generation_window](../images/pdm_1_pdm_generation_window.png)](../images/pdm_1_pdm_generation_window.png)

#### Mempersiapkan Setting utk Nama FK
Setting agar nama foreign key column di suatu tabel sama dengan nama yang kita berikan pada relation adalah di bawah ini. Tujuannya adalah jika misal ada lebih dari satu fk di tabel ybs ke satu tabel, kita yg in control. Syaratnya adalah kita HARUS memberi nama pada setiap relation yg kita bikin dengan nama kolom yg diinginkan. Ini dilakukan di CDM sebelum generate PDM, di Tools > Generate Physical Data Model > Detail
[![pdm_2_fk-column-name-setting](../images/pdm_2_fk-column-name-setting.png)](mn-name-setting.png)

#### Mempersiapkan UUID
UUID atau Universally Unique Identifier adalah suatu tipe data yang memungkinkan pembuatan id random
yang secara statistik tidak mungkin terjadi duplikat secara universal. UUID sangat bermanfaat di sistem
basis data terdistribusi yang pembuatan obyek/instance record terjadi secara tersebar, tidak terpusat.

Untuk mensetting agar UUID beroperasi buka Database > Edit Current Database > DataType > PhysDataType,
kemudian tambahkan UUID di tabel.
[![pdm_3_uuid_setting](../images/pdm_3_uuid_setting.png)](../images/pdm_3_uuid_setting.png)

#### Memisahkan Schema 
Di RDBMS modern biasanya sudah mendukung pemisahan _schema_. _Schema_ adalah struktur logical untuk penyimpanan database.
Sebuah schema mengandung obyek-obyek skema, dari mulai table, stored procedure, reports, dll.

Membuat schema baru caranya adalah right click pada tabel peran > Properties > Beri nama schema nya, 
(lower case saja), centang Is schema.
[![pdm_4_memisahkan_schema](../images/pdm_4_memisahkan_schema.png)](../images/pdm_4_memisahkan_schema.png)

#### Mempersiapkan Setting Case untuk Code Generation
Setting agar generated code untuk nama tabel dan kolom smallcase semua
Berlaku utk CDM maupun PDM. Tools > Model Options > Tab Code > Pilih lowercase
[![pdm_6_small_case_table_column_names](../images/pdm_6_small_case_table_column_names.png)](../images/pdm_6_small_case_table_column_names.png)

#### Generate Database
Untuk menggenerate database yang perlu dilakukan adalah klik menu Database > Generate Database.

Pindahkan direktori ke direktori kerja (gunakan trik Ctrl-P <- untuk mengkopi-paste posisi direktori.
File name juga diubah sesuai dengan versinya. Untuk versi awal kita gunakan create_struct_v0.0.1.sql.
[![pdm_7_generate_database](../images/pdm_7_generate_database.png)](../images/pdm_7_generate_database.png)

Standar bawaan power designer create maupun alter table diawali dengan drop dulu. Agar create langsung lancar
uncheck dulu di Options:
[![pdm_8_generate_database_options](../images/pdm_8_generate_database_options.png)](../images/pdm_8_generate_database_options.png)

Item yg harus diuncheck agar script "create" lancar adalah:
- Database > Drop Database
- User > Drop User
- Domain > Drop Domain
- Table & Column > Drop Table
- Column > Drop Database
- Key > Physical Options
- Foreign Key > Drop primary key

Jika sudah, klik OK, maka script database akan dibuat.


#### Memperbaiki Script

Ada bug yang belum resolvable di Power Designer generator utk Postgresql, yaitu pada saat alter key, postgresql tidak 
menempelkan nama schema pada foreign key table, sehingga postgres error. Untuk mengkoreksinya sisipkan teks berikut
sesudah blok script create, sebelum blok script alter.

`SET search_path TO ref, public;`

Berikut contohnya:

```
...

/*==============================================================*/
/* Index: PERAN_PK                                              */
/*==============================================================*/
create unique index PERAN_PK on ref.peran (
peran_id
);

SET search_path TO ref, public;     ///--- TAMBAHKAN BARIS INI

alter table pengguna
   add constraint FK_PENGGUNA_PEKERJAAN_PERAN foreign key (pekerjaan_id)
      references peran (peran_id)
      on delete restrict on update restrict;

...

```

Note:
Script tsb akan berlaku terus pada koneksi yang sama. Maka jika anda mengulang script yang menyisipkan baris tadi, 
tambahkan baris:

`SET search_path TO public;`

Di paling awal script yg anda edit.


#### Mengupdate Desain Database dgn Alter Script

Pengembangan database dengan metode agile pasti tidak statis. Database akan selalu berubah setiap cycle. Agar perubahan selalu terdokumentasi tahap-tahap yang dilakukan adalah sbb:

1.  Salin CDM (Conceptual Data Model) sebagai archive. Caranya, posisikan kursor
    di diagram CDM, kemudian Save as MyApp-v0.0.1, kemudian save as lagi sebagai
    MyApp.
  
2.  Salin PDM (Physical Data Model) sebagai Archive PDM (XML). Caranya posisikan
    di diagram PDM, kemudian Save as MyApp-v0.0.1, kemudian save as lagi sebagai
    MyApp.

    [![pdm_9_save_archive_pdm](../images/pdm_9_save_archive_pdm.png)](../images/pdm_9_save_archive_pdm.png)

3.  Kembali ke CDM, generate lagi PDM, jangan lupa check preserve modification.

4.  Kembali Ke PDM, perhatikan bahwa perubahan sudah masuk. Ke menu buka
    Database > Apply Model Changes to Database. Setting sesuai gambar di bawah:
    
    -  Beri nama alter_struct dengan versinya, misal alter_struct-v0.0.2.sql
    -  Pilih archive versi yg disimpan tadi, misal MyApp-v0.0.1.apm.
    -  Tekan OK, setelah itu maka script akan muncul dengan alter script sesuai 
       dengan perubahan yang dilakukan.

    [![pdm_10_apply_model_changes](../images/pdm_10_apply_model_changes.png)](../images/pdm_10_apply_model_changes.png)


#### Mengatasi Error Alter

Jika pada saat pelaksanaan alter untuk database multischema terjadi error, solusinya seperti berikut ini:

-   Perbaiki tabel: Database > Edit Current DBMS > General > Object > Table > Drop
    [![pdm_11_correct_alter_scripts](../images/pdm_11_correct_alter_scripts.png)](../images/pdm_11_correct_alter_scripts.png)

-   Perbaiki PK: Database > Edit Current DBMS > General > Object > PKey > Create
    [![pdm_12_correct_alter_scripts_pk](../images/pdm_12_correct_alter_scripts_pk.png)](../images/pdm_12_correct_alter_scripts_pk.png)

-   Perbaiki key: Database > Edit Current DBMS > General > Object > Key > Create
    [![pdm_12_correct_alter_scripts_index](../images/pdm_12_correct_alter_scripts_index.png)](../images/pdm_12_correct_alter_scripts_index.png)

