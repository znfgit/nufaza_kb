/*
Title: Auth
Sort: 2
*/


### Persiapkan AuthService

Buat file `src\app\auth-guard.service.ts` isinya:

```
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';
import { tap } from 'rxjs/operators/tap';


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: NbAuthService, private router: Router) {
  }

  canActivate() {
    return this.authService.isAuthenticated()
      .pipe(
        tap(authenticated => {
          if (!authenticated) {
            this.router.navigate(['auth/login']);
          }
        }),
      );
  }
}
```

Masuk ke file `app-routing.module.ts`, ubah jadi begini:
```
  ...
  // { path: 'pages', loadChildren: 'app/pages/pages.module#PagesModule' },
  {
    path: 'pages',
    canActivate: [AuthGuard], // here we tell Angular to check the access with our AuthGuard
    loadChildren: 'app/pages/pages.module#PagesModule',
  },
  ...
  
```

### Menghubungkan Login dengan Backend API Platform

Buat folder `src/app/auth`.
Buat folder `src/app/auth/login`.
Buat file  `src/app/auth/auth.module.ts`, isinya sbb:
```
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxAuthRoutingModule } from './auth-routing.module';
import { NbAuthModule } from '@nebular/auth';
import { NbAlertModule, NbButtonModule, NbCheckboxModule, NbInputModule } from '@nebular/theme';

import { NgxLoginComponent } from './login/login.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NgxAuthRoutingModule,

    NbAuthModule,
  ],
  declarations: [
    NgxLoginComponent,
  ],
})
export class NgxAuthModule extends NbAuthModule {
}
```

Buat file `src/app/auth/auth-routing.module.ts`, isinya sbb:
```
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NbAuthComponent } from '@nebular/auth';

import { NgxLoginComponent } from './login/login.component';

export const routes: Routes = [
  {
    path: '',
    component: NbAuthComponent,
    children: [
      {
        path: 'login',
        component: NgxLoginComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NgxAuthRoutingModule {
}
```

Buat file `src/app/auth/login/login.component.ts`, isinya sbb:
```
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component } from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent extends NbLoginComponent {
}
```

Buat file `src/app/auth/login/login.component.html`, isinya sbb:
```
<nb-auth-block>
  <h2 class="title">Sign In</h2>

  <form (ngSubmit)="login()" #form="ngForm" autocomplete="nope">

    <nb-alert *ngIf="showMessages.error && errors?.length && !submitted" outline="danger">
      <div><strong>Oh snap!</strong></div>
      <div *ngFor="let error of errors">{{ error }}</div>
    </nb-alert>

    <nb-alert *ngIf="showMessages.success && messages?.length && !submitted" outline="success">
      <div><strong>Hooray!</strong></div>
      <div *ngFor="let message of messages">{{ message }}</div>
    </nb-alert>

    <div class="form-group">
      <label for="input-username">Email address</label>
      <input nbInput
             [(ngModel)]="user.username"
             #username="ngModel"
             name="username"
             id="input-username"
             pattern=".+@.+\..+"
             placeholder="Email address"
             autofocus
             fullWidth
             [status]="username.dirty ? (username.invalid  ? 'danger' : 'success') : ''"
             [required]="getConfigValue('forms.validation.username.required')">
      <small class="form-text error" *ngIf="username.invalid && username.touched && username.errors?.required">
        Email is required!
      </small>
      <small class="form-text error"
             *ngIf="username.invalid && username.touched && username.errors?.pattern">
        Email should be the real one!
      </small>
    </div>

    <div class="form-group">
      <label for="input-password">Password</label>
      <input nbInput
             [(ngModel)]="user.password"
             #password="ngModel"
             name="password"
             type="password"
             id="input-password"
             placeholder="Password"
             fullWidth
             [status]="password.dirty ? (password.invalid  ? 'danger' : 'success') : ''"
             [required]="getConfigValue('forms.validation.password.required')"
             [minlength]="getConfigValue('forms.validation.password.minLength')"
             [maxlength]="getConfigValue('forms.validation.password.maxLength')">
      <small class="form-text error" *ngIf="password.invalid && password.touched && password.errors?.required">
        Password is required!
      </small>
      <small
        class="form-text error"
        *ngIf="password.invalid && password.touched && (password.errors?.minlength || password.errors?.maxlength)">
        Password should contains
        from {{ getConfigValue('forms.validation.password.minLength') }}
        to {{ getConfigValue('forms.validation.password.maxLength') }}
        characters
      </small>
    </div>

    <div class="form-group accept-group col-sm-12">
      <nb-checkbox name="rememberMe" [(ngModel)]="user.rememberMe" *ngIf="rememberMe">Remember me</nb-checkbox>
      <a class="forgot-password" routerLink="../request-password">Forgot Password?</a>
    </div>

    <button nbButton
            status="success"
            fullWidth
            [disabled]="submitted || !form.valid"
            [class.btn-pulse]="submitted">
      Sign In
    </button>
  </form>

  <div class="links">

    <ng-container *ngIf="socialLinks && socialLinks.length > 0">
      <small class="form-text">Or connect with:</small>

      <div class="socials">
        <ng-container *ngFor="let socialLink of socialLinks">
          <a *ngIf="socialLink.link"
             [routerLink]="socialLink.link"
             [attr.target]="socialLink.target"
             [attr.class]="socialLink.icon"
             [class.with-icon]="socialLink.icon">{{ socialLink.title }}</a>
          <a *ngIf="socialLink.url"
             [attr.href]="socialLink.url"
             [attr.target]="socialLink.target"
             [attr.class]="socialLink.icon"
             [class.with-icon]="socialLink.icon">{{ socialLink.title }}</a>
        </ng-container>
      </div>
    </ng-container>

    <small class="form-text">
      Don't have an account? <a routerLink="../register"><strong>Sign Up</strong></a>
    </small>
  </div>
</nb-auth-block>

```

Tambahkan di `src/app/app-routing.module.ts`:

```
// Custom component
import { NgxLoginComponent } from './auth/login/login.component';
```

Dan ganti baris `path: 'auth'` ke bawah jadi begini:
```
  {
    path: 'auth',
    loadChildren: './auth/auth.module#NgxAuthModule',
```

Di `src/app/app.module.ts` tambahkan:
```
import { NgxAuthModule } from './auth/auth.module';
```
dan ubah `NbAuthModule.forRoot({` menjadi `NgxAuthModule.forRoot({`
serta jadi begini:
```
        strategies: [
          NbPasswordAuthStrategy.setup({
              name: 'email',
              // Sesuaikan berikut ini dgn posisi API anda
              baseEndpoint: 'http://localhost:8000/api', 
              token: {
                class: NbAuthJWTToken,
                key: 'token',
              },
              login: {
                endpoint: '/login_check',
                method: 'post',
              },
            ...
```

### Menginject Semua HTTP Request dengan Token

Kebayang kan repotnya kalau berkali-kali harus menempelkan header "Bearer token" pada setiap request agar API di backend mau melayani aplikasi. Nah ini ada solusinya. 

Pertama tambahkan Interceptor, NbAuthJWTInterceptor pada load library core angular di file `app.module.ts`.

```
import { Component, Inject, Input, OnInit } from '@angular/core';
import { NbAuthService, NbAuthJWTInterceptor } from '@nebular/auth';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

```

Kemudian tambahkan di bawah `providers: `:
```
{ provide: HTTP_INTERCEPTORS, useClass: NbAuthJWTInterceptor, multi: true }, // added by abah
```


### Mengintegrasikan Logout

Syarat logout jalan adalah di backend sudah disiapkan API nya, dan injector sudah ready juga. 

Kemudian pastikan di bawah code ini di `app-module.ts`:
```
    NgxAuthModule.forRoot({
         strategies: [
            NbPasswordAuthStrategy.setup({
```

Sudah ada code konfigurasi logout:
```
              logout: {
                endpoint: '/logout',
                redirect: {
                  success: '/',
                  failure: '/'
                }
              },
```

Kemudian di file `header.component.ts` pastikan importnya kumplit:
```
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../../../@theme/components/modal/modal.component';
import { filter, map } from 'rxjs/operators';
```

Terus di dalam `constructor(...)`:
```
              @Inject(NB_WINDOW) private window,
              private modalService: NgbModal,
              private authService: NbAuthService
```

Dan pada init:
```
  ngOnInit() {
    
    this.menuService.onItemClick()
    .pipe(
        filter(({ tag }) => tag === 'my-context-menu'),
        map(({ item: { title } }) => title),
      )
    .subscribe(title => {

        console.log(title);
        switch(title) {
          case "Profile":
            this.showProfile();
            break;
          case "Logout":
            this.logout();
            break;
        }
    });
  }
```
