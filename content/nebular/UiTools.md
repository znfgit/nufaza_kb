/*
Title: UI Tools

Sort: 3
*/


### Menampilkan Modal

Cara memanggil modal dgn sederhana:

Pertama pastikan sudah ada komponennya dulu, yg reusable di `@theme\components\modal`. Yaitu `modal.component.ts` (yg diambil dari pages): 

```
import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-modal',
  template: `
    <div class="modal-header">
      <span>{{ modalHeader }}</span>
      <button class="close" aria-label="Close" (click)="closeModal()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      {{ modalContent }}
    </div>
    <div class="modal-footer">
      <button class="btn btn-md btn-primary" (click)="closeModal()">Save changes</button>
    </div>
  `,
})
export class ModalComponent {

  modalHeader: string;
  modalContent = `Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
    nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis
    nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.`;

  constructor(private activeModal: NgbActiveModal) { }

  closeModal() {
    this.activeModal.close();
  }
}

```

Memanggilnya cukup gini, di imports:
```
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
```

Dan code untuk manggil:
```
    const activeModal = this.modalService.open(ModalComponent, {
      size: 'sm',
      backdrop: 'static',
      container: 'nb-layout',
    });
    activeModal.componentInstance.modalHeader = 'Profile Anda';
    activeModal.componentInstance.modalContent = 'Anda kayanya admin';
```

