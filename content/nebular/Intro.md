/*
Title: Intro
Sort: 1
*/

### Installing

Clone dulu dari reponya.
```
git clone https://github.com/akveo/ngx-admin.git
```

Kemudian install npm modules:
```
cd ngx-admin && npm i
```

Ikuti dokumentasi:
https://akveo.github.io/nebular/docs


### Mengubah Template Default

Di folder `src\app\@theme\theme.module.ts`, ganti 
```
...
NbThemeModule.forRoot(
    {
      // name: 'cosmic',    //-- comment jadi seperti ini
      name: 'default',      //-- tambahkan baris ini
    },
```

#### Folder structure

dist            --> deployable folder
e2e             --> end to end testing
node_modules    --> vendor kalau di composer
src             --> source codes
|-- app         --> app codes
|--+-- @core    --> core app functionality that is shared app-wide
   |-- @theme   --> theme specific codes
   |-- auth     --> authentication specific costumizes (look and feel)
   |-- pages    --> app pages   
|-- assets      --> images, maps, etc
|-- environtmet --> prod / dev?

Ini rasanya harus dimantapkan lagi. Nanti lah.





