/*
Title: Route Redirect
Sort: 5
*/

## Route Redirect

Route redirect seringkali diperlukan dalam berbagai kasus, misalnya:

-   Setelah menyimpan form, perlu melakukan redirect ke page awal
-   Memisahkan proses penyusunan export excel atau pdf dengan proses downloadnya.
-   Memblokir akses ke suatu modul dengan mengarahkannya ke page lain, misal page warning atau maintenance.


## Code Redirect

Berikut contoh redirect:

```
$app->get('/cobaredirect', function () use ($app) {
    // Code membuat excel/menyimpan form dsb
    return $app->redirect('/');
});

