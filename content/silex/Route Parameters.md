/*
Title: Route Parameters
Sort: 4
*/

## Route Parameters

Route dapat menerima passing parameter/variable. Untuk GET request dapat dilakukan melalui query string ataupun route path.

### By querystring

Seperti biasa pada umumnya web query
```
// By querystring
$app->get('/cobanama', 'MyApp\Coba::halloNama');
```

### By route path

Siapkan dulu pathnya. Misal kita akan nangkap variable nama. Maka buat nama variabelnya ditutup kurung kurawal.

```
// By route path
$app->get('/cobanama/{nama}', 'MyApp\Coba::halloNama');
```

Untuk mengetest, coba di browser (Login dulu saja agar gak usah setting security bypass):
http://myapp/cobanama?nama=Abah
http://myapp/cobanama/Abah (edited)


Dokumentasi lebih jauh untuk silex bisa dibuka di:
http://silex.sensiolabs.org/
