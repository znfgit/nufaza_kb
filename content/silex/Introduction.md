/*
Title: Introduction
Sort: 1
*/

## What

Silex adalah micro PHP Framework buatan Fabien P, pencipta Symfony.

## Why

Silex dipilih karena cocok utk web stack yg front-end heavy, sebagaimana
SPA berbasis ExtJS yang kita pakai.

## Instalasi

Instalasi silex biasanya melalui composer. Utk Xond3, instalasi silex sudah otomatis melalui wizard.

