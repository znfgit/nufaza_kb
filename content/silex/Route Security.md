/*
Title: Route Security
Sort: 3
*/

## Route Security

By default settingan di Xond3, semua route itu harus melalui security firewall. Untuk membypassnya, kita harus meng-entry bypass code. Contoh keperluan bypass ini misalnya untuk page public, gerbang upload dari aplikasi android, atau sinkronisasi data dari aplikasi desktop yg tidak memungkinkan login berbasis form.

## Membypass security routing utk silex

Tambahkan di app.php:
```
// Default Security Setting
 $app['security.firewalls'] = array(
...
     'cobaclass' => array(
         'pattern' => '^/cobaclass.*$',
         'anonymous' => true
     ),
```

serta di

``` 
$app['security.access_rules'] = array(
...
    array('^/cobaclass$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
...
);

```
Agar bisa masuk cobaclass tanpa login.
