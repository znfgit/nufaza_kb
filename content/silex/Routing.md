/*
Title: Routing
Sort: 2
*/

## Mengenal Routing

Routing adalah mapping antara URL query dengan code di backend. Standarnya PHP, tidak ada routing. Setiap eksekusi langsung mengakses file sesuai struktur folder yang ada.

Routing menyembunyikan struktur folder. Dengan UrlRewrite, semua string request diarahkan ke `web/index.php`, dan mappingnya dilakukan di `app/app.php`.

## Cara routing

### Direct Routing

Coba tambahkan di `C:\Projects\localhost\my_app\app\app.php`:
```
$app->get('/coba', function (Request $request) use ($app) {
    return "Assalaamu'alaikum";
});
```

### Routing to Class
Coba tambahkan di `C:\Projects\localhost\my_app\app\app.php`:
```
$app->get('/cobaclass', 'MyApp\Coba::hallo');
```

Dan buat Coba.php di `C:\Projects\localhost\my_app\src\MyApp\Coba.php`
```
<?php
/**
 * Created by PhpStorm.
 * User: Abah
 * Date: 27/04/2016
 * Time: 11:31
 */

namespace MyApp;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Exception\Exception;
class Coba {

    public function hallo (Request $request, Application $app) {
        return "Hallo, mencoba dari Class";
    }
}
