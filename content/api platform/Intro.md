/*
Title: Intro
Sort: 1
*/

# API Platform

Adalah server side API framework di atas Symfony 4 dan Doctrine. Dia mengotomatisasi CRUD untuk aplikasi berorientasi API-first .

## Instalasi

Ada dua jurus. Bisa pake docker, instan. Atau ngeteng.

### Pakai Docker.

Pre-requistry pake docker harus udah tahu ya. Step2nya:
-  Unduh dari sini zip/tarballnya https://github.com/api-platform/api-platform/releases/latest, extract.
-  Jalanin 
```
    docker-compose up -d
```

### Ngeteng

Pre-requistrynya harus udah siap composer sama nodejs.

#### Backend API

Pertama kita buat sisi backend api nya.

-   Bikin project Symfony 4 dulu
```
    composer create-project symfony/skeleton bookshop-api
```

-   Masuk ke project folder
```
    cd bookshop-api
```

-   Install server component API Platform di atasnya pakai flex.
```
    composer req api
```

-   Seting dulu databasenya. Buka file `config/packages/config.yaml`. 
    Untuk API dengan satu Database SQL Server ganti di bawah *dbal* seperti ini:
```
    dbal:
        driver: 'pdo_sqlsrv'        
        dbname: '%anu%'
        user: '%user_anda%'
        password: '%password_anda%'
```

-   Cek koneksi dengan membuat database yang dimaksud dengan command. 
    Utk di unix diganti `php bin\console` diganti `bin/console`.
```
    php bin\console doctrine:database:create
```


-   Muncul jawaban `Created database [anu] for connection named default`. 
    Itu berarti koneksi berhasil. Kalau gagal? Periksa bertahap ya:
    -  Mulai dari servernya sudahkah dienable TCP/IP nya, 
    -  apakah firewallnya sudah dibuka utk port ybs (SQLServer di TCP 1433), 
    -  apakah tes connection pake applikasi manajemen database bisa via TCP/IP, 
    -  dst2.

-   Kita coba bikin dua entitas di src/Entity yaitu book dan review, kemudian kita generate schemanya.

    **Book.php**
```
    // api/src/Entity/Book.php

    namespace App\Entity;

    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\ORM\Mapping as ORM;
    use ApiPlatform\Core\Annotation\ApiResource;

    /**
     + A book.
     *
     + @ORM\Entity
     + @ApiResource
     */
    class Book
    {
        /**
         + @var int The id of this book.
         *
         + @ORM\Id
         + @ORM\GeneratedValue
         + @ORM\Column(type="integer")
         */
        private $id;

        /**
         + @var string|null The ISBN of this book (or null if doesn't have one).
         *
         + @ORM\Column(nullable=true)
         */
        public $isbn;

        /**
         + @var string The title of this book.
         *
         + @ORM\Column
         */
        public $title;

        /**
         + @var string The description of this book.
         *
         + @ORM\Column(type="text")
         */
        public $description;

        /**
         + @var string The author of this book.
         *
         + @ORM\Column
         */
        public $author;

        /**
         + @var \DateTimeInterface The publication date of this book.
         *
         + @ORM\Column(type="datetime")
         */
        public $publicationDate;

        /**
         + @var Review[] Available reviews for this book.
         *
         + @ORM\OneToMany(targetEntity="Review", mappedBy="book")
         */
        public $reviews;
        
        public function __construct() {
            $this->reviews = new ArrayCollection();
        }

        public function getId(): ?int
        {
            return $this->id;
        }
    }
```

**Review**

```
    // api/src/Entity/Review.php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use ApiPlatform\Core\Annotation\ApiResource;

    /**
     + A review of a book.
     *
     + @ORM\Entity
     + @ApiResource 
     */
    class Review
    {
        /**
         + @var int The id of this review.
         *
         + @ORM\Id
         + @ORM\GeneratedValue
         + @ORM\Column(type="integer")
         */
        private $id;

        /**
         + @var int The rating of this review (between 0 and 5).
         *
         + @ORM\Column(type="smallint")
         */
        public $rating;

        /**
         + @var string the body of the review.
         *
         + @ORM\Column(type="text")
         */
        public $body;

        /**
         + @var string The author of the review.
         *
         + @ORM\Column
         */
        public $author;

        /**
         + @var \DateTimeInterface The date of publication of this review.
         *
         + @ORM\Column(type="datetime_immutable")
         */
        public $publicationDate;

        /**
         + @var Book The book this review is about.
         *
         + @ORM\ManyToOne(targetEntity="Book", inversedBy="reviews")
         */
        public $book;

        public function getId(): ?int
        {
            return $this->id;
        }
    }
```

-   Kemudian kita generate.
```
    php bin\console doctrine:schema:create
```
-   Install juga schema generator:
```
    composer require --dev api-platform/schema-generator
```

-   Kita install dan jalankan Symfony WebServerBundle
```
    composer req server --dev
    bin/console server:run
```
-   Install juga CORS agar bisa API bisa dibaca JS
```
    composer require "nelmio/cors-bundle:@dev"
```

Terus di folder `config\packages\nelmio_cors.yaml`, edit jadi begini:

```
    nelmio_cors:
    defaults:
        allow_credentials: false
        allow_origin: []
        allow_headers: []
        allow_methods: []
        expose_headers: ['Content-Disposition', 'Content-Length', 'Link']
        max_age: 0
        hosts: []
        origin_regex: false
        forced_allow_origin_value: ~
    paths:
        '^/api':
            allow_origin: ['*']
            allow_headers: ['*']
            allow_methods: ['GET', 'OPTIONS', 'POST', 'PUT', 'PATCH', 'DELETE']
            max_age: 120
        '^/': ~
```

#### Front End Consumer App

Kemudian kita bikin consumer app untuk memanage api.

-  Bikin react app pake command biasa. Posisi masih di dalam direktori api, gpp.
```
   npx create-react-app admin
```

-  Ubah isi src/App.js:

```
    import React, { Component } from 'react';
    import { HydraAdmin } from '@api-platform/admin';

    class App extends Component {
      render() {
        return  // Replace with your own API entrypoint
      }
    }

    export default App;
```

-  Jalankan
```
    cd admin
    npm start
```


## Testing

Coba localhost:8000 utk tampilan API  dan localhost:3000 untuk simulasi aplikasi.

### API

#### Test GET
Pilih salah satu entity, kemudian pencet GET. Kemudian pencet Try It Out. Kasih halaman 1, Execute.

#### Test POST
Pilih BOOK, kemudian pencet POST. Kemudian pencet Try It Out. Di bawah "Example Value" sebenarnya itu form input text. Ganti json yg ada dengan ini.
```
{
  "isbn": "9781782164104",
  "title": "Persistence in PHP with the Doctrine ORM",
  "description": "This book is designed for PHP developers and architects who want to modernize their skills through better understanding of Persistence and ORM.",
  "author": "Kévin Dunglas",
  "publicationDate": "2013-12-01"
}
```
Kemudian execute. Baca hasilnya di bawah/


