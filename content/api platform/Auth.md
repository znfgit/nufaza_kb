/*
Title: Auth
Sort: 2
*/

### Intro
Authentikasi untuk API Platform menggunakan LexikJWTAuthenticationBundle. Bundle tersebut menggunakan authentication provider Symfony. Standardnya di Symfony 4 kita pakai Doctrine User Provider.

### Installing Doctrine User Provider

Pastikan manajemen migrations sudah siap. Install package2 ini dulu:
```
composer require symfony/maker-bundle --dev
composer require migrations
php bin/console make:migration
```

Install dulu kalau belum:
```
composer require symfony/security-bundle
```
Agar log aktif
```
composer require symfony/monolog-bundle
```

Dan migrations untuk mengupdate modelnya:
```
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate
```

### Setup Doctrine User

Bikin User Entity-nya. Biar cepet kita ambil dari contoh saja, save di src/Entity/User.php:

```
// src/Entity/User.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized, array('allowed_classes' => false));
    }
}
```

### Installing LexikJWTAuthenticationBundle

Install via composer
```
composer require lexik/jwt-authentication-bundle
```

Buat folder untuk nyimpen keys
```
mkdir -p config/jwt # For Symfony3+, no need of the -p option
```

Generate ssh keys
```
openssl genrsa -out config/jwt/private.pem -aes256 4096
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
```

Konfigurasi `config/packages/lexik_jwt_authentication.yaml`:
```
lexik_jwt_authentication:
    secret_key:       '%kernel.project_dir%/config/jwt/private.pem' # required for token creation
    public_key:       '%kernel.project_dir%/config/jwt/public.pem'  # required for token verification
    pass_phrase:      'sukasepjay' # required for token creation, usage of an environment variable is recommended
    token_ttl:        3600
```

Konfigurasi `config/packages/security.yaml`:
```
        login:
            pattern:  ^/api/login
            stateless: true
            anonymous: true
            json_login:
                check_path:               /api/login_check
                success_handler:          lexik_jwt_authentication.handler.authentication_success
                failure_handler:          lexik_jwt_authentication.handler.authentication_failure

        api:
            pattern:   ^/api
            stateless: true
            guard:
                authenticators:
                    - lexik_jwt_authentication.jwt_token_authenticator

    access_control:
        - { path: ^/api/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/api,       roles: IS_AUTHENTICATED_FULLY }
```
Dengan konfigurasi ini, semua request ke ^api/* selain login akan diminta token. Tanpa token maka akan keluar `{"code":401,"message":"JWT Token not found"}`.

Tambahkan di `config/routes.yaml`:
```
api_login_check:
    path: /api/login_check
```

Untuk mengetest:
```
curl -X POST -H "Content-Type: application/json" http://localhost:8000/api/login_check -d '{"username":"admin","password":"admin"}'

curl -X POST -H "Content-Type: application/json" http://api.anisa/api/login_check -d '{"username":"admin","password":"admin"}'
```
Maka dia akan mengembalikan hasil semacam ini:
```
{
   "token" : "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.eyJleHAiOjE0MzQ3Mjc1MzYsInVzZXJuYW1lIjoia29ybGVvbiIsImlhdCI6IjE0MzQ2NDExMzYifQ.nh0L_wuJy6ZKIQWh6OrW5hdLkviTs1_bau2GqYdDCB0Yqy_RplkFghsuqMpsFls8zKEErdX5TYCOR7muX0aQvQxGQ4mpBkvMDhJ4-pE4ct2obeMTr_s4X8nC00rBYPofrOONUOR4utbzvbd4d2xT_tj4TdR_0tsr91Y7VskCRFnoXAnNT-qQb7ci7HIBTbutb9zVStOFejrb4aLbr7Fl4byeIEYgp2Gd7gY"
}
```
Perhatikan bahwa jika anda menggunakan console, token akan terpotong2 oleh new line. Hapus newlinenya sebelum menggunakan token.

### Memindahkan Swagger UI

Swagger UI adalah dokumentasi API yg secara default tampil ketika kita mengakses /api via browser. Namun setelah kita pasang authentikasi JWT, /api akan menagih token (utk apache dan IIS). Untuk apache kita bisa workaround agar Swagger tetap tampil namun untuk IIS, kita harus pindahkan docsnya ke alamat lain. Setelah itu kita juga pastikan agar docs tetap bisa digunakan untuk testing api.

Sekarang kita coba pindahkan ke /docs. Tambahkan di `config/routes.yaml`:
```
swagger_ui:
    path: /docs
    controller: api_platform.swagger.action.ui
```
Tambahkan ke `config/packages/api_platform.yaml`:
```
    enable_swagger_ui: false
    swagger:
         api_keys:
             apiKey:
                name: Authorization
                type: header
```

### Mencoba API dengan JWT Protection
Pertama ambil dulu tokennya pakai command curl tadi, kemudian masuk ke /docs. Tekan authorize. Ketik "Bearer TOKEN". TOKEN diganti dgn copas token yg didapat tadi.
Contohnya 
```
Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1Mzc0ODc2MjMsImV4cCI6MTUzNzQ5MTIyMywicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiYWRtaW4ifQ.eSQaZvPiMHVDNj9QlqnUh_9mWgTzHkEJz5yGuW5Z-XNa4yuXI0uyaScxCi3GYq4pQEyJbELdxICtjHNrMrp6prc2DWHR25Vp2NgmNw9PaLTP_HyzKaAOpS5CCcGL5TMKUeLhsK63UF37Z9BZnaaLBDRdFmbeEu8VcK2NoUTOzAZin9Ks278iozVWpJtEdy2edHHdeXd5bAeqHL8gkSdl6zLBpjXVCqV_BMU1AaBYCA4Bx902Dw6INurxAKXe-yUkZwJGnjUVeoE8SWb6EOdVP_t2aodDu8L1KcAVXUD_oRHym-ybsUyEy3LGmSMBcCn9hL0Z6h2uIIilEKbHIE3MlE0JRsA_0XrXeeovRMhZ7XeHh0wYH9tqXnfLzNFIi0US0bnIjRThUj8IRtID5weMEvNsRIXAttw7v-BVJJHj45FeN9bkthZ-mKbbiO2sRV4kL1FBBZCyfPxv-fULK_L8rHed8JDv0KhiX_uRhfyMusmlYVQOdjlno9hC5dXmel3XnmWRuNjiWGOGb8zqxLZMhElreRMtIPdzBTQF-qJ6bA57U79d2biIMVumRwixUX3ltPINn8-pcSmCeM0_RxuGMpzVhNiJShtB7-pH8j1Z7JTrXsfapoKTwjvDIQBehiUr5efJilLyfpgSpcyj89xGREBzvoh-PzXcs1a_Xo94PwM
```
Setelah itu, masuk salah satu entity > command, misal GET. Kemudian tekan try out. Maka di response datanya akan keluar. Jika token salah maka responsenya akan `401 invalid token` atau `token not found`.

### Menambahkan Halaman Login untuk Mendapatkan Token

Install dulu:
```
composer require gfreeau/get-jwt-bundle
```

Karena kita menggunakan annotation @Method, install dulu SensioFrameworkExtraBundle:
```
composer require sensio/framework-extra-bundle
```

Tambahkan di `security.yaml`, di bawah firewalls (tapi di atas `api:`, jika tidak, tidak akan nendang:
```
    firewalls:
        gettoken:
            pattern:  ^/api/getToken$
            stateless: true
            gfreeau_get_jwt:
                # this is the default config
                username_parameter: username
                password_parameter: password
                authentication_provider: security.authentication.provider.dao
                user_checker: security.user_checker 
                success_handler: lexik_jwt_authentication.handler.authentication_success
                failure_handler: lexik_jwt_authentication.handler.authentication_failure
```
Buat `Controller/SecurityController.php`:
```
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SecurityController extends AbstractController
{
    /**
     * @Route("/api/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
    * @Route("/api/getToken")
    * @Method({"POST"})
    */
    public function getTokenAction()
    {
        // The security layer will intercept this request
        return new Response('', 401);
    }
}

```

Bikin juga templatenya di `templates\security\login.html.twig`
```
{# templates/security/login.html.twig #}
{# ... you will probably extend your base template, like base.html.twig #}

{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}

<form action="/api/getToken" method="post">
    <label for="username">Username:</label>
    <input type="text" id="username" name="username" value="{{ last_username }}" />

    <label for="password">Password:</label>
    <input type="password" id="password" name="password" />

    {#
        If you want to control the URL the user
        is redirected to on success (more details below)
        <input type="hidden" name="_target_path" value="/account" />
    #}

    <button type="submit">login</button>
</form>
```
Silakan coba login di http://localhost:8000/api/login.
Anda akan mendapatkan token tanpa harus pakai CURL.


### Menambahkan Route Untuk Menjawab Logout dari Nebular

Sebelumnya pastikan nebular sudah diset untuk selalu meng-inject token pada setiap requestnya.

Kemudian tambahkan kode berikut di `src/Controller/SecurityController.php`
```
    /**
     * @Route("/api/logout", name="logout")
     */
    public function logout(AuthenticationUtils $authenticationUtils)
    {
        return new Response('{"success": true}', 200);
    }
```
