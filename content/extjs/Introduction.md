/*
Title: Introduction
Sort: 1
*/

## What

ExtJS adalah sebuah application framework untuk membangun aplikasi web yang interactive dan cross platform menggunakan teknik Ajax, DHTML dan DOM scripting yang murni berbasis JavaScript. ExtJS biasanya digunakan untuk membuat RIA (Rich Internet Application) yang bersifat SPA (Single Page Application), walaupun komponen-komponennya bisa dipakai  untuk melengkapi aplikasi html biasa.

## Why

ExtJS dipilih karena sangat terstruktur dengan baik, komponennya paling lengkap, dan jika sudah dikuasai dapat membuat programmer web sangat produktif.

## Instalasi

### Sencha Command
Sencha command adalah program console yang dibuat untuk meng-otomatisasi pembuatan (generation) atau kompilasi aplikasi. Sencha command memerlukan Java & Ruby karena mengandung berbagai komponen opensource lainnya seperti Compass. 

Instalasi sencha command cukup sederhana, cukup jalankan executable setupnya.

### Extract source code
Extract saja source extjs-5.0.0.736.zip ke c:\Projects\localhost. Rename menjadi c:\Projects\localhost\extjs5.

### Setup localhost
Siapkan localhost/extjs5. Buat site localhost di IIS. Arahkan ke c:\Projects\localhost. Kasih domain localhost

### Test di Browser
Pastikan bahwa http://localhost/extjs5 dapat dibuka dari browser.



