/*
Title: Intro
Sort: 1
*/

Bismillah.

Nufaza KB adalah Knowledgebase untuk PT. Nufaza. Menggunakan file statis dengan format 
[Markdown](http://daringfireball.net/projects/markdown). Sistem ini dibuat tanpa database,
dan dimantain melalui git repository. Nufaza KB mendukung pencarian full text.
